<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'deadsea_wp1');

/** MySQL database username */
define('DB_USER', 'deadsea_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'I^O*CDksjA61)[4');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yhXDmJtMQ57lTN8JvAv4d3t6ICl1BsQ3VU8TfTqloqmsyeYi8LujzI61BtoHeCBt');
define('SECURE_AUTH_KEY',  'ylO5rkPA2k9MGSdVN1esQCGW68kRj45YgpNgN7Rw2TE0cKSrINMgVVf0fzf0GvAO');
define('LOGGED_IN_KEY',    'Y0cLNEBxdONCTeVC9HKncKE57iahWvzObrBEbhbZ3jWQwun8r9fUhuf4cUcQrD8G');
define('NONCE_KEY',        'ah0NsVfPKGlD1KVe6Gi0OB0lAa7d6q19IJjUKOuPWlbza0Cd2d7iuDeXytZhy1YX');
define('AUTH_SALT',        'vjnqWEbwptmF2KItaklj0JDedjgF2DdZQPw6JPknb53hbp72kvgHbkDaVdVxOUfn');
define('SECURE_AUTH_SALT', 'KOerRs48ODwf6yL7LcJrpRPNURP6cHutk8jFbmVwfEPJcmhomHLcroO1nlIpCYTf');
define('LOGGED_IN_SALT',   'l6Vp69RHKfMvrlHouTp5pIFJmakjzaERqvokmUAIH442wDRGI7L0ikrGoMzV5HIa');
define('NONCE_SALT',       'KORMQXzVosuTxGeA20TeaGkOlpMAHupSiex4zYQuCLPCNHKaJTotvMh87eZRg9Ve');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
