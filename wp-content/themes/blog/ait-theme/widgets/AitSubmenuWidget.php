<?php


class AitSubmenuWidget extends WP_Widget
{
	protected $language;

	function __construct()
	{
		$this->language = defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : "en";
		$widget_ops = array('classname' => 'widget_submenu', 'description' => __( 'Display submenu for current page', 'ait-admin') );
		parent::__construct('ait-submenu', __('Theme &rarr; Submenu', 'ait-admin'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract( $args );
		$result = '';

		global $post;
		if(isset($post)){
			$children = wp_list_pages( 'echo=0&child_of=' . $post->ID . '&title_li=' );
			if ($children) { $parent = $post->ID;
			} else {
				$parent = $post->post_parent;
				if(!$parent){ $parent = $post->ID; }
			}
			$parent_title = get_the_title($parent);
		} else {
			$parent = 0;
			$parent_title = '';
		}

		$output = wp_list_pages( array('title_li' => '', 'echo' => 0, 'child_of' =>$parent, 'sort_column' => 'menu_order', 'depth' => 1) );
		if(empty( $output )){
			$output = wp_list_pages( array('title_li' => '', 'echo' => 0, 'sort_column' => 'menu_order', 'depth' => 1) );
		}

		/* WIDGET CONTENT :: START */
		$result .= $before_widget;
		$title = '';
		if(isset($instance['title_'.$this->language])){
			$title = apply_filters('widget_title', empty($instance['title_'.$this->language]) ? '' : $instance['title_'.$this->language], $instance, $this->id_base);
		}
		$result .= $before_title.$title.$after_title;
		$result .= '<ul>'.$output.'</ul>';
		$result .= $after_widget;
		/* WIDGET CONTENT :: END */

		echo($result);
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title_'.$this->language] = strip_tags($new_instance['title_'.$this->language]);

		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
            'title_'.$this->language => '',
        ) );
    ?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title_'.$this->language)); ?>"><?php _e('Title:', 'ait-admin'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title_'.$this->language)); ?>" name="<?php echo esc_attr($this->get_field_name('title_'.$this->language)); ?>" type="text" value="<?php echo esc_attr($instance['title_'.$this->language]); ?>" />
		</p>
<?php
	}

}
