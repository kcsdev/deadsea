<?php

/*
 * AIT WordPress Theme
 *
 * Copyright (c) 2014, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */

return array(

	'menus' => array(
		'main'   => __('Main menu', 'ait-admin'),
		'footer' => __('Footer menu', 'ait-admin'),
	),

	// Supported standard WordPress features
	'theme-support' => array(
		'automatic-feed-links',
		'post-thumbnails',
		'woocommerce',
	),

	// Supported custom ait-theme features
	'ait-theme-support' => array(
		'megamenu',
		'cpts' => array(
			'ad-space',
			'toggle',
		),
		'elements' => array(
			'get-directions',
			'advertising-spaces',
			'columns',
			'contact-form',
			'easy-slider',
			'facebook',
			'google-map',
			'mixcloud',
			'page-title',
			'posts',
			'revolution-slider',
			'rule',
			'seo',
			'soundcloud',
			'text',
			'toggles',
			'twitter',
			'video',
			'widget-area',
			'authors',
		),
	),


	'plugins' => array(
		'ait-toolkit' => array(
			'required' => true,
		),
		'ait-shortcodes' => array(
			'required' => true,
		),
		'ait-languages' => array(
			'required' => true,
		),
		'revslider' => array(
			'required' => true,
		),
	),


	'assets' => array(
		'fonts' => array(
			'opensans' => array(
				'regular', 'semibold', 'bold', 'italic',
			),
			'titilliumweb' => array(
				'regular', 'bold', 'semibold', 'black', 'italic',
			),
			'awesome',
		),

		'css' => array(
			'jquery-selectbox' => array(
				'file' => '/libs/jquery.selectbox.css',
			),
			'font-awesome'	=> array(
				'file'	=> '/libs/font-awesome.css',
			),
			'jquery-ui-css' => true,
		),

		'admin-css' => array(
			'admin-css' => array(
				'file' => '/admin/style.css',
			),
		),

		'js' => array(
			'jquery-selectbox' => array(
				'file' => '/libs/jquery.selectbox-0.2.js',
				'deps' => array('jquery')
			),
			'jquery-raty' => array(
				'file' => '/libs/jquery.raty-2.5.2.js',
				'deps' => array('jquery')
			),
			'jquery-waypoints' => array(
				'file' => '/libs/jquery-waypoints-2.0.3.js',
				'deps' => array('jquery')
			),
			'jquery-infieldlabels' => array(
				'file'	=> '/libs/jquery.infieldlabel-0.1.4.js',
				'deps'	=> array('jquery'),
			),

			'ait-mobile-script' => array(
				'file' => '/mobile.js',
				'deps' => array('jquery')
			),
			'ait-menu-script' => array(
				'file' => '/menu.js',
				'deps' => array('jquery', 'ait-mobile-script')
			),
			'ait-portfolio-script' => array(
				'file' => '/portfolio-item.js',
				'deps' => array('jquery', 'ait-mobile-script', 'jquery-ui-accordion', 'jquery-bxslider')
			),
			'ait-custom-script' => array(
				'file' => '/custom.js',
				'deps' => array('jquery', 'ait-mobile-script')
			),
			'jquery-gmap3-local' => array(
				'file'	=> '/libs/gmap3.min.js',
				'deps'	=> array('jquery', 'googlemaps-api'),
			),
			'jquery-gmap3-infobox-local' => array(
				'file'	=> '/libs/gmap3.infobox.js',
				'deps'	=> array('jquery', 'jquery-gmap3-local'),
			),

			'ait-woocommerce-script' => array(
				'file' => '/woocommerce.js',
				'deps' => array('jquery'),
				'enqueue-only-if' => '!is_admin() and aitIsPluginActive("woocommerce")',
			),

			'ait-script' => array(
				'file' => '/script.js',
				'deps' => array('jquery', 'ait-mobile-script', 'ait-menu-script', 'ait-portfolio-script', 'ait-custom-script')
			),
		),
	),

	'frontend-ajax' => array(
		'send-email',
	),

	// add meta info for user
	// @config - name of neon file in ait-themes/config/metaboxes/user-profile.metabox.neon
	'page-post-metaboxes' => array(
		'user-metabox' => array(
			'types' => array(
				'user',
			),
			'config' => 'user-profile',
		),
		'post-gps-metabox' => array(
			'title' => __('Post Geolocation', 'ait-admin'),
			'types' => array(
				'post',
			),
			'config' => 'post-gps',
			'saveCallback'=> 'aitFixPostCoordinates'
		),
		'post-background-metabox' => array(
			'title' => __('Post Background', 'ait-admin'),
			'types' => array(
				'post',
			),
			'config' => 'post-background',
		),
		'post-gallery-metabox' => array(
			'title' =>  __('Post Gallery', 'ait-admin'),
			'types' => array(
				'post',
			),
			'config' => 'post-gallery',
		),
	),


);
