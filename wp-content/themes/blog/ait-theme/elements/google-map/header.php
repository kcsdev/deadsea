{if ($el->hasOption(title)) or ($el->hasOption(description))}


	{if $el->option->specialTitle and ($wp->isSingular(post) or $wp->isAuthor or $wp->isCategory() or is_date()) }
	<div n:class="elm-mainheader, $el->hasOption(headAlign) ? $el->option->headAlign">

		{if $wp->isSingular(post)}
			<h2 class="elm-maintitle">{__ 'Blog Post Location'}</h2>
		{elseif $wp->isAuthor}
			<h2 class="elm-maintitle">{__ 'Map of All Posts by %s'|printf: $author->displayName}</h2>
		{elseif $wp->isCategory()}
			<h2 class="elm-maintitle">{__ 'All Posts from This Category'}</h2>
		{elseif is_date()}
			<h2 class="elm-maintitle">{__ 'All Posts from This Month'}</h2>
		{/if}
		
		{if $el->option->description}
			<p class="elm-maindesc">{!$el->option->description}</p>
		{/if}
		
	</div>
	{else}
		{if $el->option->title !='' or $el->option->description !='' }
			<div n:class="elm-mainheader, $el->hasOption(headAlign) ? $el->option->headAlign">
				{if $el->option->title}
					<h2 class="elm-maintitle">{!$el->option->title}</h2>
				{/if}

				{if $el->option->description}
					<p class="elm-maindesc">{!$el->option->description}</p>
				{/if}
			</div>
		{/if}
	{/if}



{/if}
