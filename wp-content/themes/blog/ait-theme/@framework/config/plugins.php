<?php


return array(
	
	'ait-toolkit' => array(
		'name'          => 'AIT Toolkit',
		'version'       => '1.6.1',
		'required'      => true,
		'ait-prepacked' => true,
		'source'        => aitPath('plugins', '/ait-toolkit.zip'),
	),
	'ait-shortcodes' => array(
		'name'          => 'AIT Shortcodes',
		'version'       => '1.1.1',
		'required'      => true,
		'ait-prepacked' => true,
		'source'        => aitPath('plugins', '/ait-shortcodes.zip'),
	),
	
	'revslider' => array(
		'name'          => 'Revolution Slider',
		'version'       => '4.6.93',
		'required'      => true,
		'ait-prepacked' => true,
		'source'        => aitPath('plugins', '/revslider.zip'),
	),
);
