<div class="slide entry-slide active" style="background-image: url('{imageUrl $titleImage, width => 1000, height => 500, crop => 1}');"></div>
{if aitPostGalleryExists()}
	{var $meta = $post->meta('post-gallery-metabox') }
	{var $duration = $meta->slideDuration}
	{var $inputs = $meta->inputs}

	{foreach $inputs as $input}
		{var $slideBgImage = "background-image: url('{$input['bgImage']}');"}

		<div class="slide slide-{$iterator->counter}" style="{$slideBgImage|noescape}"></div>
	{/foreach}

	<script>
	/*
	 * AIT WordPress Theme
	 *
	 * Copyright (c) 2012, Affinity Information Technology, s.r.o. (http://ait-themes.com)
	 */
	jQuery(window).load(function(){

		var slidesWrap = jQuery('.entry-thumbnail');
		slides = slidesWrap.find('.slide');
		entryInfo = jQuery('.entry-info');

		var numSlides = slides.length,
			slideDuration = {$duration},
			imgsWrap = jQuery('.entry-gallery').find('.thumbnails');


		// slides.first().addClass('active');

		iterator = 0;


		function autoSlide() {
			iterator = iterator + 1;
			if( iterator == numSlides ) iterator = 0;

			slides.removeClass('active');
			slides.eq(iterator).addClass('active');

			var bg = slides.eq(iterator).css('background-image');
        	bg = bg.replace('url(','').replace(')','');
			entryInfo.find('a').attr("href", bg);
		}
		var intervalId = setInterval(autoSlide,slideDuration);

		entryInfo.hover(function(ev){
		    clearInterval(intervalId);
		}, function(){
		    intervalId = setInterval( autoSlide, slideDuration);
		});




	});

	</script>


{/if}










