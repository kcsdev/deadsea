	<article id="post-0" class="post no-results not-found">
		<header class="entry-header">
			<h1 class="entry-title">

			{var $currentUser = wp_get_current_user()}

			{if $wp->canCurrentUser(edit_posts) and $message == empty-site}
				{__ 'No posts to display'}
			{elseif $message == 'no-author-posts'}
				{if is_user_logged_in() and is_author($currentUser->ID)}
					{__ 'Welcome, this is your blog'}
				{else}
					{__ 'No posts to display'}
				{/if}
			{else}
				{__ 'Nothing Found'}
			{/if}
			</h1>
		</header>

		<div class="entry-content">
			<p>
			{if $wp->canCurrentUser(edit_posts) and $message == empty-site}

				{!__ 'Ready to publish your first post? <a href="%s">Get started here</a>.' |printf:$wp->adminUrl('post-new.php')}

			{elseif $message == nothing-found}

				{__ 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.'}

			{elseif $message == 'no-posts' or (!$wp->canCurrentUser(edit_posts) and $message == empty-site)}

				{__ 'Apologies, but no results were found. Perhaps searching will help find a related post.'}

			{elseif $message == 'no-author-posts'}

				{var $welcomeAuthor = $options->theme->authors->welcomeAuthor}
				{var $welcomeVisitor = $options->theme->authors->welcomeVisitor}

				{if ((isset($welcomeAuthor) and $welcomeAuthor != '') or (isset($welcomeVisitor) and $welcomeVisitor != ''))}

					{if is_user_logged_in() and is_author($currentUser->ID)}
						{!do_shortcode($welcomeAuthor)}
					{else}
						{!do_shortcode($welcomeVisitor)}
					{/if}

				{else}

					{__ 'Apologies, but no results were found. Perhaps searching will help find a related post.'}

				{/if}

			{/if}
			</p>

			{searchForm}

		</div><!-- .entry-content -->
	</article><!-- #post-0 -->
