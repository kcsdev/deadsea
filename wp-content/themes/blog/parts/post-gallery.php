{var $images = array()}
{var $labels = array()}
{var $meta = $post->meta('post-gallery-metabox') }
{var $inputs = $meta->inputs}
{var $duration = $meta->slideDuration}

{foreach $inputs as $input}
	{? array_push($images, $input['bgImage'])}
	{? array_push($labels, $input['label'])}
{/foreach}
<div class="entry-gallery">
	<div class="thumbnails-wrap">
		{if count($inputs) > 3}
		<div class="navigation">
			<a href="#" class="left"></a>
			<a href="#" class="right"></a>
		</div>
		{/if}

		<div class="thumbnails">
			<a href="{$post->imageUrl}" class="disable-default-colorbox">
				<img src="{aitResizeImage($post->imageUrl, array('width' => 480, 'height' => 300, 'crop' => true))}" alt="{!$post->title}">
				<span class="thumbnail-label">{!aitGetThumbnailTitle($post->id)}</span>
			</a>
			{foreach $inputs as $input}
				{var $image_link = $input['bgImage']}
				{var $image_label = $input['label']}
				<a href="{$image_link}" class="disable-default-colorbox">
					<img src="{aitResizeImage($image_link, array('width' => 480, 'height' => 300, 'crop' => true))}" alt="{!$image_label}">
					<span class="thumbnail-label">{!$image_label}</span>
				</a>
			{/foreach}
		</div>
	</div>
</div>



<script>
/*
 * AIT WordPress Theme
 *
 * Copyright (c) 2012, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */
jQuery( window ).load(function() {
	var gridMain = jQuery('.grid-main'),
		entryThumbnail = jQuery('.entry-thumbnail'),
		imgsWrap = gridMain.find('.entry-gallery').find('.thumbnails'),
		numImgs = imgsWrap.children().length,
		navigation = gridMain.find('.entry-gallery').find('.navigation'),
		images = imgsWrap.find('a'),
		wrapPosition = 0;

	if( numImgs < 5 ) navigation.hide();
	imgsWrap.css('left', wrapPosition + '%');


	navigation.find('a.left').on('click', function(e){
		e.preventDefault();

		if( wrapPosition == 0 ) return false;

		wrapPosition = wrapPosition + 25;
		imgsWrap.css('left', wrapPosition + '%');

	});

	navigation.find('a.right').on('click', function(e){
		e.preventDefault();

		if( Math.abs(wrapPosition) == (numImgs - 3) * 25 ) return false;

		wrapPosition = wrapPosition - 25;
		imgsWrap.css('left', wrapPosition + '%');

	});

	images.on('click', function (e){
		e.preventDefault();
		slides.removeClass('active');
		slides.eq(images.index(this)).addClass('active');
		iterator = images.index(this);
		entryInfo.find('a').first().attr("href", this.getAttribute('href'));
	});
});
</script>