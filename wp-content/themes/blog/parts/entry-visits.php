{var $postID = isset($postID) ? $postID : ""}
<div class="visitors">
	<span class="number">
	<?php if ( function_exists( 'ev_post_views' ) ) ev_post_views(array('post_id' => $postID)); ?>
	</span>
	<span class="caption">{__ 'visits'}</span>
</div>
