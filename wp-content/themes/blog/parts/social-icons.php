{ifset $authorIcons}
	{var $icons = $authorIcons}
{else}
	{var $icons = $options->theme->social->socIcons}
{/ifset}

{if $options->theme->social->enableSocialIcons or isset($authorIcons) }
<div class="social-icons">
	<ul><!--
		{foreach $icons as $icon}
			{if is_array($icon)}
				{var $icon = (object)$icon}
			{/if}

			--><li class="{if $icon->iconFont}iconFont{else}iconImg{/if}">
				<a href="{$icon->url}" {if $options->theme->social->socIconsNewWindow}target="_blank"{/if}>
					{if $icon->iconFont}
						<i class="fa {$icon->iconFont}"></i>
					{else}
						{if $icon->icon}<img src="{$icon->icon}" class="s-icon s-icon-light" alt="icon">{/if}
					{/if}
					<span class="s-title">{AitLangs::getCurrentLocaleText($icon->title)}</span>
				</a>
			</li><!--
		{/foreach}
	--></ul>
</div>
{/if}
