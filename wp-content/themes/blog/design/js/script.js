/*
 * AIT WordPress Theme
 *
 * Copyright (c) 2012, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */
/* Main Initialization Hook */
jQuery(document).ready(function(){
	/* menu.js initialization */
	desktopMenu();
	// responsiveMenu();
	/* menu.js initialization */

	/* portfolio-item.js initialization */
	portfolioSingleToggles();
	/* portfolio-item.js initialization */

	/* custom.js initialization */
	renameUiClasses();
	removeUnwantedClasses();

	initWPGallery();
	initColorbox();
	initRatings();
	//initInfieldLabels();
	initSelectBox();

	notificationClose();

	btnTop();

	fixTouchMenu();
	/* custom.js initialization */

	/* Theme Dependent FIX Functions */

	/* Theme Dependent FIX Functions */
});
/* Main Initialization Hook */

/* Theme Dependenent Fix Functions */

// Langwitch | Woocommerce
function fixInitSelectBox(){
	jQuery('.widget_ait-languages #lang_choice').selectbox();
	jQuery('.widget-container.woocommerce select').selectbox({
		onOpen: function(inst){
			jQuery(this).parent().parent().parent().css({'z-index': 100});
		},
		onClose: function(inst){
			jQuery(this).parent().parent().parent().removeAttr("style");
		}
	});
}

// Langwitch | Language Dropdown
function fixLanguageMenu(){
	if(isResponsive(640)){
		// only run at 640px-
		jQuery('.language-icons a.current-lang').bind('touchstart MSPointerDown', function(){
			if(jQuery('.language-icons').hasClass('menu-opened')){
				jQuery('.language-icons .language-icons__list').hide();
			} else {
				jQuery('.language-icons .language-icons__list').show();
			}
			jQuery('.language-icons').toggleClass('menu-opened');

			return false;
		});
	}
}

// Blog Theme Button-Top
function btnTop(){
	var btnTop = jQuery('.footer-menu .btn-top');
	if(btnTop.length){
		btnTop.on('click', function(){
			jQuery('html, body').animate({
				scrollTop: 0
			}, 1000);
		});
	}
}

/* Theme Dependenent Fix Function */



function fixTouchMenu() {
	menu = jQuery('.main-nav-wrap');
	if(isResponsive(980)){


		if(!isMobile()) {
			menu.mouseenter(function(){
				menu.addClass('hover');
			})
			.mouseleave(function(){
				menu.removeClass('hover');
			});
		}else {
			jQuery('.site-header').css('position', 'absolute');
			jQuery('.main-nav-wrap .nav-menu-main').css('display', 'none');
			jQuery('.main-nav-wrap .nav-menu-main').css('position', 'absolute');
			jQuery('.main-nav-wrap .nav-menu-main').css('overflow', 'visible');
			jQuery('.main-nav-wrap .nav-menu-main').css('height', 'auto');
			menu.click(function(){
				if(menu.hasClass('hover')) {
					jQuery('.main-nav-wrap .nav-menu-main').css('display', 'none');
					menu.removeClass('hover');
				}
				else {
					jQuery('.main-nav-wrap .nav-menu-main').css('display', 'block');
					menu.addClass('hover');
				}
			});

		}
	}


}


jQuery(document).ready(function(){
    jQuery("img").removeClass("skew");
});