<?php //netteCache[01]000605a:2:{s:4:"time";s:21:"0.34075900 1441875927";s:9:"callbacks";a:4:{i:0;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:9:"checkFile";}i:1;s:118:"/home/deadsea/domains/deadseaguide.com/public_html/wp-content/themes/blog/ait-theme/elements/google-map/javascript.php";i:2;i:1441875423;}i:1;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:20:"NFramework::REVISION";i:2;s:22:"released on 2014-08-28";}i:2;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:15:"WPLATTE_VERSION";i:2;s:5:"2.9.0";}i:3;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:17:"AIT_THEME_VERSION";i:2;s:4:"1.29";}}}?><?php

// source file: /home/deadsea/domains/deadseaguide.com/public_html/wp-content/themes/blog/ait-theme/elements/google-map/javascript.php

?><?php
// prolog NCoreMacros
list($_l, $_g) = NCoreMacros::initRuntime($template, 'j6gdopmkrl')
;
// prolog NUIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return NUIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
?>
<script id="<?php echo NTemplateHelpers::escapeHtml($htmlId, ENT_COMPAT) ?>-container-script">

	jQuery(window).load(function(){
		var map;
		var mapDiv = jQuery("#<?php echo $htmlId ?>-container");

		var styles = [
			{
				stylers: [
					{ hue: "<?php echo $el->option('mapHue') ?>" },
					{ saturation: "<?php echo $el->option('mapSaturation') ?>" },
					{ lightness: "<?php echo $el->option('mapBrightness') ?>" },
				]
			},
			{ featureType: "landscape", stylers: [
					{ hue: "<?php echo $el->option('landscapeColor') ?>"},
					{ saturation: "<?php if ($el->option('landscapeColor') != '') { ?> <?php echo $el->option('objSaturation') ?>
 <?php } ?>"},
					{ lightness: "<?php if ($el->option('landscapeColor') != '') { ?> <?php echo $el->option('objBrightness') ?>
 <?php } ?>"},
				]
			},
			{ featureType: "administrative", stylers: [
					{ hue: "<?php echo $el->option('administrativeColor') ?>"},
					{ saturation: "<?php if ($el->option('administrativeColor') != '') { ?> <?php echo $el->option('objSaturation') ?>
 <?php } ?>"},
					{ lightness: "<?php if ($el->option('administrativeColor') != '') { ?> <?php echo $el->option('objBrightness') ?>
 <?php } ?>"},
				]
			},
			{ featureType: "road", stylers: [
					{ hue: "<?php echo $el->option('roadsColor') ?>"},
					{ saturation: "<?php if ($el->option('roadsColor') != '') { ?> <?php echo $el->option('objSaturation') ?>
 <?php } ?>"},
					{ lightness: "<?php if ($el->option('roadsColor') != '') { ?> <?php echo $el->option('objBrightness') ?>
 <?php } ?>"},
				]
			},
			{ featureType: "water", stylers: [
					{ hue: "<?php echo $el->option('waterColor') ?>"},
					{ saturation: "<?php if ($el->option('waterColor') != '') { ?> <?php echo $el->option('objSaturation') ?>
 <?php } ?>"},
					{ lightness: "<?php if ($el->option('waterColor') != '') { ?> <?php echo $el->option('objBrightness') ?>
 <?php } ?>"},
				]
			},
			{ featureType: "poi", stylers: [
					{ hue: "<?php echo $el->option('poiColor') ?>"},
					{ saturation: "<?php if ($el->option('poiColor') != '') { ?> <?php echo $el->option('objSaturation') ?>
 <?php } ?>"},
					{ lightness: "<?php if ($el->option('poiColor') != '') { ?> <?php echo $el->option('objBrightness') ?>
 <?php } ?>"},
				]
			},
		];

		mapDiv.gmap3({
			map:{
<?php if ($wp->isSingular('post')) { ?>
					<?php echo $latLngCode ?>

<?php } else { ?>
					<?php echo $addressCode ?>

<?php } ?>
				options:{
					<?php echo $centerCode ?>

					mapTypeId: google.maps.MapTypeId.<?php echo $el->option('type') ?>,
					zoom: <?php echo $el->option('zoom') ?>,
					scrollwheel: <?php echo $scrollWheel ?>,
					styles: styles,
				}
			},
			marker:{
				values:[
<?php $markers = $el->option('markers') ?>
					<?php if (empty($markers)) { $markers = array() ;} ?>
					//loop custom map markers
<?php $iterations = 0; foreach ($markers as $mark) { ?>
						{
							address: "<?php echo $mark['address'] ?>",
							data: <?php if ($mark['url'] != "") { ?>'<div class="gmap-infowindow-content"><a href="<?php echo $mark['url'] ?>">'+
							'<h3><?php if (strlen($mark['title']) > 70) { echo htmlspecialchars(substr($mark['title'], 0, 70), ENT_QUOTES) ?>
...<?php } else { echo $mark['title'] ;} ?></h3>'+
							'<p><?php echo htmlspecialchars(substr($mark['description'], 0, 75), ENT_QUOTES) ?></p></a></div>'
									<?php } else { ?>'<div class="gmap-infowindow-content">'+
									'<h3><?php if (strlen($mark['title']) > 70) { echo substr($mark['title'], 0, 70) ?>
...<?php } else { echo $mark['title'] ;} ?></h3>'+
									'<p><?php echo substr($mark['description'], 0, 75) ?></p></div>'<?php } ?>,
<?php if ($mark['icon'] != "") { ?>
							options:
							{
								icon: "<?php echo $mark['icon'] ?>"
							}
<?php } ?>
						},
<?php $iterations++; } ?>

					// loop wordpress posts
<?php $iterations = 0; foreach ($items as $item) { $meta = $item->meta('post-gps-metabox') ;if (!is_object($meta)) { continue ;} $meta = $meta->map ;if ($meta['latitude'] == "0" and $meta['longitude'] == "0") { continue ;} ?>
						{
							lat: <?php echo NTemplateHelpers::escapeJs($meta['latitude']) ?>, lng: <?php echo NTemplateHelpers::escapeJs($meta['longitude']) ?>,
							data: '<div class="gmap-infowindow-content"><a href="<?php echo $item->permalink ?>">'+
							'<?php if ($item->hasImage) { ?><span><img src="<?php echo aitResizeImage($item->imageUrl, array('width' => 60, 'height' => 60, 'crop' => 1)) ?>
" alt="<?php echo $item->title ?>"></span><?php } ?>'+
							'<h3><?php if (strlen($item->title) > 45) { echo substr($item->title, 0, 45) ?>
...<?php } else { echo $item->title ;} ?></h3>'+
							'<span class="date"><?php echo $template->dateI18n($item->date('c'), 'j. M Y') ?></span></a></div>',
<?php if ($el->option('icon') != "") { ?>
							options:
							{
								icon: "<?php echo $el->option('icon') ?>",
							},
<?php } ?>
							id: "marker-<?php echo NTemplateHelpers::escapeJs($item->id) ?>",
						},
<?php $iterations++; } ?>

				],
				cluster:{
					radius: 50,
					0: {
						content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
						width: 53,
						height: 53
					},
					events: {
						click: function(cluster) {
							var map = jQuery(this).gmap3("get");
							map.panTo(cluster.main.getPosition());
							map.setZoom(map.getZoom() + 2);
						}
					}
				},
				options:{
					draggable: false
				},
				events:{
					click: function(marker, event, context){
						var map = jQuery(this).gmap3("get");

						/* Remove All previous infoboxes */
						mapDiv.find('.infoBox').remove();

						if(context.data != "disabled"){

							var infoBoxOptions = {
								content: context.data,
								disableAutoPan: false,
								pixelOffset: new google.maps.Size(-117, -130),
								zIndex: 99,
								boxStyle: {
									background: "#FFFFFF",
									opacity: 1,
									width: "265px",
									height: "80px"
								},
								closeBoxMargin: "2px 2px 2px 2px",
								closeBoxURL: "<?php echo aitPaths()->url->img ?>/infobox_close.png",
								infoBoxClearance: new google.maps.Size(1, 1),
								position: marker.position
							};

							var infoBox = new InfoBox(infoBoxOptions);
							infoBox.open(map, marker);
						}

						map.panTo(marker.getPosition());
					},
				},
			}
		});


		setTimeout(function(){
			checkTouchDevice();
<?php if ($displayPosts and $wp->isSingular('post')) { ?>
				var currMarker = mapDiv.gmap3({ get: { name: "marker", id: "marker-<?php echo NTemplateHelpers::escapeJs($post->id) ?>" }});
				google.maps.event.trigger(currMarker, 'click');
<?php } ?>
		},1000);


<?php if ($options->theme->general->progressivePageLoading) { ?>
			if(!isResponsive(1024)){
				jQuery("#<?php echo $htmlId ?>").waypoint(function(){
					jQuery("#<?php echo $htmlId ?>").parent().parent().addClass('load-finished');
				}, { triggerOnce: true, offset: "95%" });
			} else {
				jQuery("#<?php echo $htmlId ?>").parent().parent().addClass('load-finished');
			}
<?php } else { ?>
			jQuery("#<?php echo $htmlId ?>").parent().parent().addClass('load-finished');
<?php } ?>

		var checkTouchDevice = function() {
			if (Modernizr.touch){
				map = mapDiv.gmap3("get");
				map.setOptions({ draggable : false });
				var draggableClass = 'inactive', draggableTitle = 'Activate map';
				var draggableButton = jQuery('<div class="draggable-toggle-button '+draggableClass+'">'+draggableTitle+'</div>').appendTo(mapDiv);

				draggableButton.click(function () {
					if(jQuery(this).hasClass('active')){
						jQuery(this).removeClass('active').addClass('inactive').text(<?php echo NTemplateHelpers::escapeJs(__('Activate map', 'wplatte')) ?>);
						map.setOptions({ draggable : false });
					} else {
						jQuery(this).removeClass('inactive').addClass('active').text(<?php echo NTemplateHelpers::escapeJs(__('Deactivate map', 'wplatte')) ?>);
						map.setOptions({ draggable : true });
					}
				});
			}
		}

	});

</script>
