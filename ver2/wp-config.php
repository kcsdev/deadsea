<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'deadsea_wp2');

/** MySQL database username */
define('DB_USER', 'deadsea_wp2');

/** MySQL database password */
define('DB_PASSWORD', 'W#Ved2RSwT@CoUb8Ov]21&@5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Mz2DHkAgLSK2TMifVaPy5PUeMRoi531VjHKS6ME68Xm3NUDMOn9tUEbVoKvWsvQW');
define('SECURE_AUTH_KEY',  'KmkRtFDWwnfanr250DI1LSiTtG08ben8fycrb43zTC1wSGxluFkMkET95DpgBsRz');
define('LOGGED_IN_KEY',    'rME1TL8sLUPSPDT5qx0VHKnE3sGy2cQn0MiO72ruBcW7TPejmfKNPAtFmNGo9sE6');
define('NONCE_KEY',        'Al0X5hKy3Xjo28BvUYogrpVglwzXf5ZEHrIhbX7YfV5zkMkO1R3hm7uJ1vHKkEjK');
define('AUTH_SALT',        'j3O6Hnfnk5WOKb91Qwn7cGUeBKZdRq2XO4E3aKiDlplDwgOuIlK5Q3JVJvCdHLiO');
define('SECURE_AUTH_SALT', '4eptCBRBOZMoPrbM3GR9IpVhsMSw0OCH6fXOJNI6HRrpS8pFZEgum3LZRj6szV1f');
define('LOGGED_IN_SALT',   'EoGOnT6yRRzV5VDxWAAFYF9BGLeGwFSNJUov2xM4JzKDXjMzmU32Q17SsoV7QwM0');
define('NONCE_SALT',       'VzaxtmNrG19h970Tr5uOqrWRZcf9ys3LBY4vtmrwsvQENhD4fslS1iOu2hika9g3');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
