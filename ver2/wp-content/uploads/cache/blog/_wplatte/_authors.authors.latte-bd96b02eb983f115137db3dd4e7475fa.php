<?php //netteCache[01]000606a:2:{s:4:"time";s:21:"0.74330600 1445937329";s:9:"callbacks";a:4:{i:0;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:9:"checkFile";}i:1;s:119:"/home/deadsea/domains/deadseaguide.com/public_html/ver2/wp-content/themes/blog/ait-theme/elements/authors/authors.latte";i:2;i:1445870399;}i:1;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:20:"NFramework::REVISION";i:2;s:22:"released on 2014-08-28";}i:2;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:15:"WPLATTE_VERSION";i:2;s:5:"2.9.0";}i:3;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:17:"AIT_THEME_VERSION";i:2;s:4:"1.34";}}}?><?php

// source file: /home/deadsea/domains/deadseaguide.com/public_html/ver2/wp-content/themes/blog/ait-theme/elements/authors/authors.latte

?><?php
// prolog NCoreMacros
list($_l, $_g) = NCoreMacros::initRuntime($template, '7ue6rmbd9d')
;
// prolog NUIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return NUIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
NCoreMacros::includeTemplate($el->common('header'), $template->getParameters(), $_l->templates['7ue6rmbd9d'])->render() ?>

<div id="<?php echo NTemplateHelpers::escapeHtml($htmlId, ENT_COMPAT) ?>" class="elm-item-organizer <?php echo NTemplateHelpers::escapeHtml($htmlClass, ENT_COMPAT) ?>">

<?php $authors = array() ;$authorConfig = $el->getOptionObjectFromConfig('authors') ;$enableCarousel  = false ?>

<?php $args = array(
		'meta_key'    => 'last_name',
		'include'     => !in_array("0", (array)$el->option->authors) ? $el->option->authors : aitGetUsersByRoles($authorConfig['roles']),
		'orderby'     => $el->option('orderby'),
		'order'       => $el->option('order'),
		'number'      => $el->option('count'),
	) ?>

<?php $iterations = 0; foreach (get_users($args) as $author) { array_push($authors, WpLatte::createEntity('PostAuthor', $author->ID)) ;$iterations++; } ?>



<?php if ($args['orderby'] == 'random') { shuffle($authors) ;} ?>


<?php if (!empty($authors)) { $layout = $el->option->layout ;$target = $el->option('linksInNewWindow') ? 'target="_blank"':null ;$textRows = $el->option->textRows ;$authorAvatar = $el->option->authorAvatar ;$authorPostsCount = $el->option->authorPostsCount ;$authorBio = $el->option->authorBio ;$authorSocial = $el->option->authorSocial ;$authorBackground = $el->option->itemBackground ;$authorBackgroundImage = $el->option->itemBackgroundImage ;if ($layout == 'box') { $enableCarousel  = $el->option->boxEnableCarousel ;$boxAlign 		  = $el->option->boxAlign ;$numOfRows       = $el->option->boxRows ;$numOfColumns    = $el->option->boxColumns ;} else { $enableCarousel  = $el->option->listEnableCarousel ;$numOfRows       = $el->option->listRows ;$numOfColumns    = $el->option->listColumns ;} ?>

<?php if ($enableCarousel) { ?>
			<div class="loading"><span class="ait-preloader"><?php echo __('Loading&hellip;', 'wplatte') ?></span></div>
<?php } ?>

<?php if ($layout == 'box') { ?>
			<div data-cols="<?php echo NTemplateHelpers::escapeHtml($numOfColumns, ENT_COMPAT) ?>
" data-first="1" data-last="<?php echo NTemplateHelpers::escapeHtml(ceil(sizeof($authors) / $numOfRows), ENT_COMPAT) ?>
" data-arrow-left="<?php echo NTemplateHelpers::escapeHtml(__('previous', 'wplatte'), ENT_COMPAT) ?>
" data-arrow-right="<?php echo NTemplateHelpers::escapeHtml(__('next', 'wplatte'), ENT_COMPAT) ?>
"<?php if ($_l->tmp = array_filter(array('elm-item-organizer-container', "column-{$numOfColumns}", "layout-{$layout}", $enableCarousel ? 'carousel-container' : 'carousel-disabled',))) echo ' class="' . NTemplateHelpers::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT) . '"' ?>>
<?php $iterations = 0; foreach ($iterator = $_l->its[] = new NSmartCachingIterator($authors) as $author) { $meta = (object)$author->meta('user-metabox') ?>

<?php if ($enableCarousel and $iterator->isFirst($numOfRows)) { ?>
					<div<?php if ($_l->tmp = array_filter(array('item-box', $enableCarousel ? 'carousel-item':null))) echo ' class="' . NTemplateHelpers::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT) . '"' ?>>
<?php } ?>
				<div	data-id="<?php echo NTemplateHelpers::escapeHtml($iterator->counter, ENT_COMPAT) ?>
"<?php if ($_l->tmp = array_filter(array('item', "item{$iterator->counter}",	$enableCarousel ? 'carousel-item':null, $iterator->isFirst($numOfColumns) ? 'item-first':null, $iterator->isLast($numOfColumns) ? 'item-last':null, $authorAvatar ? 'image-present':null, ($authorBackground == "enable" && $authorBackgroundImage) ? 'author-bg':null, $boxAlign ? $boxAlign:null))) echo ' class="' . NTemplateHelpers::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT) . '"' ?>>

					<a href="<?php echo NTemplateHelpers::escapeHtml($author->postsUrl, ENT_COMPAT) ?>" rel="author" class="author-link">

<?php if ($authorBackground == 'enable' and $authorBackgroundImage) { ?>

<?php if (isset($meta->authorBackground) and $meta->authorBackground != '') { $backgroundUrl = $meta->authorBackground ;} else { $backgroundUrl = $options->theme->header->headbg->image ;} ?>

							<div class="author-background-wrap">
								<div class="author-background" style="background-image: url('<?php echo $backgroundUrl ?>');"></div>
							</div>

<?php } ?>

<?php if ($authorAvatar) { ?>
							<div class="item-thumbnail">
								<?php echo $author->avatar(150) ?>

<?php if ($authorPostsCount) { ?>
									<div class="author-posts-count">
										<span><?php echo NTemplateHelpers::escapeHtml($template->printf(_n('%s posts', '%s posts', 'wplatte'), count_user_posts($author->id)), ENT_NOQUOTES) ?></span>
									</div>
<?php } ?>
							</div>
<?php } ?>

						<div class="item-title">
							<h3><?php echo NTemplateHelpers::escapeHtml($author->displayName, ENT_NOQUOTES) ?></h3>
<?php if ($authorPostsCount and !$authorAvatar) { ?>
								<div class="author-posts-count">
									<span><?php echo NTemplateHelpers::escapeHtml($template->printf(_n('%s posts', '%s posts', 'wplatte'), count_user_posts($author->id)), ENT_NOQUOTES) ?></span>
								</div>
<?php } ?>
						</div>
					</a>

					<div class="item-text">
<?php if ($authorBio and $author->bio != '') { ?>
							<div class="item-excerpt">
								<p class="txtrows-<?php echo NTemplateHelpers::escapeHtml($textRows, ENT_COMPAT) ?>
"><?php echo $template->striptags(wp_trim_words(strip_shortcodes($author->bio), 200)) ?></p>
							</div>
<?php } if (isset($meta->socIcons) and !empty($meta->socIcons) and $authorSocial) { ?>
							<div class="item-icons">
								<ul class="author-icons">
<?php $iterations = 0; foreach ($meta->socIcons as $icon) { ?>
									<li>
										<a href="<?php echo NTemplateHelpers::escapeHtml($icon['url'], ENT_COMPAT) ?>">
<?php if ($icon['iconFont']) { ?>
												<i class="fa <?php echo NTemplateHelpers::escapeHtml($icon['iconFont'], ENT_COMPAT) ?>"></i>
<?php } else { ?>
												<?php if ($icon['icon']) { ?><img src="<?php echo NTemplateHelpers::escapeHtml($icon['icon'], ENT_COMPAT) ?>
" class="s-icon s-icon-light" alt="icon" /><?php } ?>

<?php } ?>
											<!-- <span class="s-title"><?php echo NTemplateHelpers::escapeHtmlComment(AitLangs::getCurrentLocaleText($icon['title'])) ?></span> -->
										</a>
									</li>
<?php $iterations++; } ?>
								</ul>
							</div>
<?php } ?>
					</div>
				</div>

<?php if ($enableCarousel and $iterator->isLast($numOfRows)) { ?>
					</div>
<?php } $iterations++; } array_pop($_l->its); $iterator = end($_l->its) ?>
			</div>
<?php } else { ?>
			<div data-cols="<?php echo NTemplateHelpers::escapeHtml($numOfColumns, ENT_COMPAT) ?>
" data-first="1" data-last="<?php echo NTemplateHelpers::escapeHtml(ceil(sizeof($authors) / $numOfRows), ENT_COMPAT) ?>
" data-arrow-left="<?php echo NTemplateHelpers::escapeHtml(__('previous', 'wplatte'), ENT_COMPAT) ?>
" data-arrow-right="<?php echo NTemplateHelpers::escapeHtml(__('next', 'wplatte'), ENT_COMPAT) ?>
"<?php if ($_l->tmp = array_filter(array('elm-item-organizer-container', "column-{$numOfColumns}", "layout-{$layout}", $enableCarousel ? 'carousel-container' : 'carousel-disabled',))) echo ' class="' . NTemplateHelpers::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT) . '"' ?>>
<?php $iterations = 0; foreach ($iterator = $_l->its[] = new NSmartCachingIterator($authors) as $author) { ?>

<?php $meta = (object)$author->meta('user-metabox') ?>

<?php if ($enableCarousel and $iterator->isFirst($numOfRows)) { ?>
					<div<?php if ($_l->tmp = array_filter(array('item-box', $enableCarousel ? 'carousel-item':null))) echo ' class="' . NTemplateHelpers::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT) . '"' ?>>
<?php } ?>
				<div data-id="<?php echo NTemplateHelpers::escapeHtml($iterator->counter, ENT_COMPAT) ?>
"<?php if ($_l->tmp = array_filter(array('item', "item{$iterator->counter}",	$enableCarousel ? 'carousel-item':null, $iterator->isFirst($numOfColumns) ? 'item-first':null, $iterator->isLast($numOfColumns) ? 'item-last':null, $authorAvatar ? 'image-present':null, ($authorBackground == "enable" && $authorBackgroundImage) ? 'author-bg':null))) echo ' class="' . NTemplateHelpers::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT) . '"' ?>>

					<a href="<?php echo NTemplateHelpers::escapeHtml($author->postsUrl, ENT_COMPAT) ?>" rel="author" class="author-link">

<?php if ($authorBackground == 'enable' and $authorBackgroundImage) { ?>

<?php if (isset($meta->authorBackground) and ($meta->authorBackground != '')) { $backgroundUrl = $meta->authorBackground ;} else { $backgroundUrl = $options->theme->header->headbg->image ;} ?>

							<div class="author-background-wrap">
								<div class="author-background" style="background-image: url('<?php echo $backgroundUrl ?>');"></div>
							</div>

<?php } ?>

<?php if ($authorAvatar) { ?>
							<div class="item-thumbnail">
								<?php echo $author->avatar(150) ?>

<?php if ($authorPostsCount) { ?>
									<div class="author-posts-count">
										<span><?php echo NTemplateHelpers::escapeHtml($template->printf(_n('%s posts', '%s posts', 'wplatte'), count_user_posts($author->id)), ENT_NOQUOTES) ?></span>
									</div>
<?php } ?>
							</div>
<?php } ?>

						<div class="item-title">
							<h3><?php echo NTemplateHelpers::escapeHtml($author->displayName, ENT_NOQUOTES) ?></h3>
<?php if ($authorPostsCount and !$authorAvatar) { ?>
									<div class="author-posts-count">
										<span><?php echo NTemplateHelpers::escapeHtml($template->printf(_n('%s posts', '%s posts', 'wplatte'), count_user_posts($author->id)), ENT_NOQUOTES) ?></span>
									</div>
<?php } ?>
						</div>
					</a>

					<div class="item-text">
<?php if ($authorBio and $author->bio != '') { ?>
							<div class="item-excerpt">
								<p class="txtrows-<?php echo NTemplateHelpers::escapeHtml($textRows, ENT_COMPAT) ?>
"><?php echo $template->striptags(wp_trim_words(strip_shortcodes($author->bio), 200)) ?></p>
							</div>
<?php } ?>

<?php if (isset($meta->socIcons) and !empty($meta->socIcons) and $authorSocial) { ?>
							<div class="item-icons">
								<ul class="author-icons">
<?php $iterations = 0; foreach ($meta->socIcons as $icon) { ?>
									<li>
										<a href="<?php echo NTemplateHelpers::escapeHtml($icon['url'], ENT_COMPAT) ?>">
<?php if ($icon['iconFont']) { ?>
												<i class="fa <?php echo NTemplateHelpers::escapeHtml($icon['iconFont'], ENT_COMPAT) ?>"></i>
<?php } else { ?>
												<?php if ($icon['icon']) { ?><img src="<?php echo NTemplateHelpers::escapeHtml($icon['icon'], ENT_COMPAT) ?>
" class="s-icon s-icon-light" alt="icon" /><?php } ?>

<?php } ?>
											<!-- <span class="s-title"><?php echo NTemplateHelpers::escapeHtmlComment(AitLangs::getCurrentLocaleText($icon['title'])) ?></span> -->
										</a>
									</li>
<?php $iterations++; } ?>
								</ul>
							</div>
<?php } ?>
					</div>
				</div>

<?php if ($enableCarousel and $iterator->isLast($numOfRows)) { ?>
					</div>
<?php } $iterations++; } array_pop($_l->its); $iterator = end($_l->its) ?>
			</div>
<?php } } else { ?>
		<div class="elm-item-organizer-container">
			<div class="alert alert-info">
				<?php echo NTemplateHelpers::escapeHtml(_x('Authors', 'name of element', 'wplatte'), ENT_NOQUOTES) ?>
&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo NTemplateHelpers::escapeHtml(__('Info: There are no items created, add some please.', 'wplatte'), ENT_NOQUOTES) ?>

			</div>
		</div>
<?php } ?>
</div>

<?php NCoreMacros::includeTemplate(WpLatteMacros::getTemplatePart("ait-theme/elements/authors/javascript", ""), array('enableCarousel' => $enableCarousel) + get_defined_vars(), $_l->templates['7ue6rmbd9d'])->render() ;