<?php




function register_portal_user() {
	if(!empty($_REQUEST['ait-action'])){
		if($_REQUEST['ait-action'] == 'register'){
			$redirect = !empty($_POST['redirect_to']) ? $_POST['redirect_to'] : home_url('/');

			if(!empty($_POST['user_login']) && !empty($_POST['user_email'])){
				if(username_exists( $_POST['user_login'] ) == null && email_exists( $_POST['user_email'] ) == false){
					// register user automatically
					$user_data = array(
						'user_login'	=> $_POST['user_login'],
						'user_pass'		=> wp_generate_password(),
						'user_email'	=> $_POST['user_email'],
						'role'			=> aitOptions()->get('theme')->authors->defaultUserRole,
					);
					$user_id = wp_insert_user( $user_data ) ;

					global $wp_version;
					if(version_compare($wp_version, "4.3",">=")){
						wp_new_user_notification($user_id, null, 'both');
					} else {
						wp_new_user_notification($user_id, $user_data['user_pass']);
					}

					$redirect = $redirect.'/'.'?ait-notification=user-registration-success';
				} else {
					$redirect = $redirect.'/'.'?ait-notification=user-registration-exists';
				}
			} else {
				$redirect = $redirect.'/'.'?ait-notification=user-registration-error';
			}

			wp_safe_redirect( $redirect );
			exit();
		}

	}
}
add_action('after_setup_theme', 'register_portal_user');



function aitFrontendNotices(){
	$currentNotice = false;
	$notices = array(
		'user-registration-success' => array(
			"icon"	=> 'fa fa-check-circle',
			"type"	=> "success",
			"msg"	=> __('User successfully registered, email was sent to your email address.', 'ait'),
		),
		'user-registration-exists' => array(
			"icon"	=> 'fa fa-exclamation-circle',
			"type"	=> "warning",
			"msg"	=> __('Username or email already registered', 'ait'),
		),
		'user-registration-error' => array(
			"icon"	=> 'fa fa-times-circle',
			"type"	=> "error",
			"msg"	=> __('There was an error during registration, please check name and email. If the problem persists, please contact website administrator', 'ait'),
		),
		'user-login-failed' => array(
			"icon"	=> 'fa fa-times-circle',
			"type"	=> "error",
			"msg"	=> __('System login failed, please check your username and password', 'ait'),
		),
	);

	if(isset($_REQUEST['ait-notification']) && !empty($_REQUEST['ait-notification'])){
		$currentNotice = $_REQUEST['ait-notification'];
	}

	if(isset($notices[$currentNotice])){
	?>
		<div class="frontend-notification <?php echo esc_attr($notices[$currentNotice]['type']); ?>">
			<div class="grid-main">
				<i class="<?php echo esc_attr($notices[$currentNotice]['icon']); ?>"></i>
				<span><?php echo $notices[$currentNotice]['msg']; ?></span>
				<i class="fa fa-times"></i>
			</div>
			<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('.frontend-notification').addClass('shown');

				jQuery('.frontend-notification i.fa-times').on('click', function(){
					jQuery('.frontend-notification').addClass('hidden');
					setTimeout(function(){
						jQuery('.frontend-notification').remove();
					},2000);
				});
			});
			</script>
		</div>
	<?php
	}
}
add_action( 'ait-html-body-begin', 'aitFrontendNotices', 12, 0);


add_action( 'wp_login_failed', 'aitFrontendLoginFail' );  // hook failed login
function aitFrontendLoginFail( $username ) {
	$referrer = $_SERVER['HTTP_REFERER'];
	if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
		$referrer = add_query_arg('ait-notification', 'user-login-failed', $referrer);
		wp_redirect( $referrer );
		exit;
	}
}