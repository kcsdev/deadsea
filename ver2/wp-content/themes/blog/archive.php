{block content}

	{* template for page title is in parts/page-title.php *}

	{if $wp->isAuthor and $author->bio}
		{includePart parts/author-bio, pageTitle => false}
	{/if}

	{if $wp->havePosts}

		{loop as $post}
			{includePart parts/post-content}
		{/loop}

		{includePart parts/pagination, location => pagination-below}

	{else}
		{if $wp->isAuthor}
			{includePart parts/none, message => no-author-posts}
		{else}
			{includePart parts/none, message => no-posts}
		{/if}
	{/if}
