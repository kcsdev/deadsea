
console.log(map_markers);

// Escaple HTML
 var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };

  function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return entityMap[s];
    });
  }

// Checkboxes
/*var sub1 = {'id':'monumentoCheckbox','group':'monumento','checked':'checked','display':'Beaches'};
var sub2 = {'id':'museusCheckbox','group':'museus','checked':'checked','display':'Parks'};
var sub3 = {'id':'restaurantesCheckbox','group':'restaurantes','checked':'checked','display':'Clubs'};
var sub4 = {'id':'hotelCheckbox','group':'hotel','checked':'checked','display':'Hotels'};
var sub_categories = [sub1,sub2,sub3,sub4];*/

// Markers & Categories declarations
var all_markers = [];
var sub_categories = {};
var markerGroups = [];
jQuery.each( map_markers, function( id, mark ) {
    if(mark.map.map.address.length > 0){
 all_markers.push({'name':escapeHtml(mark.name),'address':escapeHtml(mark.map.map.address),'lat':mark.map.map.latitude,'lng':mark.map.map.longitude,'type':mark.category[0].slug,post_id:id});
    sub_categories[mark.category[0].cat_ID] = {'id':mark.category[0].cat_ID,'group':mark.category[0].slug,'checked':'checked','display':escapeHtml(mark.category[0].name)};
        markerGroups[mark.category[0].slug] = [];
    }
});
console.log(sub_categories);
//console.log(all_markers);

// Checkboxes div
var checkboxes = '';
jQuery.each(sub_categories,function(i,sub){
    checkboxes += '<input id="'+sub.id+'" type="checkbox" onclick="toggleGroup(&#39;'+sub.group+'&#39;)" checked="'+sub.checked+'">'+sub.display+'</input>';
});

// Markers div
var div_markers = '';
jQuery.each(all_markers,function(i,mark){
    div_markers += '<marker name="'+mark.name+'" address="'+mark.address+'" lat="'+mark.lat+'" lng="'+mark.lng+'" type="'+mark.type+'"  post_id="'+mark.post_id+'"/>';
});
//console.log(div_markers);


// Append the menu
jQuery( document ).ready(function() {
    jQuery('.siderbarmap ul').append(checkboxes);
});

// Google map functions
function xmlParse(str) {
    if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.loadXML(str);
        return doc;
    }

    if (typeof DOMParser != 'undefined') {
        return (new DOMParser()).parseFromString(str, 'text/xml');
    }

    return createElement('div', null);
}
var infoWindow = new google.maps.InfoWindow();
var customIcons = {
    monumento: {
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png'
    },
    hotel: {
        icon: 'http://maps.google.com/mapfiles/ms/icons/green.png'
    },
    restaurantes: {
        icon: 'http://maps.google.com/mapfiles/ms/icons/yellow.png'
    },
    museus: {
        icon: 'http://maps.google.com/mapfiles/ms/icons/purple.png'
    }
};

/*var markerGroups = {
    "museus": [],
    "monumentos": [],
    "restaurantes": [],
    "hotel": []
};*/

function load() {
    var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(31.5368743, 35.3704775),
        zoom: 11,
        mapTypeId: 'roadmap'
    });
    var infoWindow = new google.maps.InfoWindow();



    map.set('styles', [{
        zoomControl: false
    }, {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{
            color: "#ffd986"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "geometry.fill",
        stylers: [{
            color: "#9e574f"
        }]
    }, {
        featureType: "road.local",
        elementType: "geometry.fill",
        stylers: [{
            color: "#d0cbc0"
        }, {
            weight: 1.1
        }

        ]
    }, {
        featureType: 'road',
        elementType: 'labels',
        stylers: [{
            saturation: -100
        }]
    }, {
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [{
            hue: '#ffff00'
        }, {
            gamma: 1.4
        }, {
            saturation: 82
        }, {
            lightness: 96
        }]
    }, {
        featureType: 'poi.school',
        elementType: 'geometry',
        stylers: [{
            hue: '#fff700'
        }, {
            lightness: -15
        }, {
            saturation: 99
        }]
    }]);

    //         downloadUrl("markers.xml", function (data) {
    var xml = xmlParse('<markers>'+div_markers+'</markers>');
    // var xml = data.responseXML;
    var markers = xml.documentElement.getElementsByTagName("marker");
    for (var i = 0; i < markers.length; i++) {
        var name = markers[i].getAttribute("name");
        var address = markers[i].getAttribute("address");
        var type = markers[i].getAttribute("type");
        var post_id = parseFloat(markers[i].getAttribute("post_id"));
        
        var point = new google.maps.LatLng(
        parseFloat(markers[i].getAttribute("lat")),
        parseFloat(markers[i].getAttribute("lng")));
        var html = "<h3>" + name + "</h3><h4>" + address +"</h4><p style='max-width:200px;'>"+map_markers[post_id].excerpt+"</p><img src='"+map_markers[post_id].image[0]+"'><br><a href='"+ map_markers[post_id].link+"'>Read More...";
        
        // var icon = customIcons[type] || {};
        var marker = createMarker(point, name, address, type, map);
        bindInfoWindow(marker, map, infoWindow, html);
    }
    // });
}

function createMarker(point, name, address, type, map) {
    var icon = customIcons['hotel'] || {};
    var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: icon.icon,
        // shadow: icon.shadow,
        type: type
    });
    if (!markerGroups[type]) markerGroups[type] = [];
    markerGroups[type].push(marker);
    //var html = "";
    //bindInfoWindow(marker, map, infoWindow, html);
    return marker;
}

function toggleGroup(type) {
    for (var i = 0; i < markerGroups[type].length; i++) {
        var marker = markerGroups[type][i];
        if (!marker.getVisible()) {
            marker.setVisible(true);
        } else {
            marker.setVisible(false);
        }
    }
}

function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);

    });
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };

    request.open('GET', url, true);
    request.send(null);
}

function doNothing() {}
google.maps.event.addDomListener(window, 'load', load);