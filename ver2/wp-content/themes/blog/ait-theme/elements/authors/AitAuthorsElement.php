<?php

/*
 * AIT WordPress Theme Framework
 *
 * Copyright (c) 2013, 2014 Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */



class AitAuthorsElement extends AitElement
{
	public function getHtmlClasses($asString = true)
	{
		$classes = parent::getHtmlClasses(false);
		$classes[] = 'elm-item-organizer-main';

		return $asString ? implode(' ', $classes) : $classes;
	}
}
