<script id="{$htmlId}-container-script">

	jQuery(window).load(function(){
		var map;
		var mapDiv = jQuery("#{!$htmlId}-container");

		var styles = [
			{
				stylers: [
					{ hue: "{!$el->option(mapHue)}" },
					{ saturation: "{!$el->option(mapSaturation)}" },
					{ lightness: "{!$el->option(mapBrightness)}" },
				]
			},
			{ featureType: "landscape", stylers: [
					{ hue: "{!$el->option(landscapeColor)}"},
					{ saturation: "{if $el->option(landscapeColor) != ''} {!$el->option(objSaturation)} {/if}"},
					{ lightness: "{if $el->option(landscapeColor) != ''} {!$el->option(objBrightness)} {/if}"},
				]
			},
			{ featureType: "administrative", stylers: [
					{ hue: "{!$el->option(administrativeColor)}"},
					{ saturation: "{if $el->option(administrativeColor) != ''} {!$el->option(objSaturation)} {/if}"},
					{ lightness: "{if $el->option(administrativeColor) != ''} {!$el->option(objBrightness)} {/if}"},
				]
			},
			{ featureType: "road", stylers: [
					{ hue: "{!$el->option(roadsColor)}"},
					{ saturation: "{if $el->option(roadsColor) != ''} {!$el->option(objSaturation)} {/if}"},
					{ lightness: "{if $el->option(roadsColor) != ''} {!$el->option(objBrightness)} {/if}"},
				]
			},
			{ featureType: "water", stylers: [
					{ hue: "{!$el->option(waterColor)}"},
					{ saturation: "{if $el->option(waterColor) != ''} {!$el->option(objSaturation)} {/if}"},
					{ lightness: "{if $el->option(waterColor) != ''} {!$el->option(objBrightness)} {/if}"},
				]
			},
			{ featureType: "poi", stylers: [
					{ hue: "{!$el->option(poiColor)}"},
					{ saturation: "{if $el->option(poiColor) != ''} {!$el->option(objSaturation)} {/if}"},
					{ lightness: "{if $el->option(poiColor) != ''} {!$el->option(objBrightness)} {/if}"},
				]
			},
		];

		mapDiv.gmap3({
			map:{
				{if $wp->isSingular(post)}
					{!$latLngCode}
				{else}
					{!$addressCode}
				{/if}
				options:{
					{!$centerCode}
					mapTypeId: google.maps.MapTypeId.{!$el->option(type)},
					zoom: {!$el->option(zoom)},
					scrollwheel: {!$scrollWheel},
					styles: styles,
				}
			},
			marker:{
				values:[
					{var $markers = $el->option(markers)}
					{if empty($markers)} {* cast empty string to empty array for cycle *}
						{var $markers = array()}
					{/if}
					//loop custom map markers
					{foreach $markers as $mark}
						{
							address: "{!$mark[address]}",
							data: {if $mark[url] != ""}'<div class="gmap-infowindow-content"><a href="{!$mark[url]}">'+
							'<h3>{if strlen($mark[title]) > 70}{!htmlspecialchars(mb_substr($mark[title], 0, 70), ENT_QUOTES)}...{else}{!$mark[title]}{/if}</h3>'+
							'<p>{!htmlspecialchars(mb_substr($mark[description], 0, 75), ENT_QUOTES)}</p></a></div>'
									{else}'<div class="gmap-infowindow-content">'+
									'<h3>{if strlen($mark[title]) > 70}{!mb_substr($mark[title], 0, 70)}...{else}{!$mark[title]}{/if}</h3>'+
									'<p>{!mb_substr($mark[description], 0, 75)}</p></div>'{/if},
							{if $mark[icon] != ""}
							options:
							{
								icon: "{!$mark[icon]}"
							}
							{/if}
						},
					{/foreach}

					// loop wordpress posts
					{foreach $items as $item}
						{var $meta = $item->meta('post-gps-metabox')}
						{if !is_object($meta)}
 							{? continue }
						{/if}
						{var $meta = $meta->map}
						{if $meta['latitude'] == "0" and $meta['longitude'] == "0"}
 							{? continue }
						{/if}
						{
							lat: {$meta['latitude']}, lng: {$meta['longitude']},
							data: '<div class="gmap-infowindow-content"><a href="{!$item->permalink}">'+
							'{if $item->hasImage}<span><img src="{imageUrl $item->imageUrl, width => 60, height => 60, crop => 1}" alt="{!$item->title}"></span>{/if}'+
							'<h3>{if strlen($item->title) > 45}{!mb_substr($item->title, 0, 45)}...{else}{!$item->title}{/if}</h3>'+
							'<span class="date">{!$item->date('c')|dateI18n: 'j. M Y'}</span></a></div>',
							{if $el->option(icon) != ""}
							options:
							{
								icon: "{!$el->option(icon)}",
							},
							{/if}
							id: "marker-{$item->id}",
						},
					{/foreach}

				],
				cluster:{
					radius: 50,
					0: {
						content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
						width: 53,
						height: 53
					},
					events: {
						click: function(cluster) {
							var map = jQuery(this).gmap3("get");
							map.panTo(cluster.main.getPosition());
							map.setZoom(map.getZoom() + 2);
						}
					}
				},
				options:{
					draggable: false
				},
				events:{
					click: function(marker, event, context){
						var map = jQuery(this).gmap3("get");

						/* Remove All previous infoboxes */
						mapDiv.find('.infoBox').remove();

						if(context.data != "disabled"){

							var infoBoxOptions = {
								content: context.data,
								disableAutoPan: false,
								pixelOffset: new google.maps.Size(-117, -130),
								zIndex: 99,
								boxStyle: {
									background: "#FFFFFF",
									opacity: 1,
									width: "265px",
									height: "80px"
								},
								closeBoxMargin: "2px 2px 2px 2px",
								closeBoxURL: "{!aitPaths()->url->img}/infobox_close.png",
								infoBoxClearance: new google.maps.Size(1, 1),
								position: marker.position
							};

							var infoBox = new InfoBox(infoBoxOptions);
							infoBox.open(map, marker);
						}

						map.panTo(marker.getPosition());
					},
				},
			}
		});


		setTimeout(function(){
			checkTouchDevice();
			{if $displayPosts and $wp->isSingular(post)}
				var currMarker = mapDiv.gmap3({ get: { name: "marker", id: "marker-{$post->id}" }});
				google.maps.event.trigger(currMarker, 'click');
			{/if}
		},1000);


		{if $options->theme->general->progressivePageLoading}
			if(!isResponsive(1024)){
				jQuery("#{!$htmlId}").waypoint(function(){
					jQuery("#{!$htmlId}").parent().parent().addClass('load-finished');
				}, { triggerOnce: true, offset: "95%" });
			} else {
				jQuery("#{!$htmlId}").parent().parent().addClass('load-finished');
			}
		{else}
			jQuery("#{!$htmlId}").parent().parent().addClass('load-finished');
		{/if}

		var checkTouchDevice = function() {
			if (Modernizr.touch){
				map = mapDiv.gmap3("get");
				map.setOptions({ draggable : false });
				var draggableClass = 'inactive', draggableTitle = 'Activate map';
				var draggableButton = jQuery('<div class="draggable-toggle-button '+draggableClass+'">'+draggableTitle+'</div>').appendTo(mapDiv);

				draggableButton.click(function () {
					if(jQuery(this).hasClass('active')){
						jQuery(this).removeClass('active').addClass('inactive').text({__ 'Activate map'});
						map.setOptions({ draggable : false });
					} else {
						jQuery(this).removeClass('inactive').addClass('active').text({__ 'Deactivate map'});
						map.setOptions({ draggable : true });
					}
				});
			}
		}

	});

</script>
