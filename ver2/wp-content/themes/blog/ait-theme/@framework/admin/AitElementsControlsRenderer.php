<?php


class AitElementsControlsRenderer extends AitOptionsControlsRenderer
{
	/** @var  AitElement[] */
	protected $usedUnsortableElements = array();

	/** @var  AitElement[] */
	protected $usedSortableElements = array();

    /** @var  string[] */
    protected $usedElementsIds = array();

    /** @var  AitElement[] */
    protected $availableFullWidthElements = array();

    /** @var  AitElement[] */
    protected $availableColumnableElements = array();

	/** @var AitElementsManager */
	protected $em;



	/**
	 * Constructor
	 */
	public function __construct($params)
	{
		parent::__construct($params);
		$this->em = aitManager('elements');
		$this->prepareUsedElements();
		foreach(array_values($this->options) as $element){
			$this->usedElementsIds[key($element)] = true;
		}

		$this->prepareAvailableElements();
	}



	public function renderUsedUnsortableElements()
	{
		foreach($this->options as $i => $el){
			$elIds[key($el)] = $i;
		}

		$params = array();

		foreach($this->defaults as $i => $el){
			$elId = key($el);
			if(isset($elIds[$elId]) && isset($this->usedUnsortableElements[$elIds[$elId]])) {
				$el = $this->usedUnsortableElements[$elIds[$elId]];

				$params['htmlId'] = sanitize_key(sprintf("ait-%s-element-%s-%s", $this->isRenderingDefaultLayout ? 'global' : 'local', $el->id, $elIds[$elId]));
				$params['htmlClass'] = ' ait-used-element ';
				$params['clone'] = ($el->cloneable and !$this->isRenderingDefaultLayout);

				if(!$el->isDisplay() or !apply_filters("ait-allow-render-controls-{$this->configType}-{$elId}", true, $this->oid)){
					$params['htmlClass'] .= ' ait-element-off';
				}

				$this->renderElement($el, (object) $params);
			}
		}
	}



	public function renderUsedSortableElementsHandlers()
	{
		foreach ($this->usedSortableElements as $element) {
			if ($this->em->isElementSidebarsBoundary($element->getId())) {
				$this->renderSidebarsBoundaryElement($element);
				continue;
			}

			$this->renderElementHandler($element);
		}
	}



	public function renderUsedSortableElementsContents()
	{
		foreach ($this->usedSortableElements as $element) {
			if ($this->em->isElementSidebarsBoundary($element->getId())) {
				continue;
			}

			$this->renderElementContent($element);
		}
	}



	public function renderAvailableElementsHandlers()
	{
		$this->options = $this->defaults; // override current values of options with defaults
		?>

		<h3><?php _e('Fullwidth Elements', 'ait-admin') ?></h3>
		<div id="ait-available-elements-not-droppable-to-columns">
			<?php
			foreach($this->availableFullWidthElements as $element){
				$this->renderElementHandler($element);
			}
			?>
		</div>

		<h3><?php _e('Fullwidth or Columnable Elements', 'ait-admin') ?></h3>
		<div id="ait-available-elements-droppable-to-columns">
			<?php
			foreach($this->availableColumnableElements as $element){
				$this->renderElementHandler($element);
			}
			?>
		</div>
	<?php
	}



	public function renderAvailableElementsContents()
	{
		$availableElements = array_merge($this->availableFullWidthElements, $this->availableColumnableElements);

		foreach ($availableElements as $element) {
			$this->renderElementContent($element);
		}
	}



	protected function renderElementHandler(AitElement $element)
	{
		$htmlElementId = sanitize_key(sprintf("ait-%s-element-%s-__%s__", $this->isRenderingDefaultLayout ? 'global' : 'local', $element->getId(), $element->getOptionsControlsGroup()->getIndex()));
		$htmlElementContentId = $htmlElementId . '-content';
		$htmlDataClone = $element->isCloneable() && !$this->isRenderingDefaultLayout;
		$htmlClass = $element->isUsed() ? 'ait-used-element' : 'ait-available-element';
		if(!$element->isDisplay() or !apply_filters("ait-allow-render-controls-{$this->configType}-{$element->getId()}", true, $this->oid)) $htmlClass .= ' ait-element-off';
		if($element->isColumnable()) $htmlClass .= ' ait-element-columnable';
		if($element->option('@columns-element-index')) $htmlClass .= ' hidden in-column';
		if($element->isDisabled()) $htmlClass .= ' ait-element-disabled';
		if($element->getOptionsControlsGroup()->getIndex() == AitElement::UNDEFINED_INDEX && ((isset($this->usedElementsIds[$element->getId()]) and !$element->isCloneable() and !$this->isRenderingDefaultLayout) or (isset($this->usedElementsIds[$element->getId()]) and $this->isRenderingDefaultLayout))) {
			$htmlClass .= ' hidden';
		}
		if(AIT_THEME_PACKAGE == 'standard' and $element->isDisabled()) $htmlClass .= ' hidden';

		?>


		<div id="<?php echo $htmlElementId ?>" class="ait-element <?php echo $htmlClass ?>"
			<?php
			echo aitDataAttr('element', array('type' => $element->getId(), 'clone' => $htmlDataClone, 'global' => $this->isRenderingDefaultLayout));
			echo aitDataAttr('element-id', $htmlElementId);
			echo aitDataAttr('element-content-id', $htmlElementContentId);
			echo aitDataAttr('columns-element-index', $element->option('@columns-element-index'));
			echo aitDataAttr('columns-element-column-index', $element->option('@columns-element-column-index'));
			?>
			>
			<div class="ait-element-handler">
				<div class="ait-element-actions">
					<!-- <a class="ait-element-help" href="#">?</a> -->
					<a class="ait-element-close" href="#">&#9644;</a>
					<?php if($element->getId() != 'content' and $element->getId() != 'comments' and $element->isSortable()): ?>
						<a class="ait-element-remove" href="#">&#10006;</a>
					<?php endif; ?>
				</div>
				<?php
				if ($element->hasOption('@element-user-description') && $element->option('@element-user-description') != '') {
					$elementUserDescription = $element->option('@element-user-description');
					$elementUserDescriptionCssClass = ' element-has-user-description';
				} else {
					$elementUserDescription = '';
					$elementUserDescriptionCssClass = '';
				}
				?>
				<div class="ait-element-title">
                    <h4><?php esc_html_e($element->getTitle()); ?></h4>
					<span class="ait-element-user-description<?php echo $elementUserDescriptionCssClass; ?>" title="<?php _e('Edit element description', 'ait-admin'); ?>"><?php echo $elementUserDescription ?></span>
				</div>
			</div>
		</div>
	<?php
	}



	protected function renderElementContent(AitElement $element)
	{
		$htmlElementId = sanitize_key(sprintf("ait-%s-element-%s-__%s__", $this->isRenderingDefaultLayout ? 'global' : 'local', $element->getId(), $element->getOptionsControlsGroup()->getIndex()));
		$htmlId = $htmlElementId . '-content';
		if ($element->getOptionsControlsGroup()->getIndex() == AitElement::UNDEFINED_INDEX) $htmlId .= '-prototype';
		?>
		<div id="<?php echo $htmlId; ?>" class="ait-element-content" <?php echo aitDataAttr('element-id', $htmlElementId); ?>>
			<div class="ait-element-controls">
				<?php
				$this->renderOptionsControlsGroup($element->getOptionsControlsGroup())
				?>
			</div>
		</div>
	<?php
	}



	protected function renderElement(AitElement $el, $params)
	{
		?>
		<div
			id="<?php echo $params->htmlId ?>"
			class="ait-element <?php echo $params->htmlClass ?>"
			<?php
			echo aitDataAttr('element', array('type' => $el->id, 'clone' => $params->clone, 'global' => $this->isRenderingDefaultLayout));
			echo aitDataAttr('element-id', $params->htmlId);
			echo aitDataAttr('element-content-id', $params->htmlId . '-content');
			echo aitDataAttr('columns-element-index', $el->option('@columns-element-index'));
			echo aitDataAttr('columns-element-column-index', $el->option('@columns-element-column-index'));
			?>
			>
			<div class="ait-element-handler">
				<div class="ait-element-actions">
					<!-- <a class="ait-element-help" href="#">?</a> -->
					<a class="ait-element-close" href="#">&#9644;</a>
					<?php if($el->id != 'content' and $el->id != 'comments' and $el->sortable): ?>
						<a class="ait-element-remove" href="#">&#10006;</a>
					<?php endif; ?>
				</div>
				<div class="ait-element-title"><h4><?php esc_html_e($el->getTitle()); ?></h4></div>
			</div>

			<div id="<?php echo $params->htmlId;?>-content" class="ait-element-content" <?php echo aitDataAttr('element-id', $params->htmlId); ?>>

				<div class="ait-element-controls">
					<?php
					$this->renderOptionsControlsGroup($el->getOptionsControlsGroup());
					?>
				</div>
			</div>
		</div>
	<?php
	}



	private function renderSidebarsBoundaryElement(AitElement $el)
	{
		$index = $el->getOptionsControlsGroup()->getIndex();

		$htmlElementId = sanitize_key(sprintf("ait-%s-element-%s-__%d__", $this->isRenderingDefaultLayout ? 'global' : 'local', $el->id, $index));
		$htmlElementContentId = $htmlElementId . '-content';
		?>
		<div id="<?php echo $htmlElementId ?>" class="ait-element ait-used-element ait-sidebars-boundary ait-<?php echo $el->id ?>"
			<?php
			echo aitDataAttr('element', array('type' => $el->id, 'clone' => false, 'global' => true));
			echo aitDataAttr('element-id', $htmlElementId);
			echo aitDataAttr('element-content-id', $htmlElementContentId);
			?>
			>
			<div class="ait-element-handler">
				<div class="ait-element-title">
					<h4>
						<?php printf(__('Sidebars <strong>%s</strong> here', 'ait-admin'), ($el->id == 'sidebars-boundary-start') ? __('start', 'ait-admin') : __('end', 'ait-admin')) ?>
					</h4>
				</div>
			</div>
			<div id="<?php echo $htmlElementId; ?>-content" class="ait-element-content" <?php echo aitDataAttr('element-id', $htmlElementContentId); ?>>
				<?php

				$sections = $el->getOptionsControlsGroup()->getSections();

				/** @var AitOptionsControlsSection $section */
				$section = reset($sections);
				$option = $section->getOptionControl($el->getId());

				echo $option->getHtml();
				?>
			</div>
		</div>
	<?php
	}



	private function prepareUsedElements()
	{
		$usedElements = $this->em->createElementsFromOptions($this->options, $this->oid);

		foreach ($usedElements as $index => $element) {
			if (!isset($this->em->prototypes[$element->getId()]) || $element->isDisabled()) {
				continue;
			}

			$element->setUsed(true);

			if ($element->isSortable()) {
				$this->usedSortableElements[(string)$index] = $element;
			} else {
				$this->usedUnsortableElements[(string)$index] = $element;
			}
		}
	}



	private function prepareAvailableElements()
	{
		$availableElements = $this->em->createElementsFromOptions($this->defaults, $this->oid);
		$oldLC = setlocale(LC_COLLATE, "0"); // get current lc_collate

		if(PHP_OS === 'WINNT'){
			setlocale(LC_COLLATE, null); // set system's lc_collate for Windows
		}else{
			$l = get_locale();
			setlocale(LC_COLLATE, "$l.UTF8", "$l.UTF-8"); // set lc_collate for unix systems
		}

		$sort = create_function('$a, $b', 'return strcoll($a->getTitle(), $b->getTitle());');
		usort($availableElements, $sort);

		setlocale(LC_COLLATE, $oldLC); // restore old lc_collate

		foreach ($availableElements as $index => $el) {
			$el->getOptionsControlsGroup()->setIndex(AitElement::UNDEFINED_INDEX);

			if ($this->em->isElementSidebarsBoundary($el->getId())) {
				continue;
			}

			if ($el->isColumnable()){
				$this->availableColumnableElements[$index] = $el;
			} else {
				$this->availableFullWidthElements[$index] = $el;
			}
		}
	}


}
