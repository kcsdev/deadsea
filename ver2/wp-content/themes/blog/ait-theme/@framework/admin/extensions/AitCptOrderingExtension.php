<?php
/*
Plugin Name: Simple Page Ordering
Version: 2.2
Author: Jake Goldman, 10up
Author URI: http://10up.com
License: GPLv2 or later
*/



class AitCptOrderingExtension extends AitPageOrderingExtension
{

	public static function register()
	{
		new self;
	}



	public function check($postType)
	{
		// This class supports only AIT Toolkit CPTs

		$isAitCpt = false;
		$cpts = get_post_types(array('ait-cpt' => true));
		$isAitCpt = in_array($postType, $cpts);

		if(!$isAitCpt or ($isAitCpt and !post_type_supports($postType, 'page-attributes'))){
			return false;
		}

		if(!$this->checkEditOthersCaps($postType)){
			return false;
		}

		// hack to enable ordering on non hierarchical CPTs, which our CPTs from AIT Toolkit most are
		add_action('parse_query', array($this, 'parseQuery'));

		return true;
	}



	public function parseQuery($q)
	{
		$q->query['orderby'] = "menu_order title";
		$q->query_vars['orderby'] = "menu_order title";
	}



	public function ajaxPageOrdering()
	{
		// check and make sure we have what we need
		if ( empty( $_POST['id'] ) || ( !isset( $_POST['previd'] ) && !isset( $_POST['nextid'] ) ) ) {
			die(-1);
		}

		// real post?
		if ( ! $post = get_post( $_POST['id'] ) ) {
			die(-1);
		}

		// does user have the right to manage these post objects?
		if ( ! $this->checkEditOthersCaps( $post->post_type ) ) {
			die(-1);
		}

		// badly written plug-in hooks for save post can break things
		// if ( !defined( 'WP_DEBUG' ) || !WP_DEBUG ) {
		// 	@error_reporting( 0 );
		// }

		$previd = empty( $_POST['previd'] ) ? false : (int) $_POST['previd'];
		$nextid = empty( $_POST['nextid'] ) ? false : (int) $_POST['nextid'];
		$start = empty( $_POST['start'] ) ? 1 : (int) $_POST['start'];
		$excluded = empty( $_POST['excluded'] ) ? array( $post->ID ) : array_filter( (array) $_POST['excluded'], 'intval' );

		$new_pos = array(); // store new positions for ajax
		$return_data = new stdClass;

		$nextid = false;

		$max_sortable_posts = 50;

		// we need to handle all post stati, except trash (in case of custom stati)
		$post_stati = get_post_stati(array(
			'show_in_admin_all_list' => true,
		));

		$siblings = new WP_Query(array(
			'depth'						=> 1,
			'posts_per_page'			=> $max_sortable_posts,
			'post_type' 				=> $post->post_type,
			'post_status' 				=> $post_stati,
			'orderby' 					=> 'menu_order title',
			'order' 					=> 'ASC',
			'post__not_in'				=> $excluded,
			'update_post_term_cache'	=> false,
			'update_post_meta_cache'	=> false,
			'suppress_filters' 			=> true,
			'ignore_sticky_posts'		=> true,
		)); // fetch all the siblings (relative ordering)

		// don't waste overhead of revisions on a menu order change (especially since they can't *all* be rolled back at once)
		remove_action( 'pre_post_update', 'wp_save_post_revision' );

		foreach( $siblings->posts as $sibling ) :

			// don't handle the actual post
			if ( $sibling->ID === $post->ID )
				continue;

			// if this is the post that comes after our repositioned post, set our repositioned post position and increment menu order
			if ( $nextid === $sibling->ID ) {
				wp_update_post(array(
					'ID'			=> $post->ID,
					'menu_order'	=> $start,
				));
				$new_pos[$post->ID] = array(
					'menu_order'	=> $start,
				);
				$start++;
			}

			// if repositioned post has been set, and new items are already in the right order, we can stop
			if ( isset( $new_pos[$post->ID] ) && $sibling->menu_order >= $start ) {
				$return_data->next = false;
				break;
			}

			// set the menu order of the current sibling and increment the menu order
			if ( $sibling->menu_order != $start ) {
				wp_update_post(array(
					'ID' 			=> $sibling->ID,
					'menu_order'	=> $start,
				));
			}
			$new_pos[$sibling->ID] = $start;
			$start++;

			if ( !$nextid && $previd == $sibling->ID ) {
				wp_update_post(array(
					'ID' 			=> $post->ID,
					'menu_order' 	=> $start,
				));
				$new_pos[$post->ID] = array(
					'menu_order'	=> $start,
				);
				$start++;
			}

		endforeach;

		// max per request
		if ( !isset( $return_data->next ) && $siblings->max_num_pages > 1 ) {
			$return_data->next = array(
				'id' 		=> $post->ID,
				'previd' 	=> $previd,
				'nextid' 	=> $nextid,
				'start'		=> $start,
				'excluded'	=> array_merge( array_keys( $new_pos ), $excluded ),
			);
		} else {
			$return_data->next = false;
		}

		if ( ! $return_data->next ) {
			// if the moved post has children, we need to refresh the page (unless we're continuing)
			$children = get_posts(array(
				'numberposts'				=> 1,
				'post_type' 				=> $post->post_type,
				'post_status' 				=> $post_stati,
				'post_parent' 				=> $post->ID,
				'fields'					=> 'ids',
				'update_post_term_cache'	=> false,
				'update_post_meta_cache'	=> false,
			));

			if ( ! empty( $children ) ) {
				die( 'children' );
			}
		}

		$return_data->new_pos = $new_pos;

		die( json_encode( $return_data ) );
	}



	public function sortByOrderLink($views)
	{
		return $views;
	}
}
