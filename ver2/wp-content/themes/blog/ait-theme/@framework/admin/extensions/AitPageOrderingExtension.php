<?php
/*
Plugin Name: Simple Page Ordering
Version: 2.2
Author: Jake Goldman, 10up
Author URI: http://10up.com
License: GPLv2 or later
 */



class AitPageOrderingExtension
{

	public static function register()
	{
		new self;
	}



	public function __construct()
	{
		if(!has_action('load-edit.php', array($this, 'loadEditScreen'))){
			add_action('load-edit.php', array($this, 'loadEditScreen'));
		}

		if(!has_action('wp_ajax_simple_page_ordering', array($this, 'ajaxPageOrdering'))){
			add_action('wp_ajax_simple_page_ordering', array($this, 'ajaxPageOrdering'));
		}
	}



	public function check($postType)
	{
		// The class support only Pages and all CPTs but not those from AIT Toolkit

		$isAitCpt = false;
		$cpts = get_post_types(array('ait-cpt' => true));
		$isAitCpt = in_array($postType, $cpts);

		$sortable = (post_type_supports($postType, 'page-attributes') or is_post_type_hierarchical($postType));

		if(!$sortable or $isAitCpt){
			return false;
		}

		if(!$this->checkEditOthersCaps($postType)){
			return false;
		}

		return true;
	}



	public function loadEditScreen()
	{
		$screen = get_current_screen();

		if(!$this->check($screen->post_type)){
			return;
		}

		add_filter('views_' . $screen->id, array($this, 'sortByOrderLink'));
		add_action('admin_enqueue_scripts', array($this, 'enqueueJs'));
	}



	public function enqueueJs()
	{
		if(AitUtils::contains(get_query_var('orderby'), 'menu_order')){	// we can only sort if we're organized by menu order
			wp_enqueue_script('simple-page-ordering', aitPaths()->url->admin . "/assets/js/page-ordering.js", array('jquery-ui-sortable'), '2.2', true);
			wp_enqueue_style('simple-page-ordering', aitPaths()->url->admin . "/assets/css/page-ordering.css", array(), '2.2');
		}
	}



	public function ajaxPageOrdering()
	{
		// check and make sure we have what we need
		if ( empty( $_POST['id'] ) || ( !isset( $_POST['previd'] ) && !isset( $_POST['nextid'] ) ) ) {
			die(-1);
		}

		// real post?
		if ( ! $post = get_post( $_POST['id'] ) ) {
			die(-1);
		}

		// does user have the right to manage these post objects?
		if ( ! $this->checkEditOthersCaps( $post->post_type ) ) {
			die(-1);
		}

		// badly written plug-in hooks for save post can break things
		// if ( !defined( 'WP_DEBUG' ) || !WP_DEBUG ) {
		// 	@error_reporting( 0 );
		// }

		$previd = empty( $_POST['previd'] ) ? false : (int) $_POST['previd'];
		$nextid = empty( $_POST['nextid'] ) ? false : (int) $_POST['nextid'];
		$start = empty( $_POST['start'] ) ? 1 : (int) $_POST['start'];
		$excluded = empty( $_POST['excluded'] ) ? array( $post->ID ) : array_filter( (array) $_POST['excluded'], 'intval' );

		$new_pos = array(); // store new positions for ajax
		$return_data = new stdClass;

		// attempt to get the intended parent... if either sibling has a matching parent ID, use that
		$parent_id = $post->post_parent;
		$next_post_parent = $nextid ? wp_get_post_parent_id( $nextid ) : false;

		if ( $previd == $next_post_parent ) {	// if the preceding post is the parent of the next post, move it inside
			$parent_id = $next_post_parent;
		} elseif ( $next_post_parent !== $parent_id ) {  // otherwise, if the next post's parent isn't the same as our parent, we need to study
			$prev_post_parent = $previd ? wp_get_post_parent_id( $previd ) : false;
			if ( $prev_post_parent !== $parent_id ) {	// if the previous post is not our parent now, make it so!
				$parent_id = ( $prev_post_parent !== false ) ? $prev_post_parent : $next_post_parent;
			}
		}
		// if the next post's parent isn't our parent, it might as well be false (irrelevant to our query)
		if ( $next_post_parent !== $parent_id )
			$nextid = false;

		$max_sortable_posts = 50;

		// we need to handle all post stati, except trash (in case of custom stati)
		$post_stati = get_post_stati(array(
			'show_in_admin_all_list' => true,
		));

		$siblings = new WP_Query(array(
			'depth'						=> 1,
			'posts_per_page'			=> $max_sortable_posts,
			'post_type' 				=> $post->post_type,
			'post_status' 				=> $post_stati,
			'post_parent' 				=> $parent_id,
			'orderby' 					=> 'menu_order title',
			'order' 					=> 'ASC',
			'post__not_in'				=> $excluded,
			'update_post_term_cache'	=> false,
			'update_post_meta_cache'	=> false,
			'suppress_filters' 			=> true,
			'ignore_sticky_posts'		=> true,
		)); // fetch all the siblings (relative ordering)

		// don't waste overhead of revisions on a menu order change (especially since they can't *all* be rolled back at once)
		remove_action( 'pre_post_update', 'wp_save_post_revision' );

		foreach( $siblings->posts as $sibling ) :

			// don't handle the actual post
			if ( $sibling->ID === $post->ID ) {
				continue;
			}

			// if this is the post that comes after our repositioned post, set our repositioned post position and increment menu order
			if ( $nextid === $sibling->ID ) {
				wp_update_post(array(
					'ID'			=> $post->ID,
					'menu_order'	=> $start,
					'post_parent'	=> $parent_id,
				));
				$ancestors = get_post_ancestors( $post->ID );
				$new_pos[$post->ID] = array(
					'menu_order'	=> $start,
					'post_parent'	=> $parent_id,
					'depth'			=> count( $ancestors ),
				);
				$start++;
			}

			// if repositioned post has been set, and new items are already in the right order, we can stop
			if ( isset( $new_pos[$post->ID] ) && $sibling->menu_order >= $start ) {
				$return_data->next = false;
				break;
			}

			// set the menu order of the current sibling and increment the menu order
			if ( $sibling->menu_order != $start ) {
				wp_update_post(array(
					'ID' 			=> $sibling->ID,
					'menu_order'	=> $start,
				));
			}

			$new_pos[$sibling->ID] = $start;
			$start++;

			if ( !$nextid && $previd == $sibling->ID ) {
				wp_update_post(array(
					'ID' 			=> $post->ID,
					'menu_order' 	=> $start,
					'post_parent' 	=> $parent_id
				));
				$ancestors = get_post_ancestors( $post->ID );
				$new_pos[$post->ID] = array(
					'menu_order'	=> $start,
					'post_parent' 	=> $parent_id,
					'depth' 		=> count($ancestors) );
				$start++;
			}

		endforeach;

		// max per request
		if ( !isset( $return_data->next ) && $siblings->max_num_pages > 1 ) {
			$return_data->next = array(
				'id' 		=> $post->ID,
				'previd' 	=> $previd,
				'nextid' 	=> $nextid,
				'start'		=> $start,
				'excluded'	=> array_merge( array_keys( $new_pos ), $excluded ),
			);
		} else {
			$return_data->next = false;
		}

		if ( ! $return_data->next ) {
			// if the moved post has children, we need to refresh the page (unless we're continuing)
			$children = get_posts(array(
				'numberposts'				=> 1,
				'post_type' 				=> $post->post_type,
				'post_status' 				=> $post_stati,
				'post_parent' 				=> $post->ID,
				'fields'					=> 'ids',
				'update_post_term_cache'	=> false,
				'update_post_meta_cache'	=> false,
			));

			if ( ! empty( $children ) ) {
				die( 'children' );
			}
		}

		$return_data->new_pos = $new_pos;

		die( json_encode( $return_data ) );
	}



	public function sortByOrderLink($views)
	{
		$class = (get_query_var('orderby') == 'menu_order title' ) ? 'current' : '';
		$query_string = remove_query_arg(array('orderby', 'order'));
		$query_string = add_query_arg('orderby', urlencode('menu_order title'), $query_string);
		$views['byorder'] = '<a href="'. esc_url($query_string) . '" class="' . $class . '">Sort by Order</a>';
		return $views;
	}



	public function checkEditOthersCaps($postType)
	{
		$cpt = get_post_type_object($postType);
		$cap = empty($cpt) ? 'edit_others_' . $postType . 's' : $cpt->cap->edit_others_posts;
		return current_user_can($cap);
	}
}
