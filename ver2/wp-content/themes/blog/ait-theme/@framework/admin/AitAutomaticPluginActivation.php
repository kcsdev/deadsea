<?php


class AitAutomaticPluginActivation
{



	public static function run()
	{
		if(class_exists('TGM_Bulk_Installer', false)){ // case when wp is not fully loaded, case of requirements checker
			$plugins = self::getPrepackedPluginsPaths();

			$installer = new TGM_Bulk_Installer();
			$installer->init();
			$installer->strings['unpack_package'] = '';
			$installer->strings['installing_package'] = '';

			$creds = request_filesystem_credentials('', 'direct', false, false);
			WP_Filesystem($creds);

			foreach($plugins as $pluginPackageFile){
				self::installAndActivatePlugin($pluginPackageFile, $installer);
			}
		}
	}


	protected static function installAndActivatePlugin($pluginPackageFile, $installer)
	{
		$workingDir = $installer->unpack_package($pluginPackageFile, false);

		if(is_wp_error($workingDir)){
			return $workingDir;
		}

		$result = $installer->install_package(
			array(
				'source'            => $workingDir,
				'destination'       => WP_PLUGIN_DIR,
				'clear_destination' => false,
				'clear_working'     => true,
				'hook_extra'        => array('plugin' => $pluginPackageFile),
			)
		);

		wp_cache_flush();

		if(is_wp_error($result)){
			// plugin is installed, but not active
			if($result->get_error_code() == 'folder_exists'){
				$pluginsTargetPath = $result->get_error_data('folder_exists');
				$slug = basename($pluginsTargetPath);
				$pluginData = get_plugins('/' . $slug); // Retrieve all plugins
				$pluginFile = array_keys($pluginData); // Retrieve all plugin files from installed plugins

				$pluginInfo = $slug . '/' . $pluginFile[0]; // Match plugin slug with appropriate plugin file
			}
		}else{
			$pluginInfo = $installer->plugin_info();
		}

		$activate = activate_plugin($pluginInfo);

		if(is_wp_error($activate)){

		}else{

		}

		wp_cache_flush();
	}



	protected static function getPrepackedPluginsPaths()
	{
		$packages = array();
		$plugins = AitTheme::getConfiguration('plugins');

		foreach($plugins as $slug => $plugin){
			if(
				isset($plugin['source']) and $plugin['source'] and
				isset($plugin['ait-prepacked']) and $plugin['ait-prepacked'] and // this how plugins pre-packed by AIT are marked in plugins.php config
				isset($plugin['required']) and $plugin['required'] === true
			){
				$packages[] = $plugin['source'];
			}
		}

		return $packages;
	}
}
