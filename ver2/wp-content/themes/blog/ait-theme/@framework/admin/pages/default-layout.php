<?php


class AitAdminDefaultLayoutPage extends AitAdminPagesOptionsPage
{
    public function beforeRender()
    {
        $this->pageUrl = AitUtils::adminPageUrl(array('page' => $this->pageSlug));
    }


    protected function renderHeaderTitle()
    {
        ?>
        <h3 class="ait-options-header-title"><?php _e('Default Layout <small>Default Layout Options Administration for all pages</small>', 'ait-admin') ?></h3>
        <?php
    }

    protected function renderHeaderTools()
    {
        ?>
        <div class="ait-options-header-tools">
           <ul class="ait-datalinks">
                <li id="action-indicator-reset" class="action-indicator action-reset"></li>
                <li class="ait-reset-button"><a href="#" class="ait-reset-options" title="<?php _e('Reset to Defaults', 'ait-admin') ?>"
                        <?php echo aitDataAttr(
                            'reset-options', array(
                                'confirm' => __('Are you sure you want to reset all settings to defaults?', 'ait-admin'),
                                'nonce' => AitUtils::nonce("reset-pages-options"),
                                'what' => 'pages-options',
                                'oid' => false
                            )
                        )
                        ?>
                        >
                        <?php _e('Reset to Defaults', 'ait-admin') ?></a></li>
            </ul>
        </div>
    <?php
    }



    protected function renderTitle()
    {
        _e('<strong>Default Layout</strong> Options', 'ait-admin');
    }



    protected function isIntroPage()
    {
        return false;
    }

}
