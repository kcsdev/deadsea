<?php


class AitAdminThemeOptionsPage extends AitAdminPage
{


	public function render()
	{

		?>
		<div class="ait-options-page">

			<div class="ait-options-page-top">
				<div class="ait-top-left">&nbsp;</div>
				<div class="ait-top-links">

					<ul class="ait-datalinks">
						<li id="action-indicator-reset" class="action-indicator action-reset"></li>
						<li><a href="#" class="ait-reset-options"
							<?php
								echo aitDataAttr('reset-options', array(
									'confirm' => __('Are you sure you want to reset "Theme Options"?', 'ait-admin'),
									'nonce' => AitUtils::nonce("reset-{$this->pageSlug}"),
									'what' => $this->pageSlug
									));
							?>
						>
							<?php _e('Reset Theme Options', 'ait-admin') ?>
						</a></li>

						<li><a href="#" class="ait-reset-options"
							<?php echo aitDataAttr(
								'reset-options', array(
									'confirm' => __('Are you sure you want to reset all options?', 'ait-admin'),
									'nonce' => AitUtils::nonce("reset-all-options"),
									'what' => 'all'
								));
							?>
						>
							<?php _e('Reset All Options', 'ait-admin') ?>
						</a></li>
					</ul>

					<div class="ait-clear"></div>

				</div>
				<div class="ait-top-right">&nbsp;</div>
			</div><!-- /.ait-options-layout-top -->

			<div class="ait-options-page-content">
				<div class="ait-options-sidebar">
					<div class="ait-options-sidebar-content">
						<h3 class="ait-options-sidebar-title"><?php _e('Theme Options', 'ait-admin') ?></h3>
						<ul id="ait-<?php echo $this->pageSlug ?>-tabs" class="ait-options-tabs">
							<?php
								$this->renderTabs();
							?>
						</ul>
					</div>
				</div>

				<div class="ait-options-content">
					<div class="ait-options-content-header">
						<h1><?php printf(__('<strong>Theme Options:</strong> %s', 'ait-admin'), '<span id="ait-options-panel-title"></span>') ?></h1>
					</div>

					<?php
						$this->formBegin(aitOptions()->getOptionKey('theme'));
					?>

					<div class="ait-options-controls-container">
						<div id="ait-<?php echo $this->pageSlug ?>-panels" class="ait-options-controls ait-options-panels">

							<?php
								AitOptionsControlsRenderer::create(array(
									'configType'    => 'theme',
									'adminPageSlug' => $this->pageSlug,
									'fullConfig'    => aitConfig()->getFullConfig('theme'),
									'defaults'      => aitConfig()->getDefaults('theme'),
									'options'       => aitOptions()->getOptionsByType('theme'),
								))->render();
							?>

						</div>
					</div>

					<?php $this->formEnd() ?>

				</div><!-- /.ait-options-content -->

				<div class="ait-options-aside">
					<div class="ait-options-aside-content">

						<div id="stick-to-top">
							<button class="ait-save-<?php echo $this->pageSlug ?>">
								<?php _e('Save Options', 'ait-admin') ?>
							</button>

							<div id="action-indicator-save" class="action-indicator action-save"></div>

						</div>

					</div><!-- /.ait-options-aside-content -->
				</div><!-- /.ait-options-aside -->
			</div><!-- /.ait-options-layout-content -->

		</div><!-- /.ait-options-page -->
		<?php
	}



	protected function renderTabs()
	{
		$tabs = '';

		$t = aitConfig()->getFullConfig('theme');

		foreach($t as $groupKey => $groupData){

			$panelId = sanitize_key(sprintf("ait-%s-%s-panel", $this->pageSlug, $groupKey));
			$title = (!empty($groupData['@title'])) ? $groupData['@title'] : $groupKey;
			$title = __($title, 'ait-admin');

			$tabs .= "<li id='{$panelId}-tab'><a href='#{$panelId}'>$title</a></li>";
		}

		echo $tabs;
	}

}
