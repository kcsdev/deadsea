<?php

class AitAdmin
{

	/**
	 * Generated Menu slugs
	 * @var array
	 */
	protected static $pagesSlugs = array();

	protected static $topLevelAdminPageSlug = '';


	public static function run()
	{
		AitWpAdminExtensions::register();

		add_action('admin_init', array(__CLASS__, 'onAdminInit'));

		AitShortcodesGenerator::register();

		if(!AitUtils::isAjax()){
			add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueueAdminCssAndJs'), 24);

			add_action('admin_menu', array(__CLASS__, 'renderAdminMenu'), 1);

			add_filter( 'custom_menu_order', '__return_true' );

			add_filter('menu_order', array(__CLASS__, 'changeMenuOrder'), 0);

			add_action('load-themes.php', array(__CLASS__, 'activateTheme'));
			add_action('switch_theme', array(__CLASS__, 'deactivateTheme'));

			add_action('pll_after_add_language', array(__CLASS__, 'onAfterAddLanguage'));

			add_action('admin_print_styles', array(__CLASS__, 'highlightAitMenuItems'));

			add_action('media_buttons', array(__CLASS__, 'addPageBuilderButton'), 100, 1);

			add_filter('redirect_post_location', array(__CLASS__, 'redirectToPageBuilder'), 10, 2);

			self::modifyPageRowActions();

		}
	}



	public static function highlightAitMenuItems()
	{
		$usedColorName = get_user_option('admin_color');

		$colors = array(
			'fresh'     => '#090909',
			'blue'      => '#3290B1',
			'coffee'    => '#3C3632',
			'ectoplasm' => '#352946',
			'light'     => '#bbb',
			'midnight'  => '#16181A',
			'ocean'     => '#526469',
			'sunrise'   => '#b43c38',
		);

		if(isset($colors[$usedColorName])){
			$color = $colors[$usedColorName];
			$adminPages = aitConfig()->getAdminConfig('pages');
			$css = '';

			foreach($adminPages as $page){
				$css .= "li#toplevel_page_ait-{$page['slug']} > a { background: {$color}; }";
			}

			echo "<style>$css</style>";
		}
	}



	public static function onAdminInit()
	{
		if(AitUtils::isAjax()){
			AitAdminAjax::register();
		}
	}



	public static function onAfterAddLanguage()
	{
		AitCache::clean();
	}



	/**
	 * Generates AIT Admin menu
	 */
	public static function renderAdminMenu()
	{
		$t = aitOptions()->getOptionsByType('theme');

		$iconUrl = isset($t['adminBranding']['adminMenuIcon']) ? $t['adminBranding']['adminMenuIcon'] : aitPaths()->url->admin . '/assets/img/ait-admin-menu-icon16.png';
		$adminMenuTitle = isset($t['adminBranding']['adminTitle']) ? AitLangs::getCurrentLocaleText($t['adminBranding']['adminTitle'], __('Theme Admin', 'ait-admin')) : __('Theme Admin', 'ait-admin');

		$aitAdminItemsPosition = 40.01; // position index of separator, 25 = Comments, 26 - 59 = free, 60 = second separator
		$adminPages = aitConfig()->getAdminConfig('pages');

		global $menu;

		$menu[] = array('', 'read', 'ait-separator1', '', 'wp-menu-separator ait-separator');
		$menu[] = array('', 'read', 'ait-separator2', '', 'wp-menu-separator ait-separator');

		// Add sub pages
		foreach($adminPages as $page){
			$class = AitUtils::id2class($page['slug'], 'Page', 'AitAdmin');
			$pageObject = new $class($page['slug']);

			$pageHook = add_menu_page(
				($page['slug'] == 'theme-options') ? $adminMenuTitle : $page['menu-title'],
				($page['slug'] == 'theme-options') ? $adminMenuTitle : $page['menu-title'],
				'publish_pages',
				"ait-{$page['slug']}",
				array($pageObject, "renderPage"),
				$iconUrl,
				(string) $aitAdminItemsPosition += 0.01
			);

			if(isset($page['sub']) and !empty($page['sub'])){

				// add one more time as submenu but with another title
				if($page['slug'] == 'theme-options'){
					$pageHook = add_submenu_page(
						"ait-{$page['slug']}",
						$page['menu-title'],
						$page['menu-title'],
						'publish_pages',
						"ait-{$page['slug']}",
						array($pageObject, "renderPage")
					);
				}

				foreach($page['sub'] as $subpage){
					$class = AitUtils::id2class($subpage['slug'], 'Page', 'AitAdmin');
					$pageObject = new $class($subpage['slug']);
					$pageHook = add_submenu_page(
						"ait-{$page['slug']}",
						$subpage['menu-title'],
						$subpage['menu-title'],
						'manage_options',
						"ait-{$subpage['slug']}",
						array($pageObject, "renderPage")
					);
					add_action('load-' . $pageHook, array($pageObject, "beforeRender"));
				}
			}
			add_action('load-' . $pageHook, array($pageObject, "beforeRender"));
		}
	}



	public static function changeMenuOrder($menuOrder)
	{
		$newOrder = $cpts = array();

		$adminPages = aitConfig()->getAdminConfig('pages');
		$slugs = wp_list_pluck($adminPages, 'slug');
		$firstSlug = array_shift($slugs);
		$lastSlug = array_pop($slugs);


		foreach($menuOrder as $i => $item){
			if(AitUtils::contains($item, $firstSlug)){
				$newOrder[] = 'ait-separator1';
				$newOrder[] = $item;
			}elseif(AitUtils::contains($item, $lastSlug)){
				$newOrder[] = $item;
				$newOrder[] = 'ait-separator2';
			}elseif(AitUtils::startsWith($item, 'edit.php?post_type=ait-')){
				$cpts["x$i"] = $item; // x is for insertAfter, because it overrides same indexes
			}elseif($item != 'ait-separator1' and $item != 'ait-separator2'){
				$newOrder[] = $item;
			}
		}

		$lastAitSepIndex = array_search('ait-separator2', $newOrder);

		if(!empty($cpts)){
			NArrays::insertAfter($newOrder, $lastAitSepIndex, $cpts); // insert all our cpts after our main menu items
			$newOrder = array_values($newOrder); // get rid off of "x{...}" indexes
		}else{
			unset($newOrder[$lastAitSepIndex]);
		}

		return $newOrder;
	}



	public static function getCurrentPageSlug()
	{
		$id = get_current_screen()->id;
		$adminPages = aitConfig()->getAdminConfig('pages');
		$return = '';

		foreach($adminPages as $page){
			if(AitUtils::endsWith($id, $page['slug'])){
				$return = $page['slug'];
				break;
			}
			if(isset($page['sub'])){
				foreach($page['sub'] as $subpage){
					if(AitUtils::endsWith($id, $subpage['slug'])){
						$return = $subpage['slug'];
						break;
					}
				}
			}
		}

		return $return;
	}



	/**
	 * Activates theme. Saves default options.
	 * @return void
	 */
	public static function activateTheme()
	{
		global $pagenow;

		if($pagenow == 'themes.php' and (isset($_GET['activated']) or isset($_GET['ait-theme-continue']))){

			AitCache::clean();

			$new = aitConfig()->extractDefaultsFromConfig(
				aitConfig()->getRawConfig(),
				true
			);

			foreach(AitConfig::getMainConfigTypes() as $configType){
				$key = aitOptions()->getOptionKey($configType);
				$wasAdded = add_option($key, $new[$configType]);
			}

			if(@is_writable(WP_PLUGIN_DIR)){
				AitAutomaticPluginActivation::run();
			}

			do_action('ait-theme-activation');

			flush_rewrite_rules();

			if($wasAdded){ // this is first time activation of this theme, so redirect to Importing Demo Content admin page
				$redirectTo = add_query_arg(array('page' => 'ait-backup#ait-backup-import-demo-content-panel'), admin_url("admin.php"));
			}else{
				$redirectTo = admin_url('themes.php');
			}

			aitManager('assets')->compileLessFiles();

			wp_redirect(esc_url_raw($redirectTo));
		}
	}



	/**
	 * Deactivate theme
	 * @return void
	 */
	public static function deactivateTheme()
	{
		flush_rewrite_rules();
	}



	/**
	 * Registers JavaScripts for Ait Admin
	 */
	public static function enqueueAdminCssAndJs()
	{
		global $pagenow;

		$assetsUrl = aitPaths()->url->admin . '/assets';

		wp_enqueue_style('ait-wp-admin-style', "{$assetsUrl}/css/wp-admin.css", array('media-views'), AIT_THEME_VERSION);

		$pages = array('edit.php', 'post-new.php', 'post.php', 'media-upload.php', 'nav-menus.php', 'profile.php', 'user-edit.php');

		if(self::getCurrentPageSlug() or in_array($pagenow, $pages) or apply_filters('ait-enqueue-admin-assets', false)){

			$langCode = AitLangs::getCurrentLanguageCode();

			$min = ((defined('SCRIPT_DEBUG') and SCRIPT_DEBUG) or AIT_DEV) ? '' : '.min';

			wp_enqueue_style('ait-colorpicker', "{$assetsUrl}/libs/colorpicker/colorpicker.css", array(), '2.2.1');
			wp_enqueue_style('ait-jquery-chosen', "{$assetsUrl}/libs/chosen/chosen.css", array(), '0.9.10');
			wp_enqueue_style('jquery-ui', "{$assetsUrl}/libs/jquery-ui/jquery-ui.css", array('media-views'), AIT_THEME_VERSION);
			wp_enqueue_style('ait-jquery-timepicker-addon', "{$assetsUrl}/libs/timepicker-addon/jquery-ui-timepicker-addon{$min}.css", array(), AIT_THEME_VERSION);
			wp_enqueue_style('jquery-switch', "{$assetsUrl}/libs/jquery-switch/jquery.switch.css", array(), '0.4.1');

			wp_enqueue_style('ait-admin-style', "{$assetsUrl}/css/style.css", array('media-views'), AIT_THEME_VERSION);
			wp_enqueue_style('ait-admin-options-controls', "{$assetsUrl}/css/options-controls" . ($pagenow == 'edit.php' ? "-quickedit" : "") . ".css", array('ait-admin-style', 'ait-jquery-chosen'), AIT_THEME_VERSION);

			$fontCssFile = aitUrl('css', '/libs/font-awesome.min.css');
			if($fontCssFile){
				wp_enqueue_style('ait-font-awesome-select', $fontCssFile, array(), '4.2.0');
			}

			wp_enqueue_script('ait.admin', "{$assetsUrl}/js/ait.admin.js", array('media-editor'), AIT_THEME_VERSION, TRUE);

			self::adminGlobalJsSettings();

			// js libs
			wp_register_script('ait-jquery-filedownload', "{$assetsUrl}/libs/file-download/jquery.fileDownload{$min}.js", array('jquery', 'ait.admin'), '1.3.3', TRUE);

			wp_enqueue_script('ait-colorpicker', "{$assetsUrl}/libs/colorpicker/colorpicker{$min}.js", array('jquery'), '2.2.1', TRUE);
			wp_enqueue_script('ait-jquery-chosen', "{$assetsUrl}/libs/chosen/chosen.jquery{$min}.js", array('jquery'), '1.0.0', TRUE);
			wp_enqueue_script('ait-jquery-sheepit', "{$assetsUrl}/libs/sheepit/jquery.sheepItPlugin{$min}.js", array('jquery', 'ait.admin'), '1.1.1-ait-1', TRUE);
			wp_enqueue_script('ait-jquery-deparam', "{$assetsUrl}/libs/jquery-deparam/jquery-deparam{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);
			wp_enqueue_script('ait-jquery-rangeinput', "{$assetsUrl}/libs/rangeinput/rangeinput.min.js", array('jquery', 'ait.admin'), '1.2.7', TRUE);
			wp_enqueue_script('ait-jquery-numberinput', "{$assetsUrl}/libs/numberinput/numberinput{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);

			wp_enqueue_script('ait-jquery-timepicker-addon', "{$assetsUrl}/libs/timepicker-addon/jquery-ui-timepicker-addon{$min}.js", array('jquery', 'ait.admin', 'jquery-ui-slider', 'jquery-ui-datepicker'), FALSE, TRUE);

			if($langCode !== 'en'){
				wp_enqueue_script('ait-jquery-datepicker-translation', "{$assetsUrl}/libs/datepicker/jquery-ui-i18n{$min}.js", array('jquery', 'ait.admin', 'jquery-ui-datepicker'), FALSE, TRUE);
				wp_enqueue_script('ait-jquery-timepicker-translation', "{$assetsUrl}/libs/timepicker-addon/jquery-ui-timepicker-addon-i18n{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);
			}

			wp_enqueue_script('ait-jquery-switch', "{$assetsUrl}/libs/jquery-switch/jquery.switch{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);
			wp_enqueue_script('ait-bootstrap-dropdowns', "{$assetsUrl}/libs/bootstrap-dropdowns/bootstrap-dropdowns{$min}.js", array('jquery', 'ait.admin'), FALSE, TRUE);

			wp_enqueue_script('ait-google-maps', "//maps.google.com/maps/api/js?sensor=false&amp;language=en", array('jquery'), FALSE, TRUE);
			wp_enqueue_script('ait-jquery-gmap3', "{$assetsUrl}/libs/gmap3/gmap3.min.js", array('jquery', 'ait.admin', 'ait-google-maps'), FALSE, TRUE);

			wp_enqueue_script('ait-jquery-raty', "{$assetsUrl}/libs/raty/jquery.raty-2.5.2.js", array('jquery'), '2.5.2', TRUE);

			wp_enqueue_media();

			wp_enqueue_script('ait.admin.Tabs', "{$assetsUrl}/js/ait.admin.tabs.js", array('ait.admin', 'jquery'), AIT_THEME_VERSION, TRUE);
			wp_enqueue_script('ait.admin.options', "{$assetsUrl}/js/ait.admin.options.js", array('ait.admin', 'jquery', 'jquery-ui-tabs', 'ait-jquery-chosen', 'jquery-ui-datepicker', 'ait-jquery-gmap3'), AIT_THEME_VERSION, TRUE);
			wp_enqueue_script('ait.admin.backup', "{$assetsUrl}/js/ait.admin.backup.js", array('ait.admin', 'jquery', 'ait-jquery-filedownload'), AIT_THEME_VERSION, TRUE);
			wp_enqueue_script('ait.admin.options.elements', "{$assetsUrl}/js/ait.admin.options.elements.js", array('ait.admin', 'ait.admin.options', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-ui-sortable'), AIT_THEME_VERSION, TRUE);
			wp_enqueue_script('ait.admin.nav-menus', "{$assetsUrl}/js/ait.admin.nav-menus.js", array('ait.admin', 'ait.admin.options', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-ui-sortable'), AIT_THEME_VERSION, TRUE);
		}
	}



	public static function adminGlobalJsSettings()
	{
		$u = wp_upload_dir();

		$settings = array(
			'ajax' => array(
				'url'     => admin_url('admin-ajax.php'),
				'actions' => array(),
			),
			'currentPage' => self::getCurrentPageSlug(),
			'paths' => array(
				'root'      => aitPaths()->url->root,
				'theme'     => aitPaths()->url->theme,
				'wpcontent' => content_url(),
				'uploads'   => $u['baseurl'],
			),
			'l10n' => array(
				'save' => array(
					'working' => __('&hellip; saving &hellip;', 'ait-admin'),
					'done'    => __('settings were saved successfully', 'ait-admin'),
					'error'   => __('there was an error during saving', 'ait-admin'),
				),
				'reset' => array(
					'working' => __('Resetting&hellip;', 'ait-admin'),
					'done'    => __('Successfully reset. This page will reload&hellip;', 'ait-admin'),
				),
				'confirm' => array(
					'removeElement'       => __('Are you sure you want to remove this element?', 'ait-admin'),
					'removeCustomOptions' =>  __('Are you sure you want to delete custom page options of this page?', 'ait-admin'),
					// translators: {pageTtile} is placeholder like %s
					'addCustomOptions'    => __("You are about to create custom options for page:\n{pageTitle}.\nIs this ok?", 'ait-admin'),
				),
				'datetimes' => array(
					'dateFormat'  => AitUtils::phpDate2jsDate(get_option('date_format')),
					// 'timeFormat'  => get_option('time_format'),
					'startOfWeek' => get_option('start_of_week'),
				),
				'labels' => array(
					'settingsForSpecialPageType' => __("Settings Administration for the special page type", 'ait-admin'),
					'settingsForStandardPageType' => __("Settings Administration for this standard page only", 'ait-admin'),
				),
				'elementUserDescriptionPlaceholder' => __('Click to add custom description', 'ait-admin'),
				'backup' => array(
					'info' => array(
						'noBackupFile'       => __('Please select backup file', 'ait-admin'),
						// translators: {option} and {filename} are placeholders like %s
						'selectedBadFileFix' => __("You selected option '{option}' but file was '{filename}'. We corrected this for you :)", 'ait-admin'),
						'importBackup'       => __('Are you sure you want to import backup? All tables will be truncated before import!', 'ait-admin'),
						'importDemoContent'  => __('Are you sure you want to import demo content? Whole content of your website will be replaced!', 'ait-admin'),
					),
					'import' => array(
						'working' => __("Importing...", 'ait-admin'),
						'done'    => __('Importing is done. Check out the report.', 'ait-admin'),
						'error'   => __('There was an error during importing. Check out the report.', 'ait-admin'),
					),
					'export' => array(
						'working' => __("Exporting...", 'ait-admin'),
						'done'    => __('You just got a file download dialog or ribbon.', 'ait-admin'),
						'error'   => __('Your file download failed. Please try again.', 'ait-admin'),
					)
				)
			),
		);

		$class = 'AitAdminAjax';

		$methods = get_class_methods($class);
		$r = new NClassReflection($class);

		foreach($methods as $method){
			if($r->getMethod($method)->getAnnotation('WpAjax') === true){
				$settings['ajax']['actions'][$method] = "admin:{$method}";
			}
		}


		wp_localize_script('ait.admin', 'AitAdminJsSettings', apply_filters('ait-admin-global-js-settings', $settings));
	}



	public static function addPageBuilderButton($editorId)
	{
		$s = get_current_screen();
		$post = get_post();

		if($post and $post->post_type == 'page' and $s->id == 'page'){
			printf(
				'<a href="#" id="ait-goto-page-builder-button" class="button button-primary" data-ait-empty-title-note="%s">%s</a>',
				__('Please enter title of the page', 'ait-admin'),
				__('Save and Open in Page Builder', 'ait-admin')

			);
		}
	}



	public static function modifyPageRowActions()
	{
		add_filter('page_row_actions', array(__CLASS__, 'addPageBuilderLinkToPageRowActions'), 10, 2);
	}



	public static function addPageBuilderLinkToPageRowActions($actions, $page)
	{
		$args = array('page' => 'pages-options', 'oid' => '_page_' . $page->ID);

		if($page->post_status != 'auto-draft'){
			if(get_option('show_on_front') == 'page'){
				if($b = get_option('page_for_posts')){
					$blog = (int) $b;
					if($page->ID == $blog)
						$args['oid'] = "_blog";
				}
			}


			$title = __('Page Builder', 'ait-admin');
			$args['oidnonce'] = AitUtils::nonce('oidnonce');

			$url = esc_url(AitUtils::adminPageUrl($args));

			$link = "<a href=\"$url\">$title</a>";

			$actions['page_builder'] = $link;
		}

		return $actions;
	}




	public static function redirectToPageBuilder($location, $postId)
	{
		if(!isset($_POST['ait-redirect-to-page-builder']) or $_POST['post_type'] != 'page'){
			return $location;
		}

		$args = array('page' => 'pages-options', 'oid' => '_page_' . $postId);

		$r = aitOptions()->getLocalOptionsRegister();
		$blogId = 0;

		if(get_option('show_on_front') == 'page'){
			if($b = get_option('page_for_posts')){
				$blogId = (int) $b;
				if($postId == $blogId){
					$args['oid'] = "_blog";
				}
			}
		}

		if(!in_array("_page_{$postId}", $r['pages']) and $postId != $blogId){
			$args['oidnonce'] = AitUtils::nonce('oidnonce');
		}

		$url = AitUtils::adminPageUrl($args);
		return esc_url_raw($url);
	}
}
