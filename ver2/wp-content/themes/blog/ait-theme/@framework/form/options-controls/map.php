<?php


class AitMapOptionControl extends AitOptionControl
{

	protected function init()
	{
		$this->isLessVar = false;
	}



	protected function control()
	{
		$d = (object) $this->config->default; // no getDefaultValue()
		?>
		<div class="ait-opt-label">
			<?php $this->labelWrapper() ?>
		</div>

		<div class="ait-opt ait-opt-<?php echo $this->id ?>">
			<div class="ait-opt-wrapper">

				<div class="ait-opt-tools ait-opt-maps-tools">
					<div class="ait-opt-tools-row">
						<div class="ait-opt-tools-cell1">

							<div class="ait-opt-maps-item ait-opt-maps-address">
								<label for="<?php echo $this->getIdAttr('map-address'); ?>"><?php _e('Address' , 'ait-admin') ?></label><!--
							 --><div class="ait-control-wrapper">
							 		<?php 
							 			$data = $this->getValue();
							 			if(is_string($data)){
							 				$val = $data;
							 			} else {
								 			if(!isset($data['address'])){
								 				$val = AitLangs::getCurrentLocaleText($data);
								 			} else {
								 				$val = $data['address'];
								 			}
								 		}
							 		?>
									<input type="text" id="<?php echo $this->getIdAttr('map-address') ?>" name="<?php echo $this->getNameAttr('address') ?>" value="<?php echo $val ?>">
									<input type="button" value="<?php _e('Find' , 'ait-admin') ?>">
								</div>
							</div>

							<div class="ait-opt-maps-item ait-opt-maps-latitude">
								<label for="<?php echo $this->getIdAttr('map-latitude'); ?>"><?php _e('Latitude' , 'ait-admin') ?></label><!--
							 --><div class="ait-control-wrapper">
									<?php $val = isset($data['latitude']) ? $data['latitude'] : 1 ?>
									<input type="text" id="<?php echo $this->getIdAttr('map-latitude') ?>" name="<?php echo $this->getNameAttr('latitude') ?>" value="<?php echo $val ?>">
								</div>
							</div>

							<div class="ait-opt-maps-item ait-opt-maps-longitude">
								<label for="<?php echo $this->getIdAttr('map-longitude'); ?>"><?php _e('Longitude' , 'ait-admin') ?></label><!--
							 --><div class="ait-control-wrapper">
							 		<?php $val = isset($data['longitude']) ? $data['longitude'] : 1 ?>
									<input type="text" id="<?php echo $this->getIdAttr('map-longitude') ?>" name="<?php echo $this->getNameAttr('longitude') ?>" value="<?php echo $val ?>">
								</div>
							</div>

							<?php $val = isset($data['streetview']) ? (int)$data['streetview'] : 0 ?>
							<div class="ait-opt-maps-item ait-opt-maps-streetview ait-opt-on-off">
								<label for="<?php echo $this->getIdAttr('map-streetview'); ?>"><?php _e('Streetview' , 'ait-admin') ?></label><!--
							 	--><div class="ait-control-wrapper">
									<div class="ait-opt-switch">
										<select id="<?php echo $this->getIdAttr('map-streetview'); ?>" name="<?php echo $this->getNameAttr('streetview'); ?>" class="ait-opt-<?php echo $this->key ?>">
											<option <?php selected($val, 1); ?>  value="1">On</option>
											<option <?php selected($val, 0); ?>  value="0">Off</option>
										</select>
									</div>
								</div>
							</div>

							<div class="ait-opt-maps-item ait-opt-maps-swheading">
								<div class="ait-control-wrapper">
									<?php $val = isset($data['swheading']) ? $data['swheading'] : 0 ?>
									<input type="hidden" id="<?php echo $this->getIdAttr('map-swheading') ?>" name="<?php echo $this->getNameAttr('swheading') ?>" value="<?php echo $val ?>">
								</div>
							</div>
							<div class="ait-opt-maps-item ait-opt-maps-swpitch">
								<div class="ait-control-wrapper">
									<?php $val = isset($data['swpitch']) ? $data['swpitch'] : 0 ?>
									<input type="hidden" id="<?php echo $this->getIdAttr('map-swpitch') ?>" name="<?php echo $this->getNameAttr('swpitch') ?>" value="<?php echo $val ?>">
								</div>
							</div>
							<div class="ait-opt-maps-item ait-opt-maps-swzoom">
								<div class="ait-control-wrapper">
									<?php $val = isset($data['swzoom']) ? $data['swzoom'] : 0 ?>
									<input type="hidden" id="<?php echo $this->getIdAttr('map-swzoom') ?>" name="<?php echo $this->getNameAttr('swzoom') ?>" value="<?php echo $val ?>">
								</div>
							</div>

						</div>

						<div class="ait-opt-tools-cell2">
							<div class="ait-opt-maps-item ait-opt-maps-wrap">
								<div class="ait-opt-maps-screen">
									<div class="ait-opt-maps-preview">&nbsp;</div>
								</div>
							</div>
						</div>

					</div>
				</div><!-- end of ait-opt-tools -->

			</div>

			<?php $this->help() ?>
		</div>
		<?php
	}



	public static function prepareDefaultValue($optionControlDefinition)
	{
		$d = array(
			'address'    	=> '',
			'latitude'   	=> 1,
			'longitude' 	=> 1,
			'streetview'   	=> false,
			'swheading'		=> 90,
			'swpitch'		=> 5,
			'swzoom'		=> 1,
		);

		$d = array_merge($d, $optionControlDefinition['default']);

		return $d;
	}


}
