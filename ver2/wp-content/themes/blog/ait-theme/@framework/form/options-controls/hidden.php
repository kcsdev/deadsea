<?php


class AitHiddenOptionControl extends AitOptionControl
{


	protected function init()
	{
		$this->isCloneable = true;
	}



    public function getHtml()
    {
		ob_start();
		?>
        <input type="hidden" name="<?php echo $this->getNameAttr() ?>" value="<?php echo esc_attr($this->getValue()); ?>">
		<?php
		return ob_get_clean();
    }

}
