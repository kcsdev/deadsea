<?php


class AitSidebarOptionControl extends AitOptionControl
{

	protected function control()
	{
		$val = $this->getValue();
		$sidebars = aitManager('sidebars')->getSidebars();

		?>
		<div class="ait-opt-label">
			<?php $this->labelWrapper() ?>
		</div>

		<div class="ait-opt ait-opt-<?php echo $this->id ?>">
			<?php $lang = AitLangs::checkIfPostAndGetLang(); ?>
			<div class="ait-opt-wrapper chosen-wrapper <?php echo AitLangs::htmlClass($lang ? $lang->locale : '') ?>">
				<?php if(AitLangs::isEnabled()){
					if($lang){
						?>
						<div class="flag">
							<?php
						echo $lang->flag;
							?>
						</div>
						<?php
					}else{
						?>
						<div class="flag">
							<?php
						echo AitLangs::getDefaultLang()->flag;
							?>
						</div>
						<?php
					}
				} ?>
				<select data-placeholder="<?php _e('Choose&hellip;', 'ait-admin') ?>" name="<?php echo $this->getNameAttr('sidebar'); ?>" id="<?php echo $this->getIdAttr('sidebar'); ?>" class="chosen">
					<option value="none" <?php selected($val['sidebar'], 'none') ?>><?php echo esc_html(_x('None', 'sidebar', 'ait-admin')) ?></option>
				<?php
					foreach($sidebars as $sidebarId => $params):
						?>
						<option value="<?php echo esc_attr($sidebarId) ?>" <?php selected($val['sidebar'], $sidebarId) ?>><?php echo esc_html(AitLangs::getDefaultLocaleText($params['name'], 'unknown sidebar')) ?></option>
						<?php
					endforeach;
				?>
				</select>
			</div>
		</div>

		<div class="ait-opt-help">
			<?php $this->help() ?>
		</div>
		<?php

	}

}
