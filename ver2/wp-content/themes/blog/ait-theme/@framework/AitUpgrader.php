<?php


/**
 * Does upgrade jobs
 */
class AitUpgrader
{

	protected $skeletonVersionOptionKey;
	protected $themeVersionOptionKey;

	protected $errors = array();



	public static function run()
	{
		if(AitUtils::isAjax() or defined('IFRAME_REQUEST')) return;

		$upgrader = new self;

		$upgrader->checkVersionAndMaybeDoUpgrade();
	}



	public function __construct()
	{
		if(get_option("_ait_" . AIT_CURRENT_THEME . "_skeleton_version") === false){
			$this->skeletonVersionOptionKey = "_ait_skeleton_version";
			$this->themeVersionOptionKey = "_ait_theme_version";
		}else{
			$this->skeletonVersionOptionKey = "_ait_" . AIT_CURRENT_THEME . "_skeleton_version";
			$this->themeVersionOptionKey = "_ait_" . AIT_CURRENT_THEME . "_theme_version";
		}
	}



	public function checkVersionAndMaybeDoUpgrade()
	{
		if(version_compare($this->getSkeletonVersion(), AIT_SKELETON_VERSION, '!=')){
			$this->skeletonUpgrade();
		}

		if(version_compare($this->getThemeVersion(), AIT_THEME_VERSION, '!=')){
			$this->themeUpgrade();
		}

		if(empty($this->errors)){
			if(version_compare($this->getSkeletonVersion(), AIT_SKELETON_VERSION, '!=')){
				$this->updateSkeletonVersion(AIT_SKELETON_VERSION);
				AitCache::clean();
			}
			if(version_compare($this->getThemeVersion(), AIT_THEME_VERSION, '!=')){
				$this->updateThemeVersion(AIT_THEME_VERSION);
				AitCache::clean();
			}
		}
	}



	public function skeletonUpgrade()
	{
		if(version_compare($this->getSkeletonVersion(), '2.1.4', '<')){
			$upgrade = new AitSkeletonUpgrade21;
			$this->errors = $upgrade->execute();
		}

		if(version_compare($this->getSkeletonVersion(), '2.2.3', '<')){
			$upgrade = new AitSkeletonUpgrade223;
			$errors = $upgrade->execute();
			$this->errors = array_merge($this->errors, $errors);
			$this->skeletonVersionOptionKey = "_ait_" . AIT_CURRENT_THEME . "_skeleton_version";
			$this->themeVersionOptionKey = "_ait_" . AIT_CURRENT_THEME . "_theme_version";
		}

		$this->addAdminNotices();

		do_action('ait-skeleton-upgrade', $this);
	}



	protected function addAdminNotices()
	{
		if(!empty($this->errors)){
			add_action('admin_notices', array($this, 'adminNotices'));
		}
	}



	public function themeUpgrade()
	{
		do_action('ait-theme-upgrade', $this);
	}



	public function adminNotices()
	{
		echo '<div class="error">';
		foreach($this->errors as $error){
			echo "<p>";
			echo esc_html($error);
			echo "</p>";
		}
		echo '</div>';
	}



	public function getSkeletonVersion()
	{
		return get_option($this->skeletonVersionOptionKey, '0.0.1');
	}



	public function getThemeVersion()
	{
		return get_option($this->themeVersionOptionKey, '0.0.1');
	}



	public function updateSkeletonVersion($newVersion)
	{
		update_option($this->skeletonVersionOptionKey, $newVersion);
	}



	public function updateThemeVersion($newVersion)
	{
		update_option($this->themeVersionOptionKey, $newVersion);
	}
}
