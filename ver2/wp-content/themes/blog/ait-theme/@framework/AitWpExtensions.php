<?php


class AitWpExtensions
{


	public static function register()
	{
		$ext = self::loadConfig();

		if(!AitUtils::isAjax()){ // these are no needed during ajax request

			// Remove rel attribute from the category list
			add_filter('wp_list_categories', array(__CLASS__, 'removeCategoryRelAtt'));
			add_filter('the_category', array(__CLASS__, 'removeCategoryRelAtt'));

			if($ext->wp->loginPageBranding){
				global $pagenow;

				if($pagenow == 'wp-login.php'){
					add_action('login_head', array(__CLASS__, 'loginPageBranding'));
					add_filter('login_headerurl', array(__CLASS__, 'loginPageBranding'));
					add_filter('login_headertitle', array(__CLASS__, 'loginPageBranding'));
				}
			}
		}

		// Text Widget can have shortcodes too
		add_filter('widget_text', 'do_shortcode');
	}



	public static function loadConfig()
	{
		$extensionsFilePath = aitPath('config', '/wp-extensions.php');

		if($extensionsFilePath === false){
			$ext = require aitPaths()->dir->fwConfig . '/wp-extensions.php';
		}else{
			$ext = include $extensionsFilePath;
		}
		return $ext;
	}



	public static function removeCategoryRelAtt($output)
	{
  		return str_replace(' rel="category tag"', '', $output);
	}



	public static function loginPageBranding($input = '')
	{
		$t = aitOptions()->getOptionsByType('theme');
		$branding = $t['adminBranding'];

		if(current_filter() == 'login_head'){
			if(isset($branding["loginScreenCss"]) and $branding["loginScreenCss"]){
				echo "<style>" . $branding["loginScreenCss"] . "</style>\n";
			}

			if(isset($branding["loginScreenLogo"]) and $branding["loginScreenLogo"]){
				$css = '.login h1 a {background-image: url("%s"); background-size: 274px 63px; width: 274px}';
				echo "<style>" . sprintf($css, $branding["loginScreenLogo"]) . "</style>\n";
			}
			return; // this is action, not filter
		}


		if(current_filter() == 'login_headerurl' and isset($branding["loginScreenLogoLink"]) and $branding["loginScreenLogoLink"]){
			return $branding["loginScreenLogoLink"];
		}


		if(current_filter() == 'login_headertitle' and isset($branding["loginScreenLogoTooltip"]) and $branding["loginScreenLogoTooltip"]){
			return AitLangs::getCurrentLocaleText($branding["loginScreenLogoTooltip"], $input);
		}

		return $input;
	}


}
