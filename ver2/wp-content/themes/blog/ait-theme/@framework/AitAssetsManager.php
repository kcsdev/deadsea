<?php


/**
 * Registers and enqueues scripts and styles for frontend
 */
class AitAssetsManager
{

	protected $builtinFrontendAssets = array();

	protected $assetsList = array();

	protected $ajaxActions = array();

	protected $params = array();

	protected $inlineStyleCallbacks = array();

	protected $lastEnqueuedCssHandler;



	public function __construct($builtinFrontendAssets, $assetsFromFunctionsPhpFile)
	{
		$this->builtinFrontendAssets = $builtinFrontendAssets;
		$this->assetsList[] = array('assets' => $assetsFromFunctionsPhpFile, 'params' => array());
	}



	public function addAssets($assets, $params = array())
	{
		$this->assetsList[] = array('assets' => $assets, 'params' => $params);
	}



	public function setAjaxActions($callbacks)
	{
		$this->ajaxActions = $callbacks;
	}



	public function enqueueFrontendAssets()
	{
		$builtinAssets = apply_filters('ait-theme-builtin-assets', $this->builtinFrontendAssets);
		$this->enqueueCss($builtinAssets['css']);
		$this->enqueueJs($builtinAssets['js']);

		$this->initGlobalJsVariables();

		if(is_singular() and comments_open() and get_option('thread_comments')){
			wp_enqueue_script('comment-reply');
		}

		$this->addGoogleFontsCss();

		$assetsList = apply_filters('ait-theme-assets', $this->assetsList);

		foreach($assetsList as $item){
			if(isset($item['assets']['css'])){
				$this->enqueueCss($item['assets']['css'], $item['params']);
			}

			if(isset($item['assets']['js'])){
				$this->enqueueJs($item['assets']['js'], $item['params']);
			}
		}

		$this->enqueueLess();


		if(file_exists(aitPaths()->dir->theme . '/custom.css')){
			wp_enqueue_style('ait-theme-custom-style', aitPaths()->url->theme . '/custom.css');
		}
	}



	public function compileLessFiles()
	{
		$lessCompiler = new AitLessCompiler(aitPaths()->dir->cache, aitPaths()->url->cache);

		$lessFiles = array(
			'ait-theme-main-style' => array(
				'inputFile' => aitPath('css', '/style.less'),
				'params'    => array(),
				'inlineCss' => $this->getThemeMainInlineStylesContent(),
			),
			'ait-theme-layout-style' => array(
				'inputFile' => aitPath('css', '/layout.less'),
				'params'    => array('oid' => aitOptions()->getOid()),
			),
			'ait-preloading-effects' => array(
				'inputFile' => aitPath('css', '/preloading.less'),
				'params'    => array(),
			),
			'ait-typography-style' => array(
				'inputFile' => aitPath('css', '/typography.less'),
				'params'    => array('lang' => AitLangs::getCurrentLocale()),
				'customCss' => aitOptions()->get('theme')->customCss->css,
			),
		);

		$results = array();
		foreach($lessFiles as $handler => $args){
			if($args['inputFile']){
				$results[] = array(
					'handler' => $handler,
					'args' => $args,
					'output' => $lessCompiler->compile($args),
				);
			}
		}

		return $results;
	}



	public function enqueueLess()
	{
		$results = $this->compileLessFiles();

		foreach($results as $result){
			if(!$result['output']['isEmpty']){
				if($result['output']['error']){
					$this->embedCssGeneratedFromLess($result['handler'], $result['args'], $result['output'], true);
				}else{
					$this->enqueueCssGeneratedFromLess($result['handler'], $result['args'], $result['output']);
				}
			}
		}
	}



	protected function enqueueCssGeneratedFromLess($handler, $args, $output)
	{
		wp_enqueue_style($handler, $output['url'], array(), $output['version']);

		if(isset($args['inlineCss'])){
			wp_add_inline_style($handler, $args['inlineCss']);
		}

		if(isset($args['customCss'])){
			wp_add_inline_style($handler, $args['customCss']);
		}
	}



	protected function embedCssGeneratedFromLess($handler, $args, $output, $error = false)
	{
		if($error){
			$output['embedCss'] = "\n\n/*  ==== LESS ERROR ==== */\nError in file '{$args['inputFile']}'\n" . $output['embedCss'];
			trigger_error($output['embedCss'], E_USER_WARNING);
		}

		wp_add_inline_style($this->lastEnqueuedCssHandler, $output['embedCss']);

		if(isset($args['inlineCss'])){
			wp_add_inline_style($handler, $args['inlineCss']);
		}

		if(isset($args['customCss'])){
			wp_add_inline_style($handler, $args['customCss']);
		}
	}



	public function enqueueAdminAssets()
	{
		foreach($this->assetsList as $item){

			if(isset($item['assets']['admin-css'])){
				$this->enqueueCss($item['assets']['admin-css'], $item['params']);
			}

			if(isset($item['assets']['admin-js'])){
				$this->enqueueJs($item['assets']['admin-js'], $item['params']);
			}
		}
	}



	/**
	 * Enqueues CSS files
	 */
	public function enqueueCss($styles, $params = array())
	{
		if(empty($styles) or !is_array($styles)) return; // do nothing

		$lastHandler = '';
		foreach($styles as $handler => $css){
			$lastHandler = $handler;

			if($css === true){ // wp builtin css

				wp_enqueue_style($handler);

			}elseif(is_array($css)){

				if(AitUtils::isExtUrl($css['file']) or AitUtils::isAbsUrl($css['file'])){
					$url = $css['file'];
				}else{
					if(isset($params['paths']->url->css))
						$url = $params['paths']->url->css . $css['file'];
					else
						$url = aitUrl('css', $css['file']);
				}

				wp_register_style(
					$handler,
					$url,
					isset($css['deps']) ? $css['deps'] : array(),
					isset($css['ver']) ? $css['ver'] : false,
					isset($css['media']) ? $css['media'] : 'all'
				);

				if(!isset($css['enqueue']) or (isset($css['enqueue']) and $css['enqueue']))
					wp_enqueue_style($handler);
			}
		}

		$this->lastEnqueuedCssHandler = $lastHandler;
	}



	/**
	 * Enqueue JS files
	 */
	public function enqueueJs($scripts, $params = array())
	{
		if(empty($scripts) or !is_array($scripts)) return; // do nothing

		foreach($scripts as $handler => $js){

			if(is_bool($js) and $js === true){ // wp builtin js

				wp_enqueue_script($handler);

			}elseif(is_array($js)){

				$filename = $js['file'];

				if(isset($js['lang'])){
					$filename = str_replace('{lang}', AitLangs::getCurrentLanguageCode(), $filename);
					$filename = str_replace('{gmaps-lang}', AitLangs::getGmapsLang(), $filename);
				}

				if(AitUtils::isExtUrl($filename) or AitUtils::isAbsUrl($filename)){
					$url = $filename;
				}else{
					if(isset($params['paths']->url->js))
						$url = $params['paths']->url->js . $filename;
					else
						$url = aitUrl('js', $filename);
				}

				wp_register_script(
					$handler,
					$url,
					isset($js['deps']) ? $js['deps'] : array(),
					isset($js['ver']) ? $js['ver'] : false,
					isset($js['in-footer']) ? $js['in-footer'] : true // our default is in footer
				);

				if(isset($js['enqueue-only-if']) and $js['enqueue-only-if']){
					$fn = create_function('', "return {$js['enqueue-only-if']};");
					if(call_user_func($fn)){
						wp_enqueue_script($handler);
					}
				}elseif(!isset($js['enqueue']) or (isset($js['enqueue']) and $js['enqueue'] == true)){
					wp_enqueue_script($handler);
				}

				if(isset($js['localize'])){
					if(isset($js['localize']['object-var'])){
						$var = $js['localize']['object-var'];
						unset($js['localize']['object-var']);
					}else{
						$var = AitUtils::dash2class($handler);
					}

					wp_localize_script($handler, $var, $js['localize']);
				}
			}
		}
	}



	protected function initGlobalJsVariables()
	{
		$settings = array(
			'home' => array(
				'url' => home_url(),
			),
			'ajax' => array(
				'url'     => admin_url('admin-ajax.php'),
				'actions' => array(),
			),
			'paths' => array(
				'theme' => aitPaths()->url->theme,
				'css'   => aitPaths()->url->css,
				'js'    => aitPaths()->url->js,
				'img'   => aitPaths()->url->img,
			),
			'l10n' => array(
				'datetimes' => array(
					'dateFormat'  => AitUtils::phpDate2jsDate(get_option('date_format')),
					// 'timeFormat'  => get_option('time_format'),
					'startOfWeek' => get_option('start_of_week'),
				),
			),
		);

		$settings['ajax']['actions'] = $this->ajaxActions;

		wp_localize_script('jquery-core', 'AitSettings', apply_filters('ait-global-js-settings', $settings));
	}



	public function addInlineStyleCallback($callback)
	{
		$this->inlineStyleCallbacks[] = $callback;
	}



	public function getThemeMainInlineStylesContent()
	{
		$css = '';
		$files = array();

		$oid = aitOptions()->getOid();
		$cacheKey = 'inline-styles-' . $oid;

		if($css = AitCache::load($cacheKey)){
			return $css;
		}else{

			foreach($this->inlineStyleCallbacks as $cb){
				$r = call_user_func($cb);
				$css .= $r['css'];
				$files = array_merge($files, $r['files']);
			}

			$files = array_unique($files);

			$tag = $oid == '' ? 'global' : $oid;
			AitCache::save($cacheKey, $css, array('files' => $files, 'tags' => array($tag)));
		}

		return $css;
	}

	protected function addGoogleFontsCss()
	{
		$asset = array();

		$themeOptions = aitOptions()->getOptionsByType('theme');

		if(!isset($themeOptions['typography'])) return;

		foreach($themeOptions['typography'] as $optionKey => $optionValue){
			$value = AitLangs::getCurrentLocaleText($optionValue, '');

			if(!$value) continue;

			list($fontType, $fontFamily) = array_pad(explode('@', $value), 2, null);

			if(!$fontFamily || $fontType != 'google') continue;

			$font = AitGoogleFonts::getByFontFamily($fontFamily);

			$urlArgs = array(
				'family' => $fontFamily . ':' . implode(',', $font->variants),
				'subset' => implode(',', $font->subsets)
			);

			$fontUrl = add_query_arg($urlArgs, "//fonts.googleapis.com/css");
			$handler = "google-font-".$optionKey; //different handler for asset because we may inlude multiple goolge fonts for multiple sections

			$asset['css'][$handler]['file'] = $fontUrl;
			$asset['css'][$handler]['ver'] = null;
			$this->assetsList[] = array('assets' => $asset, 'params' => array());
		}
	}

}
