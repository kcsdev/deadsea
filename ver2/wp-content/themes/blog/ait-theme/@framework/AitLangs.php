<?php


/**
 * Static wrapper for AIT Languages plugin functionality
 */
class AitLangs
{

	protected static $defaultLang;



	protected static function _defaultLang()
	{
		if(!self::$defaultLang){
			self::$defaultLang = new AitDefaultLanguage();
		}
		return self::$defaultLang;
	}



	/**
	 * Whether AIT Languages Plugin is enabled
	 * @return boolean
	 */
	public static function isEnabled()
	{
		$filtered = apply_filters('ait-langs-enabled', aitIsPluginActive('languages'));

		if($filtered === true and !aitIsPluginActive('languages')){
			return false;
		}else{
			return $filtered;
		}
	}



	/**
	 * Gets default locale of default language
	 * @return string
	 */
	public static function getDefaultLocale()
	{
		if(!self::isEnabled()) return get_locale();

		if(function_exists('aitLangsGetDefaultLanguage')){
			$locale = aitLangsGetDefaultLanguage('locale');
		}else{
			$locale = pll_default_language('locale');
		}

		if(!$locale){
			return get_locale();
		}

		return $locale;
	}



	/**
	 * Gets current locale of current language
	 * @return string
	 */
	public static function getCurrentLocale()
	{
		return get_locale();
	}


	/**
	 * Gets language code (slug) of current language
	 * @return string
	 */
	public static function getCurrentLanguageCode()
	{
		$locale = get_locale();
		if($locale == 'zh_CN'){
			return 'cn';
		}elseif($locale == 'zh_TW'){
			return 'tw';
		}elseif($locale == 'pt_BR'){
			return 'br';
		}else{
			return substr($locale, 0, 2);
		}
	}



	public static function getLocalesList()
	{
		$langs = self::getLanguagesList();
		$locales = array();

		foreach($langs as $lang){
			$locales[] = $lang->locale;
		}

		return $locales;
	}



	public static function getDefaultLang()
	{
		if(!self::isEnabled()) return self::_defaultLang();

		$lang = false;

		if(function_exists('aitLangsGetDefaultLanguage')){
			$lang = aitLangsGetDefaultLanguage();
		}else{
			global $polylang;
			if(isset($polylang->options['default_lang']) && ($default_lang = $polylang->model->get_language($polylang->options['default_lang']))){
				$lang = $default_lang;
			}
		}


		if(!$lang){
			$lang = self::_defaultLang();
		}

		return $lang;
	}



	public static function getCurrentLang()
	{
		if(!self::isEnabled()) return self::_defaultLang();

		if(!is_admin() and !did_action('wp')){
			_doing_it_wrong(__METHOD__, 'You can not get current language on frontend before WP Query (before "wp" action).  Action "template_redirect" is first safe place to use it.', '1.0');
			return self::_defaultLang();
		}

		if(function_exists('aitLangsGetCurrentLanguage')){
			$lang = aitLangsGetCurrentLanguage();
		}else{
			global $polylang;
			$lang = $polylang->curlang;
		}

		return $lang ? $lang : self::_defaultLang();
	}



	public static function isUsedNowDefaultLocale()
	{
		return (self::getCurrentLocale() == self::getDefaultLocale());
	}



	public static function isThisDefaultLocale($locale)
	{
		return (self::getDefaultLocale() == $locale);
	}



	public static function getLanguagesList()
	{
		global $polylang;

		if(self::isEnabled() and $polylang){
			return $polylang->model->get_languages_list();
		}

		return array(self::_defaultLang());
	}



	public static function getSwitcherLanguages()
	{
		if(!self::isEnabled()) return array();

		if(function_exists('aitLangsGetCurrentLanguage')){ // old ait-languages <1.7.x
			return pll_the_languages(array('raw' => true, 'new_structure' => true));
		}else{
			$langs = pll_the_languages(array('raw' => true, 'show_flags' => true));
			$langsObj = array();
			foreach($langs as $lang){
				$langsObj[] = (object) array(
					'id'             => $lang['id'],
					'slug'           => $lang['slug'],
					'name'           => $lang['name'],
					'url'            => $lang['url'],
					'flag'           => $lang['flag'],
					'flagUrl'        => '',
					'isCurrent'      => $lang['current_lang'],
					'hasTranslation' => !$lang['no_translation'],
					'htmlClass'      => implode(' ', $lang['classes']),
				);
			}
			return $langsObj;
		}
	}



	public static function isFilteredOut($lang)
	{
		if(!self::isEnabled()) return false;

		$post = get_post();

		$slug = '';

		if($post and $post->post_status != 'auto-draft'){
			$slug = self::getPostLang($post->ID)->slug;
		}elseif($post and $post->post_status == 'auto-draft'){
			$slug = self::getDefaultLang()->slug;
		}else{
			$slug = self::_getLangForFiltering();
		}

		return ($slug and $lang->slug != $slug);
	}



	protected static function _getLangForFiltering()
	{
		if(function_exists('aitLangsGetLangForFiltering')){
			$slug = aitLangsGetLangForFiltering();
		}else{
			$slug = get_user_meta(get_current_user_id(), 'pll_filter_content', true);
			if(!$slug){
				$slug = get_user_meta(get_current_user_id(), 'ait_langs_content_filtering_lang', true);
			}
		}

		return $slug;
	}



	/**
	 * Returns lang code for filtering content in admin
	 * @return string Lang code as 'en', 'sk' or empty string for all langauges
	 */
	public static function getFilteringLangCode()
	{
		return aitIsPluginActive('languages') ? self::_getLangForFiltering() : '';
	}



	/**
	 * Gets string for current locale
	 * @param  array|object|string $localesAndTexts Associative array of locales and texts, e.g. array('en_US' => 'Some text')
	 * @param  string              $defaultText     Default text when translated string does not exist
	 * @return string                               Text for current locale
	 */
	public static function getCurrentLocaleText($localesAndTexts, $defaultText = '')
	{
		$currentLocale = self::getCurrentLocale();

		if(is_array($localesAndTexts) and isset($localesAndTexts[$currentLocale])){
			return $localesAndTexts[$currentLocale];
		}elseif(is_object($localesAndTexts) and isset($localesAndTexts->{$currentLocale})){
			return $localesAndTexts->{$currentLocale};
		}elseif(is_string($localesAndTexts) and !empty($localesAndTexts)){
			return $localesAndTexts;
		}else{
			return $defaultText;
		}
	}



	/**
	 * Gets string for default locale
	 * @param  array|object|string $localesAndTexts Associative array of locales and texts, e.g. array('en_US' => 'Some text')
	 * @param  string              $defaultText     Default text when translated string does not exist
	 * @return string                               Text for default locale
	 */
	public static function getDefaultLocaleText($localesAndTexts, $defaultText = '')
	{
		$defaultLocale = self::getDefaultLocale();

		if(is_array($localesAndTexts) and isset($localesAndTexts[$defaultLocale])){
			return $localesAndTexts[$defaultLocale];
		}elseif(is_object($localesAndTexts) and isset($localesAndTexts->{$defaultLocale})){
			return $localesAndTexts->{$defaultLocale};
		}elseif(is_string($localesAndTexts) and !empty($localesAndTexts)){
			return $localesAndTexts;
		}else{
			return $defaultText;
		}
	}



	public static function getPostLang($postId)
	{
		if(!self::isEnabled()) return self::_defaultLang();

		global $polylang;

		if(isset($polylang)){
			$lang = $polylang->model->get_post_language($postId);

			if($lang){
				return $lang;
			}
		}

		return self::_defaultLang();
	}



	public static function checkIfPostAndGetLang()
	{
		global $post;

		if($post){
			return self::getPostLang($post->ID);
		}

		return false;
	}



	public static function htmlClass($locale = '')
	{
		$class = array();
		$class[] = self::isEnabled() ? ' ait-langs-enabled ' : '';
		$class[] = $locale ? 'ait-lang-' . $locale : '';
		$class[] = ($locale and $locale == self::getDefaultLocale()) ? 'ait-lang-default' : '';

		$class = apply_filters('ait-langs-html-class', $class);
		return implode(' ', $class);
	}




	public static function getGmapsLang()
	{
		// lang codes map:
		// WP locale => gmaps lang code
		$map = array(
			'bg_BG' => 'bg',
			'cs_CZ' => 'cs',
			'de_DE' => 'de',
			'en_US' => 'en',
			'es_ES' => 'es',
			'fi'    => 'fi',
			'fr_FR' => 'fr',
			'hi_IN' => 'hi',
			'hr'    => 'hr',
			'hu_HU' => 'hu',
			'id_ID' => 'id',
			'it_IT' => 'it',
			'nl_NL' => 'nl',
			'pl_PL' => 'pl',
			'pt_BR' => 'pt-BR',
			'pt_PT' => 'pt-PT',
			'ru_RU' => 'ru',
			'sk_SK' => 'sk',
			'sq'    => 'sq', // it seems gmaps does not support this lang
			'sv_SE' => 'sv',
			'zh_CN' => 'zh-CN',
			'zh_TW' => 'zh-TW',
		);

		$currentLocale = self::getCurrentLocale();

		if(isset($map[$currentLocale])){
			return $map[$currentLocale];
		}

		return 'en';
	}
}
