<?php


/**
 * Additional WpLatte macros specific for AIT Themes
 */
class AitWpLatteMacros extends NMacroSet
{

	public static $config;



	public static function install(NLatteCompiler $compiler, $config = null)
	{
		$me = new self($compiler);

		self::$config = $config;

		$me->addMacro('imageUrl', array($me, 'macroResizedImgUrl'));
		$me->addMacro('includeElement', array($me, 'macroIncludeElement'));
		$me->addMacro('dataAttr', array($me, 'macroDataAttr'));
		$me->addMacro('sidebar', 'dynamic_sidebar(%node.args);');
		$me->addMacro('googleAnalytics',  'echo ' . __CLASS__ . '::googleAnalytics(%node.args);');
	}



	/**
	 * {includeElement $element}
	 * $element is instance of AitElement class
	 */
	public function macroIncludeElement(NMacroNode $node, NPhpWriter $writer)
	{
		$el = $writer->formatArgs();

		$params = array(
			"'el' => $el",
			"'element' => {$el}",
			"'htmlId' => {$el}->getHtmlId()",
			"'htmlClass' => {$el}->getHtmlClass()",
		);

		$paramsStr = implode(', ', $params);

		$code = $writer->write('NCoreMacros::includeTemplate(' . $el . '->getTemplate(), array(' . $paramsStr . ') + $template->getParameters(), $_l->templates[%var])',
			$this->getCompiler()->getTemplateId());

		if ($node->modifiers) {
			return $writer->write('echo %modify(%raw->__toString(TRUE))', $code);
		} else {
			return $code . '->render()';
		}
	}



	public function macroResizedImgUrl(NMacroNode $node, NPhpWriter $writer)
	{
		$url = $node->tokenizer->fetchWord();
		$args = $writer->formatArray();

		if(!AitUtils::contains($args, '=>'))
			$args = substr($args, 6, -1);

		return $writer->write("echo aitResizeImage($url, $args)");
	}



	public function macroDataAttr(NMacroNode $node, NPhpWriter $writer)
	{
		$name = $node->tokenizer->fetchWord();
		$params = $writer->formatArray();

		return "echo aitDataAttr('$name', $params)";
	}



	public static function googleAnalytics($uaCode = '')
	{
		if($uaCode){
			return "
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '{$uaCode}', 'auto');ga('send', 'pageview');
</script>";
		}



		return '';
	}

}
