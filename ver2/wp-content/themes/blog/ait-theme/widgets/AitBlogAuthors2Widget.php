<?php

/*
 * AIT WordPress Theme Framework
 *
 * Copyright (c) 2013, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */


/**
 * Blog Authors Widget Class
 */
class AitBlogAuthors2Widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(false, __('Theme &rarr; Blog Authors', 'ait-admin'));

        /* Set up defaults. */
		$this->defaults = array(
			'title'    => '',
			'gravatar' => '1',
			'count'    => '1',
			'authors'	=> array(),
		);
    }



    function widget($widgetArgs, $instance)
    {
		global $wpdb;

		$widgetArgs = (object) $widgetArgs;

		$title = apply_filters('widget_title',  empty($instance['title']) ? '' : $instance['title']);
		$gravatar = $instance['gravatar'];
		$count = $instance['count'];
		$users = $instance['authors'];

		echo $widgetArgs->before_widget;

		if($title) echo $widgetArgs->before_title . $title . $widgetArgs->after_title;

		?>
		<ul class="blog-authors <?php if($gravatar) echo 'has-avatar'; else echo 'no-avatar'; if(!$count) echo ' no-count'; ?>">
		<?php

		$authors = get_users(array('who' => 'authors', 'include' => $users));

		foreach($authors as $author) {

			$postCount = count_user_posts($author->ID);

			if($postCount >= 1) {

				$authorInfo = get_userdata($author->ID);

				?><li class="blog-author"><a href="<?php echo get_author_posts_url($author->ID) ?>" title="<?php _e('View author archive', 'ait-admin') ?>">

				<div class="blog-author-avatar"><?php
					if($gravatar){
						echo get_avatar($author->ID, 100);
					}
					if($count){
					?><div class="blog-author-postscount">
						<span><?php printf(_n('%s post', '%s posts', 'ait'), $postCount); ?></span>
					</div><?php
					}
					?>
				</div>

				<div class="blog-author-name">

					<?php echo $authorInfo->display_name; ?>

				</div>
				</a></li>
			<?php
			}
		}
		?>
		</ul>
		<?php
		echo $widgetArgs->after_widget;
    }



    function update($new, $old)
    {
		$instance = $old;
		$instance['title']    = strip_tags($new['title']);
		$instance['authors']  = array_map( 'sanitize_key', $new['authors'] );
		$instance['gravatar'] = strip_tags($new['gravatar']);
		$instance['count']    = strip_tags($new['count']);
		return $instance;
    }



    function form($instance)
    {
    	$instance = wp_parse_args( (array) $instance, $this->defaults );

		$title    = esc_attr($instance['title']);
		$gravatar = esc_attr($instance['gravatar']);
		$count    = esc_attr($instance['count']);

		$users = get_users(array('who' => 'authors', 'number' => 9999));
        ?>
         <p>
          <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'ait-admin'); ?></label>
          <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
			<label for="<?php echo esc_attr($this->get_field_id( 'authors' )); ?>"><?php echo __( 'Authors', 'ait-admin' ); ?>:</label>
			<select id="<?php echo esc_attr($this->get_field_id( 'authors' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'authors' )); ?>[]" class="widefat" multiple="multiple" size="4">
			<?php foreach ($users as $author) { ?>
				<?php $authorInfo = get_userdata($author->ID ); ?>
				<option value="<?php echo esc_attr($author->ID) ?>" <?php selected( in_array( $author->ID, (array)$instance['authors'] ) ) ?> ><?php echo $authorInfo->display_name ?></option>
			<?php } ?>
			</select>
		</p>

		<p>
          <input id="<?php echo esc_attr($this->get_field_id('count')); ?>" name="<?php echo esc_attr($this->get_field_name('count')); ?>" type="checkbox" value="1" <?php checked( '1', $count ); ?>/>
          <label for="<?php echo esc_attr($this->get_field_id('count')); ?>"><?php _e('Display Post Count?', 'ait-admin'); ?></label>
        </p>

		<p>
          <input id="<?php echo esc_attr($this->get_field_id('gravatar')); ?>" name="<?php echo esc_attr($this->get_field_name('gravatar')); ?>" type="checkbox" value="1" <?php checked( '1', $gravatar ); ?>/>
          <label for="<?php echo esc_attr($this->get_field_id('gravatar')); ?>"><?php _e('Display Author Gravatar?', 'ait-admin'); ?></label>
        </p>
        <?php
    }
}
