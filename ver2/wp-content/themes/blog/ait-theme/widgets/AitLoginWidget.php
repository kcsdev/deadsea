<?php

/*
 * AIT WordPress Theme Framework
 *
 * Copyright (c) 2013, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */


class AitLoginWidget extends WP_Widget
{
	protected $language;

	function __construct()
	{
		$this->language = defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : "en";
		$widget_ops = array('classname' => 'widget_login', 'description' => __( 'Register or login users form', 'ait-admin') );
		parent::__construct('ait-login', __('Theme &rarr; Login', 'ait-admin'), $widget_ops);
	}



	function widget($args, $instance)
	{
		extract( $args );
		$result = '';

		/* WIDGET CONTENT :: START */
		$result .= $before_widget;
		$title = '';
		if(isset($instance['title_'.$this->language])){
			$title = apply_filters('widget_title', empty($instance['title_'.$this->language]) ? '' : $instance['title_'.$this->language], $instance, $this->id_base);
		}
		$result .= $before_title.$title.$after_title;

		if( is_user_logged_in() ){
			global $wp_roles;
			$currentUser = wp_get_current_user();

			// display user info

			$themeOptions = aitOptions()->getOptionsByType('theme');
			$postCount = intval(count_user_posts($currentUser->ID));

			$entity = WpLatte::createEntity('PostAuthor', $currentUser->ID);

			$backgroundUrl = '';
			if ($entity->meta('user-metabox')) {
				$entityMeta = (object)$entity->meta('user-metabox');
				$backgroundUrl = $entityMeta->authorBackground;
			}
			if($backgroundUrl == '') {
				$backgroundUrl = $themeOptions['header']['headbg']['image'];
			}

			$result .= '<div class="userlogin-container user-logged-in">';
				$result .= '<div class="profile">';
					$result .= '<div class="author-background" style="background-image: url(\''.$backgroundUrl.'\');"></div>';

					$result .= '<div class="profile-avatar">'.get_avatar($currentUser->ID);
						$result .= '<div class="author-posts-count">';

							$result .= '<span>'.sprintf(_n('%s post', '%s posts', 'ait'), $postCount).'</span>';
						$result .= '</div>';
					$result .= '</div>';

					$result .= '<div class="profile-name"><h4>'.$currentUser->user_nicename.'</h4></div>';
					if(isset($currentUser->roles[0])){
						//$result .= '<div class="profile-role"><span>'..'</span><span></span></div>';
					}

					$result .= '<a href="'.admin_url('profile.php').'" title="'.__('Account','ait').'" class="widgetlogin-button-account">'.__('Account','ait').'</a>';
					$result .= '<a href="'.wp_logout_url(home_url()).'" title="'.__('Logout','ait').'" class="widgetlogin-button-logout">'.__('Logout','ait').'</a>';
					$result .= '<div class="main-buttons">';
					$result .= '<a href="'.get_author_posts_url($currentUser->ID).'" title="'.__('My Blog','ait').'" class="widgetlogin-button-blog">'.__('My Blog','ait').'</a>';
					$result .= '<a href="'.admin_url('edit.php?post_type=post&author='.$currentUser->ID).'" title="'.__('Posts','ait').'" class="widgetlogin-button-items">'.__('Add Post','ait').'</a>';
					$result .= '</div>';

				$result .= '</div>';
			$result .= '</div>';
		} else {
			// register / login
			// redirect to custom page
			$redirectTo = home_url();
			$themeOptions = aitOptions()->getOptionsByType('theme');

		    if(isset($themeOptions['authors']['registrationRedirect']) && $themeOptions['authors']['registrationRedirect'] != '0') {
		      $redirectTo = get_permalink( $themeOptions['authors']['registrationRedirect'] );
		    }

			$result .= '<div class="userlogin-container user-not-logged-in">';
				$result .= '<div class="userlogin-tabs">';
					$result .= '<div class="userlogin-tabs-menu">';
						$result .= '<a class="userlogin-option-active" href="#">'.__('Login', 'ait').'</a>';
						if (get_option( 'users_can_register' )) {
							$result .= '<a href="#">'.__('Register', 'ait').'</a>';
						}
					$result .= '</div>';
					$result .= '<div class="userlogin-tabs-contents">';
						$result .= '<div class="userlogin-tabs-content userlogin-option-active">';
							$result .= '<!-- LOGIN TAB -->';
							$result .= $instance['description_login_'.$this->language] != '' ? '<p>'.$instance['description_login_'.$this->language].'</p>' : '';
							$result .= wp_login_form( array( 'redirect' => get_permalink(), 'form_id' => 'ait-login-form-widget', 'echo' => false, 'remember' => false,) );
							$result .= '<a href="'.wp_lostpassword_url(get_permalink()).'" class="lost-password" title="'.__('Lost Password?', 'ait').'">'.__('Lost Password?', 'ait').'</a>';
						$result .= '</div>';
						$result .= '<div class="userlogin-tabs-content">';

						if (get_option( 'users_can_register' )) {
							$result .= '<!-- REGISTER TAB -->';
							$result .= $instance['description_register_'.$this->language] != '' ? '<p>'.$instance['description_register_'.$this->language].'</p>' : '';
							$result .= '<form method="post" action="'.home_url('/?ait-action=register').'" class="wp-user-form user-register-form">';
								$result .= '<p class="input-container input-username">';
									$result .= '<label for="user_login">'.__('Username', 'ait').'</label>';
									$result .= '<input type="text" name="user_login" id="user_name" value="" size="20" tabindex="101" />';
								$result .= '</p>';
								$result .= '<p class="input-container input-email">';
									$result .= '<label for="user_email">'.__('Email', 'ait').'</label>';
									$result .= '<input type="text" name="user_email" id="user_email" value="" size="20" tabindex="102" />';
								$result .= '</p>';



								$result .= '<div class="login-fields">';
									do_action('register_form');
									$result .= '<input type="submit" name="user-submit" value="'.__('Sign up!', 'ait').'" class="user-submit" tabindex="103" />';
									$result .= '<input type="hidden" name="redirect_to" value="'.$redirectTo.'" />';
									$result .= '<input type="hidden" name="user-cookie" value="1" />';
								$result .= '</div>';

								$result .= '<div class="login-messages">';
									$result .= '<div class="login-message-error" style="display: none">'.__('Please fill out all registration fields','ait').'</div>';
								$result .= '</div>';

							$result .= '</form>';
						}

						$result .= '</div>';
					$result .= '</div>';
				$result .= '</div>';
			$result .= '</div>';
		}

		$result .= $after_widget;
		/* WIDGET CONTENT :: END */
		echo($result);

		?>


		<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".userlogin-tabs-contents input[type=text], .userlogin-tabs-contents input[type=password]").each(function(){
				var $label = jQuery(this).parent().find("label");
				var placeholder = $label.html();
				jQuery(this).attr("placeholder", placeholder);
				$label.hide();
			});

			var $tabs = jQuery(".userlogin-container .userlogin-tabs-menu a");
			var $contents = jQuery(".userlogin-container .userlogin-tabs-contents");
			var activeClass = "userlogin-option-active";
			$tabs.each(function(){
				jQuery(this).click(function(e){
					e.preventDefault();
					$tabs.each(function(){
						jQuery(this).removeClass(activeClass);
					});
					$contents.find(".userlogin-tabs-content").each(function(){
						jQuery(this).removeClass(activeClass);
					});
					jQuery(this).addClass(activeClass);
					$contents.find(".userlogin-tabs-content:eq("+jQuery(this).index()+")").addClass(activeClass);
				});
			});



			jQuery("form.user-register-form").on("submit", function(){
				var $inputs = jQuery(this).find("input[type=text]");
				var $selects = jQuery(this).find("select:not(:disabled)");
				var valid = false;
				var all = parseInt($selects.length + $inputs.length);
				var validation = 0;
				$selects.each(function(){
					if(jQuery(this).val() != "-1"){
						validation = validation + 1;
					}
				});
				$inputs.each(function(){
					if(jQuery(this).val() != ""){
						if(jQuery(this).attr("name") == "user_email"){
							validation = validation + 1;
						} else {
							validation = validation + 1;
						}
					}
				});
				if(validation == all){
					valid = true;
				}
				if(!valid){
					jQuery(this).find(".login-message-error").fadeIn("slow"); jQuery(this).find(".login-message-error").on("hover", function(){ jQuery(this).fadeOut("fast"); });
					return false;
				}
			});
		});
		</script>
		<?php
	}



	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title_'.$this->language] = strip_tags($new_instance['title_'.$this->language]);
		$instance['description_login_'.$this->language] = strip_tags($new_instance['description_login_'.$this->language]);
		$instance['description_logout_'.$this->language] = strip_tags($new_instance['description_logout_'.$this->language]);
		$instance['description_register_'.$this->language] = strip_tags($new_instance['description_register_'.$this->language]);

		return $instance;
	}



	function form($instance)
	{
		$instance = wp_parse_args( (array) $instance, array(
            'title_'.$this->language => '',
            'description_login_'.$this->language => '',
            'description_logout_'.$this->language => '',
            'description_register_'.$this->language => '',
        ) );
    ?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title_'.$this->language )); ?>"><?php echo __( 'Title', 'ait-admin' ); ?>:</label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title_'.$this->language )); ?>" value="<?php echo esc_attr($instance['title_'.$this->language]); ?>"class="widefat" style="width:100%;" />
        </p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id( 'description_login_'.$this->language )); ?>"><?php echo __( 'Login Description', 'ait-admin' ); ?>:</label>
			<textarea class="widefat" rows="5" cols="20" id="<?php echo esc_attr($this->get_field_id( 'description_login_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'description_login_'.$this->language )); ?>"><?php echo htmlspecialchars($instance['description_login_'.$this->language]); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'description_logout_'.$this->language )); ?>"><?php echo __( 'Logout Description', 'ait-admin' ); ?>:</label>
			<textarea class="widefat" rows="5" cols="20" id="<?php echo esc_attr($this->get_field_id( 'description_logout_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'description_logout_'.$this->language )); ?>"><?php echo htmlspecialchars($instance['description_logout_'.$this->language]); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'description_register_'.$this->language )); ?>"><?php echo __( 'Register Description', 'ait-admin' ); ?>:</label>
			<textarea class="widefat" rows="5" cols="20" id="<?php echo esc_attr($this->get_field_id( 'description_register_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'description_register_'.$this->language )); ?>"><?php echo htmlspecialchars($instance['description_register_'.$this->language]); ?></textarea>
		</p>
<?php
	}

}
