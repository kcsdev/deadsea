<?php

/*
 * AIT WordPress Theme Framework
 *
 * Copyright (c) 2013, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */


class AitPosts2Widget extends WP_Widget
{
	protected $language;

	function __construct()
	{
		$this->language = defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : "en";
		$widget_ops = array('classname' => 'widget_posts', 'description' => __( 'Customize displaying posts', 'ait-admin') );
		parent::__construct('ait-posts', __('Theme &rarr; Posts', 'ait-admin'), $widget_ops);
	}



	function widget($args, $instance)
	{
		extract( $args );
		$result = '';

		//global $wp_query;
		$query = array();
		if(!empty($instance['number_of_posts_'.$this->language])){
			$query['posts_per_page'] = intval($instance['number_of_posts_'.$this->language]);
		} else {
			$query['posts_per_page'] = 1;
		}
		/*$formats = get_post_format_slugs();
		foreach ((array) $formats as $i => $format ) {
			$formats[$i] = 'post-format-' . $format;
		}
		$query['tax_query'] = array(array('taxonomy' => 'post_format', 'field' => 'slug', 'terms' => $formats, 'operator' => 'NOT IN'));*/

		/* WIDGET CONTENT :: START */
		$result .= $before_widget;
		$title = '';
		if(isset($instance['title_'.$this->language])){
			$title = apply_filters('widget_title', empty($instance['title_'.$this->language]) ? '' : $instance['title_'.$this->language], $instance, $this->id_base);
		}
		$result .= $before_title.$title.$after_title;
		$i = 1;
		$posts = get_posts( $query );
		$num_posts = sizeof( $posts );
		if(!empty($posts)){
			$result .= '<div class="postitems-wrapper">';
			foreach($posts as $post){
				if (!empty($instance['excerpt_length_'.$this->language])){
					if(function_exists('iconv')){
						$text = iconv_substr(strip_tags(strip_shortcodes($post->post_content)), 0, $instance['excerpt_length_'.$this->language], 'UTF-8');
					} else {
						$text = substr( strip_tags(strip_shortcodes($post->post_content)), 0, $instance['excerpt_length_'.$this->language] );
					}
				} else {
					$text = $post->excerpt;
				}
				$thumbnail_args = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				switch ( $instance['thumbnail_position_'.$this->language] ) {
					case 'left': $thumbnail_class = 'fl'; $offset = 'margin-left: '.$instance['thumbnail_width_'.$this->language].'px;'; break;
					case 'right': $thumbnail_class = 'fr'; $offset = 'margin-right: '.$instance['thumbnail_width_'.$this->language].'px;'; break;
					default: $thumbnail_class = 'top'; $offset = '';
				}
				$post_class = 'no-thumbnail';
				if(has_post_thumbnail($post->ID) && $instance['show_thumbnails_'.$this->language]){$post_class = 'with-thumbnail';}
				if($i == $num_posts){$post_class .= ' last';}

				$result .= '<div class="postitem thumb-'.$thumbnail_class.' '.$post_class.'">';
				$result .= '<a href="'.get_permalink($post->ID).'" class="thumb-link">';
				if(has_post_thumbnail($post->ID) && $instance['show_thumbnails_'.$this->language]){
					$result .= '<div class="thumb-wrap" style="width: '.$instance['thumbnail_width_'.$this->language].'px;">';
					$result .= '<span class="thumb-icon" style="background-image: url('.aitResizeImage($thumbnail_args['0'], array('width' => $instance['thumbnail_width_'.$this->language], 'height' => $instance['thumbnail_height_'.$this->language], 'crop' => true)).');"></span>';
					$result .= '</div>';
				}
					$result .= '<div class="info-wrap">';
						$result .= '<div class="info-content">';
							$result .= '<div class="post-title"><h4>'.esc_attr(strip_tags($post->post_title)).'</h4></div>';
							$result .= '<div class="date">'.mysql2date(get_option('date_format'), $post->post_date, true).'</div>';
						$result .= '</div>';
					$result .= '</div>';
				$result .= '</a>';

				$result .= '<div class="post-content">';
					$result .= '<p>'.$text.' ';
				if(!empty($instance['show_read_more_'.$this->language])){
					$result .= '<a class="read-more" href="'.get_permalink($post->ID).'">| '.__('read more', 'ait').'</a>';
					$result .= '</p>';
				}
				$result .= '</div>';
				$result .= '</div>';
				$i++;
			}
			$result .= '</div>';
		} else {
			$result .= '<div class="postitems-wrapper">';
			$result .= '<div class="no-content">'.__('No posts', 'ait').'</div>';
			$result .= '</div>';
		}

		$result .= $after_widget;
		/* WIDGET CONTENT :: END */
		//wp_reset_query();
		echo($result);
	}



	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title_'.$this->language] = strip_tags($new_instance['title_'.$this->language]);
		$instance['number_of_posts_'.$this->language] = $new_instance['number_of_posts_'.$this->language];
		$instance['excerpt_length_'.$this->language] = $new_instance['excerpt_length_'.$this->language];
		$instance['show_read_more_'.$this->language] = $new_instance['show_read_more_'.$this->language];
		$instance['show_thumbnails_'.$this->language] = $new_instance['show_thumbnails_'.$this->language];
		$instance['thumbnail_width_'.$this->language] = $new_instance['thumbnail_width_'.$this->language];
		$instance['thumbnail_height_'.$this->language] = $new_instance['thumbnail_height_'.$this->language];
		$instance['thumbnail_position_'.$this->language] = $new_instance['thumbnail_position_'.$this->language];

		return $instance;
	}



	function form($instance)
	{
		$instance = wp_parse_args( (array) $instance, array(
            'title_'.$this->language => '',
            'number_of_posts_'.$this->language => 3,
            'excerpt_length_'.$this->language => 135,
			'show_read_more_'.$this->language => true,
            'show_thumbnails_'.$this->language => true,
            'thumbnail_width_'.$this->language => 150,
            'thumbnail_height_'.$this->language => 150,
            'thumbnail_position_'.$this->language => 'left',
        ) );
    ?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title_'.$this->language )); ?>"><?php echo __( 'Title', 'ait-admin' ); ?>:</label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title_'.$this->language )); ?>" value="<?php echo esc_attr($instance['title_'.$this->language]); ?>"class="widefat" style="width:100%;" />
        </p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'number_of_posts_'.$this->language )); ?>"><?php echo __( 'Number of posts', 'ait-admin' ); ?>:</label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'number_of_posts_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number_of_posts_'.$this->language )); ?>" value="<?php echo esc_attr($instance['number_of_posts_'.$this->language])?>" size="2" />
        </p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'excerpt_length_'.$this->language )); ?>"><?php echo __( 'Excerpt length', 'ait-admin' ); ?>:</label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'excerpt_length_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'excerpt_length_'.$this->language )); ?>" value="<?php echo esc_attr($instance['excerpt_length_'.$this->language])?>" size="2" />
        </p>

        <p>
            <?php $checked = ''; if ( $instance['show_read_more_'.$this->language] ) $checked = 'checked="checked"'; ?>
			<input type="checkbox" <?php echo $checked; ?> id="<?php echo esc_attr($this->get_field_id( 'show_read_more_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_read_more_'.$this->language )); ?>" class="checkbox" />
			<label for="<?php echo esc_attr($this->get_field_id( 'show_read_more_'.$this->language )); ?>"><?php echo __( 'Show read more', 'ait-admin' ); ?></label>
        </p>

        <p>
            <?php $checked = ''; if ( $instance['show_thumbnails_'.$this->language] ) $checked = 'checked="checked"'; else $checked = ''; ?>
			<input type="checkbox" <?php echo $checked; ?> id="<?php echo esc_attr($this->get_field_id( 'show_thumbnails_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_thumbnails_'.$this->language )); ?>" class="checkbox" />
			<label for="<?php echo esc_attr($this->get_field_id( 'show_thumbnails_'.$this->language )); ?>"><?php echo __( 'Show thumbnails', 'ait-admin' ); ?></label>
        </p>


<?php
	}

}
