<?php

/*
 * AIT WordPress Theme Framework
 *
 * Copyright (c) 2013, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */


class AitSocial2Widget extends WP_Widget
{
	protected $language;

	function __construct()
	{
		$this->language = defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : "en";
		$widget_ops = array('classname' => 'widget_social', 'description' => __( 'Display social icons for current page', 'ait-admin') );
		parent::__construct('ait-social', __('Theme &rarr; Social Icons', 'ait-admin'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract( $args );
		$result = '';
		$target = '';
		$themeOptions = aitOptions()->get('theme');
		$locale = AitLangs::getCurrentLocale();

		// WIDGET CONTENT :: START
		$result .= $before_widget;
		$title = '';
		if(isset($instance['title_'.$this->language])){
			$title = apply_filters('widget_title', empty($instance['title_'.$this->language]) ? '' : $instance['title_'.$this->language], $instance, $this->id_base);
		}
		$result .= $before_title.$title.$after_title;

		if($themeOptions->social->socIconsNewWindow){$target = 'target = "_blank"';}
		$result .= $instance['show_icon_titles_'.$this->language] ? '<ul><!--' : '<ul class="no-titles"><!--';

		foreach($themeOptions->social->socIcons as $icon){

			$iconTitle = '';
			$iconImage = '';

			if ($instance['show_icon_titles_'.$this->language]) {
				$iconTitle = (isset($locale) && isset($icon->title->{$locale})) ? $icon->title->{$locale} : '';
			}

			// depending to widget option firstly use font icon and if not found use image
			if ($instance['icon_type_'.$this->language] == 'font') {
				$iconImage = $icon->iconFont != '' ? '<i class="fa '.$icon->iconFont.'"></i>' : '<img src="'.$icon->icon.'" class="s-icon" alt="icon">' ;
			}else {
				$iconImage = $icon->icon != '' ? '<img src="'.$icon->icon.'" class="s-icon" alt="icon">' : '<i class="fa '.$icon->iconFont.'"></i>' ;
			}

			if (isset($iconImage) && $iconImage != "") {
				$result .= '--><li><a href="'.$icon->url.'" '.$target.'>'.$iconImage.'<span class="s-title">'. $iconTitle.'</span></a></li><!--';
			}

		}
		$result .= '--></ul>';

		$result .= $after_widget;
		// WIDGET CONTENT :: END
		echo($result);
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title_'.$this->language] = strip_tags($new_instance['title_'.$this->language]);
		$instance['show_icon_titles_'.$this->language] = $new_instance['show_icon_titles_'.$this->language];
		$instance['icon_type_'.$this->language] = $new_instance['icon_type_'.$this->language];
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
            'title_'.$this->language => '',
            'show_icon_titles_'.$this->language => true,
            'icon_type_'.$this->language => 'image'
        ) );
    ?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title_'.$this->language)); ?>"><?php _e('Title:', 'ait-admin'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title_'.$this->language)); ?>" name="<?php echo esc_attr($this->get_field_name('title_'.$this->language)); ?>" type="text" value="<?php echo esc_attr($instance['title_'.$this->language]); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'icon_type_'.$this->language )); ?>"><?php echo __( 'Icon Type', 'ait-admin' ); ?>:</label>
			<select id="<?php echo esc_attr($this->get_field_id( 'icon_type_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'icon_type_'.$this->language )); ?>">
				<option <?php if ( 'font' == $instance['icon_type_'.$this->language] ) echo 'selected="selected"'; ?> value="font"><?php echo __( 'Icon Font', 'ait-admin' ); ?></option>
				<option <?php if ( 'image' == $instance['icon_type_'.$this->language] ) echo 'selected="selected"'; ?> value="image"><?php echo __( 'Icon Image', 'ait-admin' ); ?></option>
			</select>
		</p>

		<p>
            <?php $checked = ''; if ( $instance['show_icon_titles_'.$this->language] ) $checked = 'checked="checked"'; ?>
			<input type="checkbox" <?php echo $checked; ?> id="<?php echo esc_attr($this->get_field_id( 'show_icon_titles_'.$this->language )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_icon_titles_'.$this->language )); ?>" class="checkbox" />
			<label for="<?php echo esc_attr($this->get_field_id( 'show_icon_titles_'.$this->language )); ?>"><?php echo __( 'Show icon title', 'ait-admin' ); ?></label>
        </p>
<?php
	}

}
