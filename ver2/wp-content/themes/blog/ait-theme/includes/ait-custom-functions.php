<?php


add_filter('body_class', 'AitAddClassIfGaleryExists', 11, 2);
function AitAddClassIfGaleryExists($classes, $class)
{
	if (aitPostGalleryExists()) $classes[] = 'post-gallery';
	return $classes;
}



function aitPostGalleryExists()
{
	if(is_singular(array('post'))){
		global $post;
		$meta = get_post_meta( $post->ID, '_post_post-gallery-metabox' );
		if (empty($meta)) {
			return false;
		}
		if (empty($meta[0]['inputs'])) {
			return false;
		}
		return true;
	}
}




function aitPostHasBackground()
{
  if(is_singular(array('post'))){
	global $post;
	$meta = get_post_meta( $post->ID, '_post_post-background-metabox' );
	if (empty($meta)) return false;
	if (empty($meta[0]['postBackground'])) return false;
	return true;
  }
}




function aitGetImageRatio($imagePath)
{
	list($width, $height, $type, $attr) = getimagesize($imagePath);
 	return ($width / $height);
}




function aitGetThumbnailTitle($postId)
{
  $thumbnailId = get_post_thumbnail_id($postId == 0 ? null : $postId);
  return get_post($thumbnailId)->post_title;;
}




add_filter( 'login_redirect', 'aitLoginRedirect', 10, 3 );
function aitLoginRedirect( $redirect_to, $request, $user ) {
  global $user;

  $themeOptions = aitOptions()->getOptionsByType('theme');
  if ( isset($user->roles) && is_array( $user->roles ) && (in_array( 'author', $user->roles ) || in_array( 'administrator', $user->roles ) ) ) {
	if(isset($themeOptions['authors']['loginRedirect']) && $themeOptions['authors']['loginRedirect'] != '0') {
	  return get_permalink( $themeOptions['authors']['loginRedirect'] );
	} else {
	  return get_author_posts_url( $user->ID );
	}
  } else {
	return $redirect_to;
  }
}



// remove items from admin bar for non-administrators
add_action( 'admin_bar_menu', 'aitRemoveAdminMenu', 999);
function aitRemoveAdminMenu()
{
  if ( !current_user_can( 'administrator' ) ) {
	global $wp_admin_bar;
	$wp_admin_bar->remove_node( 'ait-admin-menu' );
	$wp_admin_bar->remove_node( 'page-builder' );
	$wp_admin_bar->remove_node('new-ait-toggle');
	$wp_admin_bar->remove_node('new-ait-ad-space');
  }
}




// remove items from admin bar for non-administrators
add_action( 'admin_menu', 'aitRemoveMenuItems' );
function aitRemoveMenuItems() {
	if( !current_user_can( 'administrator' ) ):
		remove_menu_page( 'edit.php?post_type=ait-ad-space' );
		remove_menu_page( 'edit.php?post_type=ait-toggle' );
	endif;
}




global $wp_version;
if (version_compare($wp_version, '4.0.1', '<=')) {
  // version is 4.0.1 or less
  add_action( 'personal_options', array ( 'AitUserBioFix', 'start' ) );
}


class AitUserBioFix
{
	public static function start()
	{
		$action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
		add_action( $action, array ( __CLASS__, 'stop' ) );
		ob_start();
	}

	/**
	 * Strips the bio box from the buffered content.
	 *
	 * @return void
	 */
	public static function stop()
	{
		$html = ob_get_contents();
		ob_end_clean();
		// catch the table row
	$pattern = '~<tr>\s*<th><label for="description".*</tr>~imsUu';
	preg_match($pattern, $html, $matches, PREG_OFFSET_CAPTURE, 3);
		$tableRow = $matches[0][0];

		// add class to the table row
	$pattern = '<tr>';
	preg_match($pattern, $tableRow, $matches, PREG_OFFSET_CAPTURE, 3);
		$tableRow = preg_replace( $pattern, 'tr class="user-description-wrap"', $tableRow );

		$html = preg_replace( '~<tr>\s*<th><label for="description".*</tr>~imsUu', $tableRow, $html );
		print $html;
	}
}




/**
 * Version: 2.0.1
 * Author: Damir Calusic
 * Author URI: http://www.damircalusic.com/
 * License: GPLv2
 */
function um_filter_media_files($wp_query)
{
  global $current_user;

  if(!current_user_can('manage_options') && (is_admin() && $wp_query->query['post_type'] === 'attachment'))
	$wp_query->set('author', $current_user->ID);
}

function um_recount_attachments($counts_in)
{
  global $wpdb;
  global $current_user;

  $and = wp_post_mime_type_where(''); //Default mime type //AND post_author = {$current_user->ID}
  $query = $wpdb->prepare(
  	"SELECT post_mime_type, COUNT( * ) AS num_posts FROM $wpdb->posts
  		WHERE post_type = 'attachment' AND post_status != 'trash' AND post_author = %d $and
  		GROUP BY post_mime_type", $current_user->ID
  	);
  $count = $wpdb->get_results( $query, ARRAY_A );

  $counts = array();
  foreach((array)$count as $row)
	$counts[ $row['post_mime_type'] ] = $row['num_posts'];

	$query = $wpdb->prepare(
		"SELECT COUNT( * ) FROM $wpdb->posts
			WHERE post_type = 'attachment' AND post_author = %d AND post_status = 'trash' $and", $current_user->ID
		);
  $counts['trash'] = $wpdb->get_var( $query );
  return $counts;
};

add_filter('wp_count_attachments', 'um_recount_attachments');
add_action('pre_get_posts', 'um_filter_media_files');





$aitDemoUsersIdList = array(1);

add_action('ait-before-import', 'aitImportDemoUsers');
function aitImportDemoUsers($whatToImport)
{
  if ($whatToImport == "demo-content") {

	$content = @file_get_contents(dirname( __FILE__ ) . '/ait-demo-users.json');
	if(!$content){
	  return array();
	}

	$usersList = json_decode($content);

	$idList = array();

	foreach ($usersList->users as $user){
	  $userdata = array(
		'user_login'   => $user->name,
		'role'         => "author",
		'display_name' => $user->display_name,
		'user_pass'    => NULL  // When creating an user, `user_pass` is expected.
	  );

	  $user_id = wp_insert_user($userdata);
	  if(!is_wp_error($user_id)){

		global $aitDemoUsersIdList;
		array_push($aitDemoUsersIdList, $user_id);

		update_user_meta( $user_id, 'description_en', $user->bio );
	  }
	}


  }
}



add_action( 'ait-after-import', 'aitAssignPostsAuthor', 15);

function aitAssignPostsAuthor($whatToImport)
{
	if($whatToImport == "demo-content"){
		global $polylang;
		if(isset($polylang) and $polylang instanceof PLL_Admin and isset($polylang->filters_post) and $polylang->filters_post){
			remove_filter('save_post', array($polylang->filters_post, 'save_post'), 21);
		}
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'post',
			'post_status'      => 'publish'
		);

		$posts = get_posts( $args );
		global $aitDemoUsersIdList;

		foreach($posts as $post){
			$randomId = array_rand($aitDemoUsersIdList, 1);
			$post->post_author = $aitDemoUsersIdList[$randomId];
			wp_update_post($post);
		}
	}

}



function aitFixPostCoordinates( $postId, $post, $metabox, $data ) {
	// do a query for every post
	// check the current value with the already existing values
	// if there is a match, move the current point by approx. 1 ~ 3 meters

	$position_current = array(
	'lat'	=> $data['map']['latitude'],
	'lng'	=> $data['map']['longitude'],
	);

	$query = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => -1,
	));

	// quick-edit fix
	if(empty($data)){
		$d = get_post_meta($postId, '_post_post-gps-metabox');
		$data = reset($d);
	}

	// metabox doesn't exist yet - post is auto-draft
	if ( $data == NULL )
        return false;

	if($query->found_posts > 0){
		foreach($query->posts as $index => $post){
			if($post->ID != $postId){
				$meta = get_post_meta( $post->ID, '_post_post-gps-metabox');
				$meta_data = reset($meta);
				if($meta != false){
					if($meta_data['map']['latitude'] != '0' or $meta_data['map']['longitude'] != '0'){
						$position_old = array(
							'lat'	=> $meta_data['map']['latitude'],
							'lng'	=> $meta_data['map']['longitude'],
						);

						if($position_current['lat'] == $position_old['lat'] && $position_current['lng'] == $position_old['lng']){
							$position_new = aitMoveLocationByMeters($position_current['lat'], $position_current['lng'], mt_rand(1,30), mt_rand(1,30));
							$data['map']['latitude'] = $position_new['lat'];
							$data['map']['longitude'] = $position_new['lng'];
						}
					}
					$data['map']['address'] = str_replace(array('\"','"'), "", $data['map']['address']);
				}
			}
		}
	}
	update_post_meta($postId, '_post_post-gps-metabox', $data);
}




function aitMoveLocationByMeters($lat, $lng, $top, $left){
	// Earth’s radius, sphere
	$R = 6378137;

	// Coordinate offsets in radians
	$dLat = floatval($top) / $R;
	$dLng = floatval($left) / ( $R * cos(pi() * floatval($lat) / 180) );

	// OffsetPosition, decimal degrees
	$nlat = floatval($lat) + ( $dLat * 180 / pi() );
	$nlng = floatval($lng) + ( $dLng * 180 / pi() );

	return array( 'lat' => $nlat, 'lng' => $nlng);
}
