<?php

function aitGetUsersByRoles($roles)
{
	$users = array();
	if ( in_array('0', $roles) ) {
		$users_query = new WP_User_Query( array(
			'role' => '',
		) );
		$results = $users_query->get_results();
		foreach ($results as $key) {
			array_push($users, $key->ID);
		}
	}
	else {
		foreach ($roles as $key => $role) {
			$users_query = new WP_User_Query( array(
				'role' => $role,
			) );
			$results = $users_query->get_results();
			foreach ($results as $key) {
				array_push($users, $key->ID);
			}
		}
	}
	return $users;
}

?>