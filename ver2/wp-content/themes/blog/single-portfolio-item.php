{block content}

	{* template for page title is in parts/page-title.php *}

	{loop as $post}
		<div class="detail-half-content detail-portfolio-content">
				<div class="detail-thumbnail">
					{var $meta = $post->meta('portfolio-item')}
					{var $pictures = $meta->pictures}
					{if $pictures != '' && count($pictures) != 0}
						{* tu pride easy slider *}
						<section class="elm-main elm-easy-slider-main">
							<div class="elm-easy-slider-wrapper">
								<div class="elm-easy-slider easy-pager-thumbnails pager-pos-outside detail-thumbnail-wrap detail-thumbnail-slider">
									<div class="loading"><span class="ait-preloader">{!__ 'Loading&hellip;'}</span></div>
									<ul class="easy-slider">
										<li>
											{if $meta->type == 'video'}
												{if strpos($meta->videoUrl, 'youtube') !== false}
													{var $parse = parse_url($meta->videoUrl)}
													{!= parse_str($parse['query'], $query)}
													{var $clipId = $query['v']}
													<span class="easy-thumbnail">
														<span class="easy-title">{!$post->title}</span>
														<iframe id="{$clipId}" src="http://www.youtube.com/embed/{$clipId}?wmode=opaque&showinfo=0&enablejsapi=1" width="{$vidWidth}" height="{$vidHeight}" class="detail-youtube"></iframe>

													</span>
												{else}
													{var $clipId = trim(str_replace("vimeo.com", "", str_replace("/", "" ,str_replace("http:", "", $meta->videoLink))))}
													<span class="easy-thumbnail">
														<span class="easy-title">{!$post->title}</span>
														<iframe id="{$clipId}" src="http://player.vimeo.com/video/{$clipId}?title=0&amp;byline=0&amp;portrait=0" width="{$vidWidth}" height="{$vidHeight}" class="detail-vimeo"></iframe>
													</span>
												{/if}
											{else}
												<a href="{$post->imageUrl}" target="_blank" rel="item-gallery">
													<span class="easy-thumbnail">
														<span class="easy-title">{!$post->title}</span>
														<img src="{imageUrl $post->imageUrl, width => 640, crop => 1}" alt="{$post->title}" />
													</span>
												</a>
											{/if}
										</li>
										{foreach $pictures as $picture}
										<li>
											{if strpos($picture['link'], 'youtube') !== false}
												{var $parse = parse_url($picture['link'])}
												{!= parse_str($parse['query'], $query)}
												{var $clipId = $query['v']}
												<span class="easy-thumbnail">
													{if $picture['title'] != ""}<span class="easy-title">{$picture['title']}</span>{/if}
													<iframe id="{$clipId}" src="http://www.youtube.com/embed/{$clipId}?wmode=opaque&showinfo=0&enablejsapi=1" width="{$vidWidth}" height="{$vidHeight}" class="detail-youtube"></iframe>
												</span>
											{elseif strpos($picture['link'], 'vimeo') !== false}
												{var $clipId = trim(str_replace("vimeo.com", "", str_replace("/", "" ,str_replace("http:", "", $picture['link']))))}
												<span class="easy-thumbnail">
													{if $picture['title'] != ""}<span class="easy-title">{$picture['title']}</span>{/if}
													<iframe id="{$clipId}" src="http://player.vimeo.com/video/{$clipId}?title=0&amp;byline=0&amp;portrait=0" width="{$vidWidth}" height="{$vidHeight}" class="detail-vimeo"></iframe>
												</span>
											{else}
												<a href="{imageUrl $picture['image'], width => 1000, crop => 1}" target="_blank" rel="item-gallery">
													<span class="easy-thumbnail">
														{if $picture['title'] != ""}<span class="easy-title">{$picture['title']}</span>{/if}
														<img src="{imageUrl $picture['image'], width => 640, crop => 1}" alt="{$picture['title']}" />
													</span>
												</a>
											{/if}
										</li>
										{/foreach}
									</ul>


									<div class="easy-slider-pager">
										{if $meta->type == 'video'}
											{if strpos($meta->videoUrl, 'youtube') !== false}
													{var $parse = parse_url($meta->videoUrl)}
													{!= parse_str($parse['query'], $query)}
													{var $clipId = $query['v']}
												<a data-slide-index="0" href="#" title="{$post->title}">
													<span class="entry-thumbnail-icon">
														<img src="http://img.youtube.com/vi/{$clipId}/1.jpg" alt="{!$picture['title']}" class="detail-image">
													</span>
												</a>
												{elseif strpos($meta->videoUrl, 'vimeo') !== false}
													{var $clipId = trim(str_replace("vimeo.com", "", str_replace("/", "" ,str_replace("http:", "", $meta->videoUrl))))}
													{var $clipData = json_decode(file_get_contents('http://vimeo.com/api/v2/video/'.$clipId.'.json'))}
												<a data-slide-index="0" href="#" title="{$post->title}">
													<span class="entry-thumbnail-icon">
														<img src="{$clipData[0]->thumbnail_small}" alt="{!$picture['title']}" class="detail-image">
													</span>
												</a>
												{else}
												<a data-slide-index="0" href="#" title="{$post->title}">
													<span class="entry-thumbnail-icon">
														<img src="{imageUrl $post->imageUrl, width => 100, height => 75, crop => 1}" alt="{!$post->title}" class="detail-image">
													</span>
												</a>
											{/if}
										{else}
										<a data-slide-index="0" href="#" title="{$post->title}">
											<span class="entry-thumbnail-icon">
												<img src="{imageUrl $post->imageUrl, width => 100, height => 75, crop => 1}" alt="{!$post->title}" class="detail-image">
											</span>
										</a>
										{/if}
										{foreach $pictures as $picture}
											{if $picture['image'] == "" && strpos($picture['link'], 'youtube') !== false}
												{var $parse = parse_url($picture['link'])}
												{!= parse_str($parse['query'], $query)}
												{var $clipId = $query['v']}
											<a data-slide-index="{$iterator->getCounter()}" href="#" title="{$picture['title']}">
												<span class="entry-thumbnail-icon">
													<img src="http://img.youtube.com/vi/{$clipId}/1.jpg" alt="{!$picture['title']}" class="detail-image">
												</span>
											</a>
											{elseif $picture['image'] == "" && strpos($picture['link'], 'vimeo') !== false}
												{var $clipId = trim(str_replace("vimeo.com", "", str_replace("/", "" ,str_replace("http:", "", $picture['link']))))}
												{var $clipData = json_decode(file_get_contents('http://vimeo.com/api/v2/video/'.$clipId.'.json'))}
											<a data-slide-index="{$iterator->getCounter()}" href="#" title="{$picture['title']}">
												<span class="entry-thumbnail-icon">
													<img src="{$clipData[0]->thumbnail_small}" alt="{!$picture['title']}" class="detail-image">
												</span>
											</a>
											{else}
											<a data-slide-index="{$iterator->getCounter()}" href="#" title="{$picture['title']}">
												<span class="entry-thumbnail-icon">
													<img src="{imageUrl $picture['image'], width => 100, height => 75, crop => 1}" alt="{!$picture['title']}" class="detail-image">
												</span>
											</a>
											{/if}
										{/foreach}
									</div>
								</div>
								<script type="text/javascript">
									jQuery(window).load(function(){
										{if $options->theme->general->progressivePageLoading}
											if(!isResponsive(1024)){
												jQuery(".detail-thumbnail-slider").waypoint(function(){
													portfolioSingleEasySlider({$meta->videoRatio});
													jQuery(".detail-thumbnail-slider").parent().parent().addClass('load-finished');
												}, { triggerOnce: true, offset: "95%" });
											} else {
												portfolioSingleEasySlider({$meta->videoRatio});
												jQuery(".detail-thumbnail-slider").parent().parent().addClass('load-finished');
											}
										{else}
											portfolioSingleEasySlider({$meta->videoRatio});
											jQuery(".detail-thumbnail-slider").parent().parent().addClass('load-finished');
										{/if}
									});
								</script>
							</div>
						</section>
					{else}
						{if $meta->type == 'video'}
							<div class="detail-thumbnail-wrap detail-thumbnail-video">
								<div class="loading"><span class="ait-preloader">{!__ 'Loading&hellip;'}</span></div>
								{if strpos($meta->videoUrl, 'youtube') !== false}
									{var $parse = parse_url($meta->videoUrl)}
									{!= parse_str($parse['query'], $query)}
									{var $clipId = $query['v']}
									<iframe src="http://www.youtube.com/embed/{$clipId}?wmode=opaque&showinfo=0" width="{$vidWidth}" height="{$vidHeight}" class="detail-youtube"></iframe>
								{else}
									{var $clipId = trim(str_replace("vimeo.com", "", str_replace("/", "" ,str_replace("http:", "", $meta->videoUrl))))}
									<iframe src="http://player.vimeo.com/video/{$clipId}?title=0&amp;byline=0&amp;portrait=0" width="{$vidWidth}" height="{$vidHeight}" class="detail-vimeo"></iframe>
								{/if}
							</div>
							<script type="text/javascript">
								jQuery(window).load(function(){
									var ratio = {$meta->videoRatio};
									var pRatio = ratio.split(':');
									var width = jQuery('.detail-thumbnail-wrap').width();
									var height = (width / parseInt(pRatio[0])) * parseInt(pRatio[1]);
									jQuery('.detail-thumbnail-wrap iframe').each(function(){
										jQuery(this).attr({'width': width, 'height': height});
									});
									jQuery('.detail-thumbnail-wrap').addClass('video-loaded');
								});
							</script>
						{elseif $meta->type == 'website'}
							<div class="detail-thumbnail-wrap detail-thumbnail-website entry-content">
								{if $post->hasImage}
									<a href="{$meta->websiteUrl}" target="_blank" class="thumb-link">
										<span class="entry-thumbnail-icon">
											<img src="{imageUrl $post->imageUrl, width => 640, crop => 1}" alt="{!$post->title}" class="detail-image">
										</span>
									</a>
								{/if}
							</div>
						{else}
							<div class="detail-thumbnail-wrap detail-thumbnail-image entry-content">
								{if $post->hasImage}
									<a href="{imageUrl $post->imageUrl, width => 640, crop => 1}" class="thumb-link">
										<span class="entry-thumbnail-icon">
											<img src="{imageUrl $post->imageUrl, width => 640, crop => 1}" alt="{!$post->title}" class="detail-image">
										</span>
									</a>
								{/if}
							</div>
						{/if}
					{/if}
				</div>

				<div class="detail-description">
					{if $meta->informations}
					<div class="local-toggles type-accordion">
						{foreach $meta->informations as $info}
							<div class="toggle-header"><h3 class="toggle-title">{!$info[title]}</h3></div>
	  						<div class="toggle-content"><div class="toggle-container entry-content">{!$info[description]} </div></div>
						{/foreach}
					</div>
					{/if}
				</div>


			{!$post->linkPages}
		</div><!-- .detail-content -->

		<div class="detail-portfolio-content">
						<div class="detail-description">

					<div class="detail-text entry-content">
						{if $post->hasContent}
							{!$post->content}
						{else}
							{!$post->excerpt}
						{/if}
					</div>


				</div>
		</div>

		<footer class="entry-footer">
			{if $wp->isSingle and $post->author->bio and $post->author->isMulti}
				{includePart parts/author-bio, pageTitle => false}
			{/if}
		</footer><!-- .entry-footer -->

		{includePart parts/pagination location => nav-below}

	{/loop}
