{ifset $post}
{var $author = $post->author}
{/ifset}

{if $wp->isAuthor and !$pageTitle}

	<div class="author-info content">
		<div class="entry-content author-description">
			<h2>{__ 'About Me'}</h2>
			<div>
			{!do_shortcode($author->bio)}
			</div>
		</div>
	</div>

{else}

	<div class="author-info">
		<a href="{$author->postsUrl}" rel="author" class="author-link">
			<div class="author-avatar">
				{!$author->avatar(100)}
			</div><!-- #author-avatar -->
		</a>
			<div class="author-description">
				{var $authorName = '<span>'.$author->displayName.'</span>'}
				{if $pageTitle}
					<h1>{!$authorName}</h1>
				{else}
					<h2><a href="{$author->postsUrl}" rel="author" class="author-link">{!__ 'About %s'|printf: $authorName}</a></h2>
				{/if}

				{var $socIcons = $author->meta('user-metabox', 'socIcons')}
				{var $bio = $author->bio}
				{if (isset($socIcons) and $socIcons != '') or $bio != '' }
				<div>
					<p>{!wp_trim_words(strip_shortcodes($author->bio|striptags), 18)}</p>

					{if isset($socIcons) and $socIcons != ''}
						{includePart parts/social-icons, authorIcons => $author->meta('user-metabox', 'socIcons')}
					{/if}

				</div>
				{/if}
			</div><!-- /.author-description -->
	</div><!-- /.author-info -->

{/if}