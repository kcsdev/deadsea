<div class="categories">
	{if !$postElement}
	<div class="cat-title">{__ 'Posted in'}</div>
	{/if}
	<div class="cat-links">
		{if $postElement == 'yes'}
			{!$item->categoryList}
		{else}
			{!$post->categoryList}
		{/if}
	</div>
</div>