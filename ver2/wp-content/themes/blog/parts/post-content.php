
	{if !$wp->isSingular}

	{var $postElement = ''}

		{if $wp->isSearch}

			{*** SEARCH RESULTS ONLY ***}

			<article {!$post->htmlId} {!$post->htmlClass}>

				<a href="{$post->permalink}">

					<div class="item-container">

						{if $post->hasImage}
							{* var $ratio = explode(":", $imageHeight) *}
							{* var $imgHeight = ($imgWidth / $ratio[0]) * $ratio[1] *}
							<div class="item-thumbnail">
								<div class="item-thumbnail-wrapper">
									<div class="item-thumbnail-wrap" style="background-image: url('{imageUrl $post->imageUrl, width => 1000, height => 500, crop => 1}');"></div>
								</div>
							</div>
						{/if}

						<div class="item-content">
							<div class="item-title"><h3>{!$post->title}</h3></div>
							<div class="item-text">
								<div class="item-excerpt">{!$post->excerpt}</div>
							</div>
						</div>

					</div>

				</a>

				<div class="item-details">

					{if $post->type == post}
						<div class="item-row">
							<div class="item-label">{__ 'Author'}</div>
							<div class="item-row-value"><a href="{get_author_posts_url( $post->author->id )}">{!$post->author}</a></div>
						</div>
					{/if}

					<div class="item-row">
						<div class="item-label">{__ 'Date'}</div>
						<div class="item-row-value">{$post->date('c')|dateI18n: 'j. M Y'}</div>
					</div>

					{if $post->isInAnyCategory}
						<div class="item-row categories">
							<div class="item-label">{__ 'Categories'}</div>
							<div class="item-row-value">
								{var $postElement = ''}
								{includePart parts/entry-categories}
							</div>
						</div>
					{/if}

				</div>

			</article>

		{else}

			{*** STANDARD LOOP ***}

			<article {!$post->htmlId} {!$post->htmlClass}>

				<header class="entry-header {if !$post->hasImage}nothumbnail{/if}">

					<div class="entry-thumbnail">

						<div class="entry-thumbnail-wrap entry-content">

							{if $post->hasImage}
								<a href="{$post->permalink}" class="thumb-link">
									<span class="entry-thumbnail-icon">
										<img src="{imageUrl $post->imageUrl, width => 1000, height => 500, crop => 1}" alt="{!$post->title}">
									</span>
								</a>
							{/if}

							{var $dateIcon = $post->date('c')}
							{var $dateLinks = 'no'}
							{var $dateShort = 'no'}

							{includePart parts/entry-date-format, dateIcon => $dateIcon, dateLinks => $dateLinks, dateShort => $dateShort}

							<div class="entry-info">
								<div class="content">

								{if $post->isSticky and !$wp->isPaged and $wp->isHome}
									<div class="featured-post">
									{__ 'Featured Post'}
									</div>
								{/if}

								{includePart parts/entry-visits, postID => $post->id}

								{includePart parts/comments-link}

								</div>
							</div>

							<div class="entry-data">

								{if $post->type == post}
									{includePart parts/entry-author}
								{/if}

								{if $post->isInAnyCategory}
									{includePart parts/entry-categories}
								{/if}

							</div>

						</div>

					</div>

				</header><!-- /.entry-header -->

				<div class="entry-title">
					<h2><a href="{$post->permalink}">{!$post->title}</a></h2>
				</div><!-- /.entry-title -->

				<div class="entry-content loop">
					{!$post->excerpt}
				</div><!-- .entry-content -->

			</article>
		{/if}

	{else}

		{*** POST DETAIL ***}

		<article {!$post->htmlId} class="content-block">
			<div class="entry-content">
				{!$post->content}
				{!$post->linkPages}
			</div><!-- .entry-content -->

			<footer class="entry-footer">

				{if $post->tagList}
					<div class="tags">
						<span class="tags-title">{__ 'Tags: '}</span><span class="tags-links">{!$post->tagList('')}</span>
					</div>
				{/if}

				{if $wp->isSingle and $post->author->bio and $post->author->isMulti}
					{includePart parts/author-bio, pageTitle => false}
				{/if}

			</footer><!-- .entry-footer -->
		</article>

	{/if}
