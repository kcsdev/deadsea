<?php

/*
 * AIT WordPress Theme
 *
 * Copyright (c) 2013, Affinity Information Technology, s.r.o. (http://ait-themes.com)
 */

// === Usefull debugging constants ===================================

// if(!defined('AIT_DISABLE_CACHE')) define('AIT_DISABLE_CACHE', true);
// if(!defined('AIT_ENABLE_NDEBUGGER')) define('AIT_ENABLE_NDEBUGGER', true);


// === Loads AIT WordPress Framework ================================
require_once get_template_directory() . '/ait-theme/@framework/load.php';



// === Mandatory WordPress Standard functionality ===================

if(!isset($content_width)) $content_width = 1200;


// === Custom filters, actions for framework overrides ==============
require_once aitPath('includes', '/ait-custom-functions.php');
require_once aitPath('includes', '/ait-user-select-functions.php');


// === Run the theme ===============================================

AitTheme::run(aitPath('config', '/@theme-configuration.php'));


// === Custom settings ==============================================

add_filter('loop_shop_columns', create_function('', 'return 3;'));

// Display 6 products per page
add_filter('loop_shop_per_page', create_function( '$cols', 'return 6;' ), 20);



if ( aitIsPluginActive( "woocommerce" ) ) {
	add_action('after_switch_theme', 'ait_woocommerce_image_sizes', 1);
	function ait_woocommerce_image_sizes(){
		update_option( 'shop_single_image_size', array('width' => '600', 'height' => '600', 'crop' => 1) );
	}
}



// Change number of related products on product page
// Set your own value for 'posts_per_page'
add_filter( 'woocommerce_output_related_products_args', 'ait_related_products_args' );
function ait_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 3 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}


// Disable woocommerce default styles
if ( aitIsPluginActive( "woocommerce" ) ) {
	if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 ) {
		add_filter( 'woocommerce_enqueue_styles', create_function('', 'return array();') );
	} else {
		define( 'WOOCOMMERCE_USE_CSS', false );
	}
}



// === Helper Functions ============================================
function isColor($hex){
	$result = false;

	if(strpos($hex, "#") == 0 && strlen($hex) == 7){
		$result = true;
	}

	return $result;
}


// === Custom Menu Walkers =============================================
add_filter('wp_nav_menu_args', 'wp_nav_menu_modify_args', 105);

function wp_nav_menu_modify_args($args){
	if($args['theme_location'] == "footer"){
		$args['walker'] = new AitCustomInlineFrontendWalker();
	}
	return $args;
}


class AitCustomInlineFrontendWalker extends Walker_Nav_Menu
{


	function start_lvl( &$output, $depth = 0, $args = array() )
	{
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\"><!--\n";
	}



	function end_lvl( &$output, $depth = 0, $args = array() )
	{
		$indent = str_repeat("\t", $depth);
		$output .= "$indent--></ul>\n";
	}



	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
	{
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '--><li' /*. $id*/ . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		/** This filter is documented in wp-includes/post-template.php */
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}



	function end_el( &$output, $item, $depth = 0, $args = array() )
	{
		$output .= "</li><!--\n";
	}
}


// load entry-views plugin
require_once aitPath('includes', '/entry-views/entry-views.php');


// load single-locals-avatar plugin
require_once aitPath('includes', '/simple-local-avatars/simple-local-avatars.php');

// === Portal settings =============================================
require_once aitPath('theme', '/portal/functions/portal.php');

add_filter('woocommerce_prevent_admin_access', '__return_false');



// hotfix for easyadmin plugin - disable all CPTS for Author role
add_filter( 'ait-easyadmin-allowed-cpts', function($allowed){
	$user = wp_get_current_user();
	if (in_array( "author", (array) $user->roles)) {
		return array();
	}
	return $allowed;
} );




/* Display posts in admin for current user only */
function ait_altered_query($query) {
	if($query->is_admin) {
		$user = wp_get_current_user();
		if (in_array( "author", (array) $user->roles)) {
			$query->set('author', $user->data->ID);
		}
	}
	return $query;
}
add_filter('pre_get_posts', 'ait_altered_query');
function tomers_get_the_excerpt($post_id) {
  global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  $output = get_the_excerpt();
  $post = $save_post;
  return $output;
}
// pass php to js map
function localize_script_so_17497763() 
{ 
    // Get all posts
    $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'post',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true 
    );
    $posts_array = get_posts( $args ); 
    

    // Build localization array to be passed
    $localize_array = array();
    
    foreach ($posts_array as $post){
        $localize_array[$post->ID] = array(
            'id' => $post->ID,
            'map' => get_post_meta( $post->ID, '_post_post-gps-metabox', true),
            'name'=> $post->post_title,
            'slug'=> $post->post_name,
            'link'=> $post->guid,
            'image' => wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID )),
            'excerpt' => tomers_get_the_excerpt($post->ID),
            'category' => get_the_category($post->ID)
        );
    }

    wp_enqueue_script( 'map', get_template_directory_uri().'/design/js/map.js', array('jquery') );
    wp_localize_script( 'map', 'map_markers', $localize_array );
}
add_action('wp_enqueue_scripts', 'localize_script_so_17497763');


//custom post types news and events
/*add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'acme_news',
    array(
      'labels' => array(
        'name' => __( 'News' ),
        'singular_name' => __( 'News-Post' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
    register_post_type( 'acme_events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}*/
