<?php


define('AIT_REVSLIDER_ENABLED', true);
define('AIT_REVSLIDER_PACKAGE', 'developer');



add_action('plugins_loaded', 'aitRevsliderOverrides');
add_action('before_ajax_editor', 'aitAddRevsliderShortcodeToAjaxLoadedEditor');

function aitRevsliderOverrides()
{

	if(get_option('revslider-valid') === 'false'){
		update_option('revslider-valid', 'true');
	}
}


function aitAddRevsliderShortcodeToAjaxLoadedEditor()
{
	global $pagenow;

	if ($pagenow == 'admin-ajax.php') {
		// added rev slider shortcode dropdown to ajax-loaded editor
		RevSliderTinyBox::my_add_tinymce();
	}
}
